[![pipeline status](https://gricad-gitlab.univ-grenoble-alpes.fr/diamond/guix/guix-channel/badges/master/pipeline.svg)](https://gricad-gitlab.univ-grenoble-alpes.fr/diamond/guix/guix-channel/-/commits/main) 
[![build packages status](https://img.shields.io/badge/dynamic/json?url=https%3A%2F%2Fgricad-gitlab.univ-grenoble-alpes.fr%2Fdiamond%2Fguix%2Fguix-channel%2Fraw%2Fmaster%2Fstatus.json&query=%24.build_coverage&label=build%20coverage&color=green)](https://gricad-gitlab.univ-grenoble-alpes.fr/diamond/guix/guix-channel/-/commits/main)
[![package available](https://img.shields.io/badge/dynamic/json?url=https%3A%2F%2Fgricad-gitlab.univ-grenoble-alpes.fr%2Fdiamond%2Fguix%2Fguix-channel%2Fraw%2Fmaster%2Fstatus.json&query=%24.package_available&label=packages%20available&color=blue)](https://gricad-gitlab.univ-grenoble-alpes.fr/diamond/guix/guix-channel/-/tree/master/custom?ref_type=heads) 
[![pack packages status](https://img.shields.io/badge/dynamic/json?url=https%3A%2F%2Fgricad-gitlab.univ-grenoble-alpes.fr%2Fdiamond%2Fguix%2Fguix-packages%2Fraw%2Fmaster%2Fstatus.json&query=%24.container_coverage&label=container%20coverage&color=green)](https://gricad-gitlab.univ-grenoble-alpes.fr/diamond/guix/guix-packages/-/commits/main)
[![container available](https://img.shields.io/badge/dynamic/json?url=https%3A%2F%2Fgricad-gitlab.univ-grenoble-alpes.fr%2Fdiamond%2Fguix%2Fguix-packages%2Fraw%2Fmaster%2Fstatus.json&query=%24.container_available&label=containers%20available&color=blue)](https://gricad-gitlab.univ-grenoble-alpes.fr/diamond/guix/guix-packages/-/tree/master/manifests?ref_type=heads) 

# Package Guix pour DIAMOND du PEPR DIADEM

Le but de ce dépôt est de fournir des paquets guix pour les besoins de DIAMOND. Si les paquets semblent stables et fonctionnels, [charge aux mainteneurs de les proposer en upstream](https://guix.gnu.org/manual/en/html_node/Contributing.html)). 

## Comment accéder aux paquets de ce dépôt ?

Quand vous utiliser guix, l'ensemble des paquets disponibles proviennent [du channel guix par défaut.](https://hpc.guix.info/browse)

Pour ajouter ce channel précis et donc pouvoir installer les paquets à l'aide de la commande `guix install`, il faut créer un fichier `~/.config/guix/channels.scm` qui contiendra les lignes suivantes : 

```
;; Add gricad packages to those Guix provides.
(cons (channel
        (name 'guix-channel)
        (url "https://gricad-gitlab.univ-grenoble-alpes.fr/diamond/guix/guix-channel.git"))
      %default-channels)
```

Une fois ceci fait, lancez la commande `guix pull`. 

Après, le `guix pull`, comme indiqué en sortie, exécutez les commandes suivantes, pour être sûr d'utiliser la commande `guix` à jour : 

```
GUIX_PROFILE="$HOME/.config/guix/current"
. "$GUIX_PROFILE/etc/profile"
```

Pour vérifier que vous avez bien accès aux paquets diamond, vous pouvez tenter de rechercher le paquet `myquantum-espresso` :

```
guix search myquantum-espresso
```
