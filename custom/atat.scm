(define-module (custom atat)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system gnu)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages shells)
  #:use-module (gnu packages mpi)
  #:use-module (gnu packages perl))

(define-public atat
  (package
  (name "atat")
  (version "3_36")
  (source (origin
            (method url-fetch)
      (uri (string-append "http://alum.mit.edu/www/avdw/atat/atat" version ".tar.gz"))
            (sha256
             (base32
              "0fgfcrabd5wl80np1fqcihm6915pzl60dlanqigjn0aaf5dxlag8"))))
  (build-system gnu-build-system)
  (inputs
     `(("tcsh", tcsh)
       ("openmpi", openmpi)
       ("gfortran", gfortran)
       ("perl", perl)
       ))
  (arguments
   `(#:phases
      (modify-phases %standard-phases
          ;; There is no configure phase
          (delete 'configure)
          ;; Add a phase to fix bash interpreter
          (add-after 'unpack 'fix_interpreters
              (lambda* (#:key inputs #:allow-other-keys)
                  (for-each (lambda (f)
                      (invoke "sed" "-i" (string-append "s@/bin/csh@" (which "tcsh") "@g") f)
                      (invoke "sed" "-i" (string-append "s@/bin/sh@" (which "sh") "@g") f))
                          (find-files "."))))
    	  ;; on patche le HOME
    	  (add-before 'build 'patch-HOME-path
    		(lambda _
    			(setenv "HOME" (getenv "out"))))
          ;; We patch the makefile 
          ;; - to avoid the use of file that does not exist
          ;; - to force the compilation with mpi support
          (add-after 'patch-HOME-path 'patch-makefile
            (lambda _
                ; (invoke "sed" "-i" "21,33 s/^/#/" "foolproof")
                (invoke "sed" "-i" "25 s/^/#/" "makefile")
                (invoke "sed" "-i" "9,13 s/^/#/" "makefile")))
          ;; We create the bin directory, mandatory for the install phase
          (add-after 'patch-makefile 'create-bin-dir
            (lambda _
                (mkdir-p (string-append (getenv "out") "/bin"))))
        ;; There is no check phase
        (delete 'check)
      )))
  (synopsis "Alloy Theoretic Automated Toolkit (ATAT)")
  (description "The Alloy-Theoretic Automated Toolkit (ATAT)1 is a generic name that refers to a collection of alloy theory
tools developped by Axel van de Walle2 , in collaboration with various research groups.")
  (home-page "https://www.brown.edu/Departments/Engineering/Labs/avdw/atat/")
  (license license:cc-by4.0)))