(define-module (custom mfront)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system gnu)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (gnu packages commencement)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages boost)
  #:use-module (gnu packages python)
  #:use-module (gnu packages java)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages gl))

(define-public calculix
  (package
  (name "calculix")
  (version "2.22")
  (source (origin
            (method url-fetch)
      (uri (string-append "http://www.dhondt.de/cgx_" version ".all.tar.bz2"))
            (sha256
             (base32
              "1l91d5cqmjyqimmjjnhi8slkxhhblksxg9d6n4hyq3jni4846hn6"))))
  (build-system gnu-build-system)
  (inputs (list gfortran
                libx11
                mesa
                glu
                libxmu
                libxi
                libxt
                libsm
                libice))
  (arguments
   `(#:phases
      (modify-phases %standard-phases
           ; There is no proper build phase
           ; we need to go to the source directory and specify the CC compiler
           (replace 'build
              (lambda _
                  (chdir "cgx_2.22/src")
                  (invoke "make" "CC=gcc")))
           ; There is no proper install phase
           (replace 'install
              (lambda* (#:key outputs inputs #:allow-other-keys)
                (let* ((out (assoc-ref outputs "out"))
                       (bindir (string-append out "/bin"))
                       (incdir (string-append out "/include"))
                       (exdir (string-append out "/share/examples"))
                       (docdir (string-append out "/share/doc")))
                       ; we install the cgx binary ...
                       (install-file "cgx" bindir)
                       ; ... the header files ...
                       (for-each (lambda (f) (install-file f incdir))
                                 (find-files "." "\\.h"))
                       ; ... the examples ...
                       (for-each (lambda (f) (install-file f exdir))
                                 (find-files "../examples/"))
                       ; ... and the doc
                       (for-each (lambda (f) (install-file f docdir))
                                 (find-files "../doc/")))))
            ; There is no configure nor check phase
            (delete 'configure)
            (delete 'check))))
  (synopsis "A Free Software Three-Dimensional Structural Finite Element Program")
  (description "CalculiX is a package designed to solve field problems. The method used is the finite element method.")
  (home-page "https://www.calculix.de/")
  (license license:gpl2)))

(define-public mfront
  (package
  (name "mfront")
  (version "TFEL-4.2.1")
  (source (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://github.com/thelfer/tfel")
              (commit version)))
        (file-name (git-file-name name version))
        (sha256 (base32 "0swkmiwfpnmykg2spzh3lc0v95z9qyxkxhl74p7kzcz3gqyv4dgj"))))
  (build-system cmake-build-system)
  (inputs
     `(("gfortran", gfortran)
      ("gfortran-toolchain", gfortran-toolchain)
      ("gnuplot", gnuplot)
      ("python-sans-pip", python-sans-pip)
      ("boost", boost)
      ("calculix", calculix)
      ("openjdk", openjdk "jdk")))
  (arguments
   `(#:configure-flags 
            (list 
                  (string-append "-DCMAKE_INSTALL_PREFIX=" (getenv "out"))
                  "-DCMAKE_BUILD_TYPE=Release"
                  "-Denable-fortran=ON"
                  "-Denable-python=ON"
                  "-Denable-python-bindings=ON"
                  "-Denable-java=ON"
                  "-Denable-calculix=ON" 
                  "-Denable-libcxx=ON")
                  ;; Aster requires some SALOME tools (TODO)
                  ; "-Denable-aster=ON"
                  ;; proprietary licence dependencies 
                  ; "-Denable-abaqus=ON"
                  ; "-Denable-ansys=ON"
                  ; "-Denable-cyrano=ON"
                  ; "-Denable-comsol=ON"
                  ; "-Denable-diana-fea=ON"
                  ; "-Denable-lsdyna=ON"
                  ;; Europlexus is theoretically free of use for academic people but is behind a form ...
                  ; "-Denable-europlexus=ON"
                  ;; Z-set is only available via binaries ... 
                  ; "-Denable-zmat=ON" 
                  ;; not yet packaged
                  ; "-Denable-cadna=ON")
          #:phases
      (modify-phases %standard-phases
          ; The check phase requires to run the 'make check' command
          ; to compile and run the tests
          (replace 'check
            (lambda _
                (invoke "make" "check")))
          )))
  (synopsis "TFEL is a collaborative development of CEA and EDF")
  (description "MFront is a code generator which translates a set of closely related domain specific 
    languages into plain C++ on top of the TFEL library. Those languages cover three kind of material knowledge:
    material properties, mechanical behaviours and simple point-wise models.")
  (home-page "https://thelfer.github.io/tfel/web/index.html")
  (license license:gpl3)))
