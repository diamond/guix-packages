(define-module (custom atomsk)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system gnu)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages maths))

(define-public atomsk
  (package
  (name "atomsk")
  (version "Beta-0.13.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://github.com/pierrehirel/atomsk.git")
              (commit version)
              (recursive? #f)))
        (file-name (git-file-name name version))
        (sha256 (base32 "1k50h1zzn22h244ck92ippmr4xz100lax4cnnyq7p9vb61bpvn98"))))
  (build-system gnu-build-system)
  (inputs
     `(("gfortran", gfortran)
       ("lapack", lapack)
       ))
  (arguments
   `(#:phases
      (modify-phases %standard-phases
	;; Go in the source directory
        (replace 'configure
           (lambda _
              (chdir "src/")))
	;; We have to specify "atomsk" to compile it
        (replace 'build
           (lambda _
              (invoke "make" "atomsk")))
	;; The install instructions require sudo command and change permissions on files
	;; so, we replace the install procedure ... 
        (replace 'install
          (lambda* (#:key outputs inputs #:allow-other-keys)
            (let* ((out (assoc-ref outputs "out"))
                   (bindir (string-append out "/bin"))
                   (etcdir (string-append out "/etc"))
                   (docdir (string-append out "/share/doc")))
                   ;; install doc
                   (for-each (lambda (f) (install-file f docdir))
                             (find-files "../doc/"))
                   ;; install bash scripts
                   (for-each (lambda (f) (install-file f bindir))
                             (find-files "../tools/" "\\.sh"))
                   ;; install the conf file
                   (install-file "../etc/atomsk.conf" etcdir)
                   ;; install the binary
                   (install-file "atomsk" bindir)
                )))
	;; There is no check
        (delete 'check)
      )))
  (synopsis "Atomsk: A Tool For Manipulating And Converting Atomic Data Files")
  (description "Atomsk is a command-line program meant to manipulate
atomic systems for materials sciences.
Atomsk can convert from and to various file formats, and
offers options to duplicate atomic systems, introduce
defects, construct crystals and polycrystals, and more.")
  (home-page "https://atomsk.univ-lille.fr//")
  (license license:gpl3)))
