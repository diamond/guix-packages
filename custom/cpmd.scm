(define-module (custom cpmd)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system copy)
  #:use-module (guix build-system python)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages mpi))

(define-public python-cpqa
  (package
  (name "python-cpqa")
  (version "1.0")      
  (source (origin
    (method git-fetch)
      (uri (git-reference
          (url "https://github.com/CPMD-code/cpqa")
          (commit "8ba29b4652ee38353f1750e8e84fd9f38ff36b79")))
      (file-name (git-file-name name version))
      (sha256 (base32 "0k1zmz28afq0qa0i8j3jg2783nnz3hdi9k15wp8s3h9jih76b8c1"))))
    (build-system copy-build-system)
    (arguments
     '(#:install-plan
        ;; This code is just three simple python scripts
        ;; Thus, we need to make a custom building process
            ;; We install what we get from the github repo in the specific paths
       '(("./src/" "src")
         ("LICENSE" "share/LICENSE"))))
  (synopsis "CP Quality Assurance (part of the CP2K distribution) ")
  (description "CPQA is a Quality Assurance framework developed for CP2K and customised for CPMD.
    CPQA runs and compares regression tests.")
  (home-page "https://github.com/CPMD-code/cpqa")
  (license license:gpl3)))

(define-public cpmd-regtests
  (package
  (name "cpmd-regtests")
  (version "1.0")      
  (source (origin
    (method git-fetch)
      (uri (git-reference
          (url "https://github.com/CPMD-code/Regtests")
          (commit "b2b2c64d39da2ecb96857ab9a007f34332db9d1b")))
      (file-name (git-file-name name version))
      (sha256 (base32 "0chs5gmszxv4p9mahpif3b6fxjyrlhsj7cxsigc0fcshiqwavsas"))))
    (build-system copy-build-system)
    (arguments
     '(#:install-plan
        ;; This code is just three simple python scripts
        ;; Thus, we need to make a custom building process
        ;; We install what we get from the github repo in the specific paths
       '(("./tests/" "tests")
         ("config.py" "config.py")
         ("LICENSE" "share/LICENSE"))))
  (synopsis "Regression tests for the CPMD distribution")
  (description "This repository houses a set of tests that are run on CPMD to verify that changes or modifications 
    to the code have not introduced any new defects or issues. These tests are designed to ensure that the application 
    continues to function correctly and as intended after changes have been made. The sexecution of these tests 
    required the tool cpqa (distributed in the same organization under a different repository).")
  (home-page "https://github.com/CPMD-code/Regtests")
  (license license:expat)))

(define-public cpmd
  (package
  (name "cpmd")
  (version "4.3")      
  (source (origin
    (method git-fetch)
      (uri (git-reference
          (url "https://github.com/CPMD-code/CPMD")
          (commit version)))
      (file-name (git-file-name name version))
      (sha256 (base32 "0hfyhdd00zn4wd2iybpmqkvl511fc2gai8x7pz9dakiihgbm5fj7"))))
  (build-system gnu-build-system)
  (propagated-inputs (list gfortran
                           gcc
                           openmpi
                           lapack
                           openblas))
  (inputs (list python2-minimal
                python-cpqa
                cpmd-regtests))
  (arguments
   `(
     ; #:parallel-build? #f
     #:phases
      (modify-phases %standard-phases
          ;; fix the interpreters and the library path
          (add-after 'unpack 'fix_interpreters
              (lambda* (#:key inputs #:allow-other-keys)
                  (for-each (lambda (f)
                      (invoke "sed" "-i" (string-append "s@/usr/bin/ar@" (assoc-ref %build-inputs "gcc") "/bin/gcc-ar@g") f)
                      (invoke "sed" "-i" (string-append "s@/usr/bin/ranlib@" (assoc-ref %build-inputs "gcc") "/bin/gcc-ranlib@g") f)
                      (invoke "sed" "-i" (string-append "s@/home/manish/lapack-3.5.0/liblapack.a@" (assoc-ref %build-inputs "lapack") "/lib/liblapack.so@g") f)
                      (invoke "sed" "-i" (string-append "s@/home/manish/OPENBLAS/0.2.18/lib/libopenblas.a@" (assoc-ref %build-inputs "openblas") "/lib/libopenblas.so@g") f)
                      )
                          (find-files "."))))
          ;; Add compile flags
          (add-before 'configure 'add-compile-flags
            (lambda _
              (invoke "sed" "-i" "s@-O2 -Wall@-O2 -Wall -fallow-argument-mismatch -ffree-line-length-512@g" "configure/LINUX-X86_64-GFORTRAN-MPI")
              ))
          ;; Run the configure script 
          (replace 'configure
            (lambda _
              (invoke "scripts/configure.sh" "LINUX-X86_64-GFORTRAN-MPI")))
           ; There is no proper install phase
           (replace 'install
              (lambda* (#:key outputs inputs #:allow-other-keys)
                (let* ((out (assoc-ref outputs "out"))
                       (bindir (string-append out "/bin"))
                       (libdir (string-append out "/lib"))
                       (docdir (string-append out "/share/doc")))
                       ; we install the cpmd binary ...
                       (install-file "bin/cpmd.x" bindir)
                       ; ... the lib file ...
                       (install-file "lib/libcpmd.a" libdir)
                       ; ... and the doc
                       (for-each (lambda (f) (install-file f docdir))
                                 (find-files "doc/")))))
          ; ;; There is a set of regtests available on another repo
          (delete 'check)
          ; (replace 'check
          ;   (lambda _
          ;     ;; first, we copy all files necessary for the regtests
          ;     (copy-recursively (string-append (assoc-ref %build-inputs "python-cpqa") "/src") "python/src" #:keep-permissions? #f)
          ;     (copy-recursively (string-append (assoc-ref %build-inputs "cpmd-regtests") "/tests") "tests" #:keep-permissions? #f)
          ;     (copy-file (string-append (assoc-ref %build-inputs "cpmd-regtests") "/config.py") "python/src/config.py")
          ;     ;; then, we prepare the config file
          ;     (invoke "sed" "-i" "s@/sp/fd/teo/Development/cpmd_WORLD/CPMD/@../../@g" "python/src/config.py")
          ;     (invoke "sed" "-i" "s@'OBJ'@'bin'@g" "python/src/config.py")
          ;     (invoke "sed" "-i" "s@OBJ@@g" "python/src/config.py")
          ;     (invoke "sed" "-i" "s@/../regtests@@g" "python/src/config.py")
          ;     (invoke "sed" "-i" "s@nproc_mpi=2@nproc_mpi=1@g" "python/src/config.py")
          ;     (invoke "sed" "-i" "13 s/^/#/" "python/src/config.py")
          ;     (invoke "sed" "-i" "s@$PWD:@$PWD:$GUIX_PYTHONPATH:@g" "python/src/loadenv.sh")
          ;     ;; and we patch the python files ...
          ;     (invoke "sed" "-i" "22 s/^/#/" "python/src/cpqa/io.py")
          ;     (invoke "sed" "-i" "s@, numpy@@g" "python/src/cpqa/tests.py")
          ;     (invoke "sed" "-i" "s@numpy.array(data)@data@g" "python/src/cpqa/tests.py")

          ;     (invoke "sed" "-i" "s@len(self.select_paths_inp) > 0 or len(self.select_dirs) > 0@False@g" "python/src/cpqa/config.py")
          ;     (invoke "sed" "-i" (string-append "s@cpqa-driver.py@python2', '" (getenv "TMPDIR") "/source/python/src/scripts/cpqa-driver.py@g") "python/src/cpqa/runner.py")

          ;     ;; for debugging
          ;     ; (invoke "sed" "-i" "s@p = subprocess.Popen(args, cwd=self.config.tstdir, stdout=subprocess.PIPE)@print args@g" "python/src/cpqa/runner.py")
          ;     ; (invoke "sed" "-i" "s@p = subprocess.Popen(args, cwd=self.config.tstdir, stdout=subprocess.PIPE)@p = subprocess.Popen(['pwd'], cwd=self.config.tstdir, stdout=subprocess.PIPE)@g" "python/src/cpqa/runner.py")

          ;     ;; finally, we run the regtests
          ;     (chdir "python/src")
          ;     (setenv "OMPI_MCA_plm_rsh_agent" "")
          ;     (invoke "bash" "-c"
          ;        (format #f
          ;                "source ~a &&
          ;                python2 scripts/cpqa-main.py ../../tests/PROP/WANNIER/w-1/test.inp ../../tests/PROP/WANNIER/w-1" "loadenv.sh"))))
      )))
  (synopsis "Car-Parrinello Molecular Dynamics (CPMD)")
  (description "The CPMD code is a parallelized plane wave / pseudopotential implementation of Density 
    Functional Theory, particularly designed for ab-initio molecular dynamics.")
  (home-page "https://github.com/CPMD-code/CPMD")
  (license license:expat)))