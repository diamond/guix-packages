(define-module (custom nwchem)
  #:use-module (guix)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (guix build-system copy)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system gnu)
  #:use-module (guix git-download)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages algebra)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages commencement)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages mpi)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages python)
  #:use-module (gnu packages shells)
  #:use-module (gnu packages tcl)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages ssh)
  #:use-module (custom plumed)
  #:use-module (custom libxc)
  ; #:use-module (dftb+) ;; for tblite and dftf3
)

(define-public nwchem
  (package
    (name "nwchem")
    (version "7.2.2")
    (source
      (origin
        (method url-fetch)
        (uri (string-append "https://github.com/nwchemgit/nwchem/archive/refs/tags/v" version "-release.tar.gz"))
        (sha256 (base32 "1rvr643bcn5yv9si4hj8jmc42680wn63xxxmz87n2q2sqp492d53"))))
    (inputs
     `(
        ("bash" ,bash)
        ("expect" ,expect)
        ("fftw" ,fftw)
        ("gfortran" ,gfortran)
        ("global-arrays" ,global-arrays)
        ("openmpi" ,openmpi)
        ("openblas" ,openblas)
        ("lapack" ,lapack)
        ("scalapack" ,scalapack)
        ("perl" ,perl)
        ("python" ,python)
        ("plumed" ,plumed)
        ("libxc" ,libxc)
        ("dftd3" ,dftd3)
        ; ("simple-dftd3" ,simple-dftd3) ;; NWCHEM usually downloads it at build and has no (clear?) way to link it as an external tool
        ; ("tblite" ,tblite)             ;; NWCHEM usually downloads it at build and has no (clear?) way to link it as an external tool
        ("tcsh" ,tcsh)
        ("zlib" ,zlib)
        ; ("gcc-toolchain" ,gcc-toolchain) ;; might need to uncomment when trying to solve :
                                           ;;   ld: cannot find crt1.o: No such file or directory
                                           ;;   ld: cannot find crti.o: No such file or directory
                                           ;;   collect2: error: ld returned 1 exit status

      )
    )
    (native-inputs
     `(
        ("coreutils" ,coreutils) ;; only needed at build for /bin/rm
        ("inetutils" ,inetutils) ;; only needed at build for hostname
        ("patch" ,patch)         ;; only needed at build for /bin/patch
        ("which" ,which)         ;; only needed at build to automatically setup MPI environment
      )
    )
    (build-system gnu-build-system)
    (arguments
      (list 
        #:tests? #f #:test-target "check" ;; No test suite, but QA to be run by hand (7 fails out of 328 tests).
        #:phases 
          #~(modify-phases %standard-phases
              (add-before 'configure 'mpi-setup #$%openmpi-setup) ;; usually before check but somehow needed fot build
              (add-after 'unpack 'set-environment-variables
                (lambda* (#:key outputs inputs #:allow-other-keys)
                  (setenv "USE_MPI" "y")
                  (setenv "USE_MPIF" "y")
                  (setenv "USE_MPIF4" "y")
                  (setenv "NWCHEM_TARGET" "LINUX64")
                  (setenv "NWCHEM_MODULES" "pnnl") ;; All + Python
                  (setenv "USE_PYTHONCONFIG" "y")  ;; Comment those
                  (setenv "PYTHONVERSION" "3.10")  ;; three lines to
                  (setenv "PYTHONHOME" "")         ;; remove python support
                  (setenv "BLAS_SIZE" "4")                
                  (setenv "BLASOPT" "-lopenblas -fopenmp -lpthread -lrt")
                  (setenv "LAPACK_LIB" "-lopenblas -lpthread -lrt")
                  (setenv "SCALAPACK_SIZE" "4")
                  (setenv "SCALAPACK" (string-append "-L" (assoc-ref inputs "scalapack") "/lib" " -lscalapack"))
                  (setenv "EXTERNAL_GA_PATH" (string-append (assoc-ref inputs "global-arrays")))
                  (setenv "USE_PLUMED" "y")
                  (setenv "LIBXC_LIB" (string-append (assoc-ref inputs "libxc") "/lib"))
                  (setenv "LIBXC_INCLUDE" (string-append (assoc-ref inputs "libxc") "/include"))
                  (setenv "LIBXC_MODDIR" (string-append (assoc-ref inputs "libxc") "/include"))
                  (setenv "INSTALL_PREFIX" (string-append (assoc-ref outputs "out")))
                  ; (setenv "NWCHEM_MODULES" "all nwxc") ;; including nwxc ensures it is not "stubbed" by default, which requires nwxc_vdw3_dftd3 (needing download)
                  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                  ;; To include XTBLITE SUPPORT, not working
                  ; (setenv "USE_TBLITE" "y")
                  ; (setenv "NWCHEM_MODULES" "all xtb")
                  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                  (setenv "USE_OPENMP" "y") ;; I think it is not read ?
                  ; (setenv "FORCE_MPI_ENV" "y")
                  ; (setenv "LIBMPI" "-lmpi")
                  ; (setenv "MPI_LIB" (string-append (assoc-ref inputs "openmpi") "/lib"))
                  ; (setenv "MPI_INCLUDE" (string-append (assoc-ref inputs "openmpi") "/include"))
                  (setenv "ARMCI_NETWORK" "MPI-PR") ; not sure if it's needed 
                  (setenv "USE_64TO32" "y")
                  ; (setenv "BUILD_OPENBLAS" "0")  ; not sure if needed
                  ; (setenv "BUILD_SCALAPACK" "0") ; not sure if needed
                  ; (setenv "LD_LIBRARY_PATH" (getenv "LIBRARY_PATH")) ; should not need to be specified

                  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; LIBRARIES ARE NOT FOUND AT RUNTIME
                  ; (setenv "NWCHEM_BASIS_LIBRARY" (string-append (assoc-ref outputs "out") "/share/libraries")) ;;;;;;; NOT WORKING
                  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; LIBRARIES ARE NOT FOUND AT RUNTIME
                )
              )
              (add-before 'configure 'fix-interpreters
                (lambda* (#:key outputs inputs #:allow-other-keys)
                  (substitute* "./src/config/makefile.h"        ;; /usr/bin/env bash
                    (("/usr/bin/env bash")
                     (string-append (assoc-ref inputs "bash") "/bin/bash")))
                  (substitute* "./src/config/strip_compiler.sh" ;; /usr/bin/env bash
                    (("/usr/bin/env bash")
                     (string-append (assoc-ref inputs "bash") "/bin/bash")))
                  (substitute* "./src/peigs/DEFS"               ;; /usr/bin/env bash
                    (("/usr/bin/env bash")
                     (string-append (assoc-ref inputs "bash") "/bin/bash")))
                  (substitute* (find-files "./" "\\.py$")       ;; /usr/bin/env python
                    (("/usr/bin/env python")
                     (string-append (assoc-ref inputs "python") "/bin/python3")))
                  (substitute* (find-files "./" "makelib.h")    ;; /bin/rm
                    (("/bin/rm")
                     (string-append (assoc-ref inputs "coreutils") "/bin/rm")))
                  (substitute* (find-files "." "GNUmakefile")   ;; /bin/rm
                    (("/bin/rm")
                     (string-append (assoc-ref inputs "coreutils") "/bin/rm")))
                  (substitute* "./src/util/toplsf.pl"   ;; /bin/perl5
                    (("usr/local/bin/perl5")
                     (string-append (assoc-ref inputs "perl") "/bin/perl")))
                  (substitute* "./src/util/jobtime.pl"   ;; /bin/perl5
                    (("usr/local/bin/perl5")
                     (string-append (assoc-ref inputs "perl") "/bin/perl")))
                )
              )
              (add-after 'fix-interpreters 'download-mandatory-external
                (lambda* (#:key outputs inputs #:allow-other-keys)
                  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                  ;; NWChem usually tries to download dftd3.f
                  ;; So we download ity by hand in
                  ;;   [inputs-"dftd3"] /include
                  ;; and copy it at the right place
                  (copy-file
                    (string-append (assoc-ref inputs "dftd3") "/include/dftd3.f")
                    "./src/nwpw/nwpwlib/nwpwxc/dftd3.f"
                  )
                  ;; and then we avoid the actual download
                  (substitute* "./src/nwpw/nwpwlib/nwpwxc/build_dftd3a.sh"
                    ((" do_exit") "echo guix subbed an exit here") ;; to continue despite no download command being available
                    (("exit 1") "echo guix subbed an exit here")   ;; to avoid any exit altogether
                    (("^else") "")                                 ;; to avoid the part where the download happens
                    (("^tar.*$") "")                               ;; no tarball downloaded, so none to unpack
                    (("^rm -f") "ls")                              ;; and none to remove either
                  )
                  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                )
              )
              (add-after 'download-mandatory-external 'fix-nwchemrc-search-path
                (lambda* (#:key outputs inputs #:allow-other-keys)
                  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                  ;; Most of NWChem' parameters needed at runtime are
                  ;; stored in files called "nwchemrc" or ".nwchemrc"
                  ;; which are usually stored either :
                  ;;     * "./nwchemrc" in the running directory
                  ;;                    (searched first)
                  ;;  or * "$HOME/.nwchemrc" (searched second in case
                  ;;                          the former isn't found)
                  ;;  or * "/etc/nwchemrc" (searched after if neither
                  ;;                        of the previous ones have
                  ;;                        been found)
                  ;; However, this means NWCHEM would not run most of
                  ;; calculations by itself if being executed without
                  ;; the user taking time to setup a nwchemrc file on
                  ;; their own beforehand. Setting up such a file can
                  ;; prove tedious, especially in the case of Guix in
                  ;; which paths contain hashes.
                  ;; After some reflections, and for a better quality
                  ;; of life for the user, I overwrite within sources
                  ;; the latter of the three aformentioned options in
                  ;; order to look for :
                  ;;  /gnu/store/[hash]-nwchem-[version]/bin/nwchemrc
                  ;; rather than /etc/nwchemrc. This allows all users
                  ;; to either have "./nwchemrc" or "$HOME/.nwchemrc"
                  ;; if wanted, or to fallback on the predefined file
                  ;; when no personalized option is available.
                  ;; This is done in the following lines, and it also
                  ;; requires to create this default nwchemrc file at
                  ;; the desired path. This is done after the install
                  ;; phase in the "generate-nwchemrc" phase.
                  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                  (substitute* "./src/util/util_nwchemrc.F"
                    (("nwchrc=\"/etc/nwchemrc\"")
                     (string-append 
                                    "call util_getarg(0, home)\n"     ;; not gathering home but the executable path
                                    "          "                      ;; mandatory spaces in fortran 77 + indentation
                                    "nwchrc=home(1:inp_strlen(home))" ;; string of the path to the executable
                                    "//'rc'"                          ;; we want nwchemrc and not nwchem
                      )
                    )
                      ; ^ Warning for the exotic concatenation as f77 has a heavily constrained
                      ; line format, where columns position matters a lot :
                      ;  * everything after col. 72 is ignored (hence the "//\n" to use mutliple lines)
                      ;  * col. 1-5 are dedicated for comments/statement labels
                      ;  * col. 6 is for continuation of prev. line ("&"" here)
                  )
                  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                  ;; For the "modelpotential" feature, NWChem doesn't
                  ;; rely on "nwchemrc" files as for most other file-
                  ;; stored parameters.
                  ;; After a system-wide install, modelpotentials are
                  ;; found at "/share/libraries/rel-modelpotentials".
                  ;; If the superdirectory (libraries) is located via
                  ;; the NWCHEM_BASIS_LIBRARY environment variable or
                  ;; with a key/value pair in "nwchemrc" (see above),
                  ;; it turns out modelpotentials search isn't on par
                  ;; with this process. Instaed, it goes as :
                  ;;   * environment variable search first
                  ;;   * "srcdir" directory search second, located in
                  ;;     the source-code directory (build-time only)
                  ;;   * the current directory last (I'm not sure ??)
                  ;; So nwchemrc is not checked, despite in principle
                  ;; being an alternative to the environment variable
                  ;; pointing towards the very same directory. On the
                  ;; other hand, the code looks for the src directory
                  ;; which is, in the case of a guix install version,
                  ;; of course never available as it does not outlive
                  ;; the build phase.
                  ;; As such, I decide to overwrite the "looking into
                  ;; src" behavour to make it look inside nwchemrc as
                  ;; a replacement. This still allows the end user to
                  ;; use the environment variable if needed, while it
                  ;; also allows to use the environment variable like
                  ;; usually if the user wants.
                  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                  (substitute* "./src/NWints/rel/modelpotential_input.F"
                    (("character.*libname")
                     (string-append
                        "character*(nw_max_path_len) libname\n      "
                        "character*24 basis_library"))
                    (("call util_nwchem_srcdir.*libname.*")
                     (string-append
                        "basis_library='nwchem_basis_library'\n      "
                        ;; I think util_nwchemrc_get being a logical function requires it
                        ;; being in a logical expectig statement (like a if) ... ?
                        "if(.not.util_nwchemrc_get(basis_library,libname)) then\n      "
                        "  write(luout,*)''\n      "
                        "endif\n"))
                    (("basis/libraries/rel-modelpotentials/")
                      "rel-modelpotentials/")
                  )
                )
              )
              (replace 'configure
                (lambda* (#:key outputs inputs #:allow-other-keys)
                  (chdir "src")
                  (invoke "make" "nwchem_config")
                )
              )
              (add-after 'configure 'change-integer-size ;; extremely long but necessary
                (lambda* (#:key outputs inputs #:allow-other-keys)
                  (invoke "make" "-j" "64_to_32")
                )
              )
              (replace 'build
                (lambda* (#:key outputs inputs #:allow-other-keys)
                  (invoke "make" "-j" "CXX=g++" "FC=gfortran" "CC=gcc" "MPI_CXX=mpicxx" "MPI_F90=mpif90" "MPI_CC=mpicc")
                )
              )
              (add-after 'install 'generate-nwchemrc
                (lambda* (#:key outputs inputs #:allow-other-keys)
                  (invoke "sh" "-c"
                               (string-append "echo nwchem_basis_library "
                                  (assoc-ref outputs "out") "/share/libraries/"
                                  " >> " (assoc-ref outputs "out") "/bin/nwchemrc"))
                  (invoke "sh" "-c"
                               (string-append "echo nwchem_nwpw_library "
                                  (assoc-ref outputs "out") "/share/libraryps/"
                                  " >> " (assoc-ref outputs "out") "/bin/nwchemrc"))
                  (invoke "sh" "-c"
                               (string-append "echo ffield amber >> "
                                  (assoc-ref outputs "out") "/bin/nwchemrc"))
                  (invoke "sh" "-c"
                               (string-append "echo amber1 "
                                  (assoc-ref outputs "out") "/share/data/amber_s/"
                                  " >> " (assoc-ref outputs "out") "/bin/nwchemrc"))
                  (invoke "sh" "-c" 
                               (string-append "echo amber2 "
                                  (assoc-ref outputs "out") "/share/data/amber_q/"
                                  " >> " (assoc-ref outputs "out") "/bin/nwchemrc"))
                  (invoke "sh" "-c"
                               (string-append "echo amber3 "
                                  (assoc-ref outputs "out") "/share/data/amber_x/"
                                  " >> " (assoc-ref outputs "out") "/bin/nwchemrc"))
                  (invoke "sh" "-c"
                               (string-append "echo amber4 "
                                  (assoc-ref outputs "out") "/share/data/amber_u/"
                                  " >> " (assoc-ref outputs "out") "/bin/nwchemrc"))
                  (invoke "sh" "-c"
                               (string-append "echo spce "
                                  (assoc-ref outputs "out") "/share/data/slovents/spce.rst"
                                  " >> " (assoc-ref outputs "out") "/bin/nwchemrc"))
                  (invoke "sh" "-c"
                               (string-append "echo charmm_s "
                                  (assoc-ref outputs "out") "/share/data/charmm_s/"
                                  " >> " (assoc-ref outputs "out") "/bin/nwchemrc"))
                  (invoke "sh" "-c"
                               (string-append "echo charmm_x "
                                 (assoc-ref outputs "out") "/share/data/charmm_x/"
                                 " >> " (assoc-ref outputs "out") "/bin/nwchemrc"))
                )
              )
            )
      )
    )
    (home-page "https://www.nwchem-sw.org/")
    (synopsis "Northwest Computational Chemistry Package ")
    (description "NWChem aims to provide its users with computational chemistry tools that are scalable both in their ability to treat large scientific computational chemistry problems efficiently, and in their use of available parallel computing resources from high-performance parallel supercomputers to conventional workstation clusters. ")
    (license license:gpl3+)))

(define-public global-arrays
  (package
    (name "global-arrays")
    (version "5.8.2")
    (source
      (origin
        (method url-fetch) ;; the git-fetch version will not directly have a configure script available, but needs autoconf (not tested)
        (uri (string-append"https://github.com/GlobalArrays/ga/releases/download/v" version "/ga-" version ".tar.gz"))
        (sha256 (base32 "18i27ik9nik6z39my0rdljgrxgpg36z37ymgrz70avz3px59wnai"))))
    (inputs
     (list
      openmpi
      openblas
      openssh
      gfortran
      scalapack
      )
    )
    (build-system gnu-build-system)
    (arguments
      (list
        #:configure-flags 
          #~(list
              "--enable-shared=yes"
              (string-append "--with-blas=-L" (assoc-ref %build-inputs "openblas") "/lib -lopenblas")
              (string-append "--with-scalapack=-L" (assoc-ref %build-inputs "scalapack") "/lib -lscalapack")
              ; "--with-blas8=yes"
            )
          #:test-target "check"
          #:tests? #f ;; local standard minimal build --> 1 fail out of 31 tests "global/examples/lennard-jones/lennard.x"
                      ;; guix build                   --> 1 fail out of 68 tests (same as above)
          #:phases
            #~(modify-phases %standard-phases
                (delete 'bootstrap) ;; detrimentary phase which fails as it requires to download 
                                    ;; tools as well as writing in non-guix owned directories.
                (add-before 'check 'mpi-setup #$%openmpi-setup)
              )
      )
    )
    (home-page "https://globalarrays.readthedocs.io")
    (synopsis "The Global Arrays (GA) toolkit provides a shared memory style programming environment in the context of distributed array data structures (called “global arrays”).")
    (description "The Global Arrays (GA) toolkit provides a shared memory style programming environment in the context of distributed array data structures (called “global arrays”). From the user perspective, a global array can be used as if it was stored in shared memory. All details of the data distribution, addressing, and data access are encapsulated in the global array objects. Information about the actual data distribution and locality can be easily obtained and taken advantage of whenever data locality is important. The primary target architectures for which GA was developed are massively-parallel distributed-memory and scalable shared-memory systems.")
    (license license:bsd-3)))

(define-public dftd3
  (package
    (name "dftd3")
    (version "3.1")
    (source
      (origin
        (method url-fetch)
        (uri "https://web.archive.org/web/20210509200544if_/https://www.chemie.uni-bonn.de/pctc/mulliken-center/software/dft-d3/dftd3.tgz")
        (sha256 (base32 "0n8gi0raz8rik1rkd4ifq43wxs56ykxlhm22hpyq3ak1ixszjz6r"))))
    (build-system copy-build-system)
    (arguments
     `( 
        #:install-plan 
         `(
            ("./dftd3.f" "include/dftd3.f")
          )
      )
    )
    (home-page "https://web.archive.org/web/20210509200544if_/https://www.chemie.uni-bonn.de/pctc/mulliken-center/software/dft-d3")
    (synopsis "DFT-D3")
    (description "A dispersion correction for density functionals, Hartree-Fock and semi-empirical quantum chemical methods")
    (license license:gpl3+)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DISCLAIMER : all lines below here are packages not needed to build NWCHEM. Most of them work ok
;;              and were needed in some form to build global-arrays with cmake-build-system, which
;;              ends up not being suitable for NWChem because it does not produce exactly the same
;;              files. I still leave all that here as it took a lot of work to produce and it will
;;              maybe be useful later for other packaging works.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;; ends-up unused for the NWCHEM build as it does not produce "ga-config" executable needed by nwchem
(define-public global-arrays-cmake-build
  (package
    (name "global-arrays-cmake-build")
    (version "5.8.2")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://github.com/GlobalArrays/ga.git")
              (recursive? #f)
              (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256 (base32 "11msrg474vnwmxm3ybklswpsfjzxk9k9v85wby59m8kd1wid1xyr"))))
    (inputs
     (list
      blaspp
      lapackpp
      openmpi
      openblas
      ; lapack 
      gfortran
      ; scalapack
      )
    )
    (build-system cmake-build-system)
    (arguments
      (list
        #:configure-flags 
          #~(list
              "-DBUILD_SHARED_LIBS=ON"
              "-DGA=ON"
              "-DGA_BUILD=ON"
              "-DENABLE_BLAS=ON"
              "-DLINALG_VENDOR=OpenBLAS"
              (string-append "-DLINALG_PREFIX=" (assoc-ref %build-inputs "openblas"))
              (string-append "-DBLAS_PREFIX=" (assoc-ref %build-inputs "openblas"))
              ; (string-append "-DLAPACK_PREFIX=" (assoc-ref %build-inputs "lapack")) ;; not needed 
              ; "-DENABLE_SCALAPACK=ON"
              ; (string-append "-DScaLAPACK_PREFIX=" (assoc-ref %build-inputs "scalapack"))
              (string-append "-DFETCHCONTENT_SOURCE_DIR_BLASPP=" (assoc-ref %build-inputs "blaspp"))
              (string-append "-DFETCHCONTENT_SOURCE_DIR_LAPACKPP=" (assoc-ref %build-inputs "lapackpp"))
            )
          #:test-target "test"
          #:tests? #f ;; without scalapack --> 5 tests fail out of 46 (cause : timeouts each time)
          #:build-type "Release"
          #:phases
            #~(modify-phases %standard-phases
                (add-before 'check 'mpi-setup #$%openmpi-setup)
              )
      )
    )
    (home-page "https://globalarrays.readthedocs.io")
    (synopsis "The Global Arrays (GA) toolkit provides a shared memory style programming environment in the context of distributed array data structures (called “global arrays”).")
    (description "The Global Arrays (GA) toolkit provides a shared memory style programming environment in the context of distributed array data structures (called “global arrays”). From the user perspective, a global array can be used as if it was stored in shared memory. All details of the data distribution, addressing, and data access are encapsulated in the global array objects. Information about the actual data distribution and locality can be easily obtained and taken advantage of whenever data locality is important. The primary target architectures for which GA was developed are massively-parallel distributed-memory and scalable shared-memory systems.")
    (license license:bsd-3)))

;; unused for the NWCHEM build, only needed by the cmake build of global-arrays
(define-public blaspp
  (package
    (name "blaspp")
    (version "2024.05.31")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://github.com/icl-utk-edu/blaspp.git")
              (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256 (base32 "02n611n18yvyh4bcqj1zkpd3x669vvmkxm4k7d2lbxqshjhdqz1b"))))
    (inputs
     (list openblas gfortran python testsweeper))
    (build-system cmake-build-system)
    (arguments (list #:configure-flags #~(list "-Dblas=openblas")
                     #:test-target "check"
                     #:tests? #t
                     #:build-type "Release"))
    (home-page "https://github.com/icl-utk-edu/blaspp")
    (synopsis "C++ API for the Basic Linear Algebra Subroutines")
    (description "The Basic Linear Algebra Subprograms (BLAS) have been around for many decades and serve as the de facto standard for performance-portable and numerically robust implementation of essential linear algebra functionality. Originally, they were written in Fortran, and later furnished with a C API (CBLAS).")
    (license license:bsd-3)))

;; unused for the NWCHEM build, only needed by the cmake build of global-arrays
(define-public lapackpp
  (package
    (name "lapackpp")
    (version "2024.05.31")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://github.com/icl-utk-edu/lapackpp.git")
              (recursive? #f)
              (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256 (base32 "1h876nrj5gp7xlzb1r1qxxaha5fhvdsh0b6p5pdhahjfdxxh8mff"))))
    (inputs
     (list blaspp openblas lapack python testsweeper))
    (build-system cmake-build-system)
    (arguments (list #:configure-flags #~(list "-Dblas=openblas")
                     #:test-target "check"
                     #:tests? #t
                     #:build-type "Release"))
    (home-page "https://github.com/icl-utk-edu/lapackpp")
    (synopsis "C++ API for the Linear Algebra PACKage")
    (description "The Linear Algebra PACKage (LAPACK) is a standard software library for numerical linear algebra. It provides routines for solving systems of linear equations and linear least squares problems, eigenvalue problems, and singular value decomposition. It also includes routines to implement the associated matrix factorizations such as LU, QR, Cholesky, etc. LAPACK was originally written in FORTRAN 77, and moved to Fortran 90 in version 3.2 (2008). LAPACK provides routines for handling both real and complex matrices in both single and double precision.")
    (license license:bsd-3)))

;; unused for the NWCHEM build
;; NOT WORKING (can't specify blacspp so it tries to download it), sadly 
;; but would be useful for the cmake build of global-arrays with scalapack support 
(define-public scalapackpp
  (package
    (name "scalapackpp")
    (version "ed0b33f") ;; no real realease, using the one Global Arrays chooses by default.
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://github.com/wavefunction91/scalapackpp.git")
              (commit "ed0b33f")))
        (file-name (git-file-name name version))
        (sha256 (base32 "05ad5r6cllq0vxar69yl6dagjrydzaa85a28nwpjq1wp5wjjp6wr"))))
    (inputs
     (list blacspp catch2 blaspp lapackpp))
    (build-system cmake-build-system)
    (arguments (list 
      #:configure-flags 
          #~(list
              (string-append "-DFETCHCONTENT_SOURCE_DIR_BLACSPP=" (assoc-ref %build-inputs "blacspp"))
              (string-append "-DFETCHCONTENT_SOURCE_DIR_BLASPP=" (assoc-ref %build-inputs "blaspp"))
              (string-append "-DFETCHCONTENT_SOURCE_DIR_LAPACKPP=" (assoc-ref %build-inputs "lapackpp"))
            )
      #:build-type "Release"))
    (home-page "https://github.com/wavefunction91/scalapackpp")
    (synopsis "C++ API for the Scalable Linear Algebra PACKage")
    (description "C++ API for the Scalable Linear Algebra PACKage")
    (license license:bsd-3)))

;; unused for the NWCHEM build, needed by the non-working scalapackpp
(define-public blacspp
  (package
    (name "blacspp")
    (version "b6de5b1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://github.com/wavefunction91/blacspp.git")
              (recursive? #f)
              (commit "b6de5b1")))
        (file-name (git-file-name name version))
        (sha256 (base32 "1g7yfamhv4fdh8liiqx83mixppbhp5g4wbylwi5n58vl666ql3nv"))))
    (inputs
     (list catch2 openmpi openblas scalapack python linalg-cmake-modules))
    (build-system cmake-build-system)
    (arguments
      (list 
        #:configure-flags 
          #~(list
              (string-append "-DFETCHCONTENT_SOURCE_DIR_LINALG-CMAKE-MODULES=" (assoc-ref %build-inputs "linalg-cmake-modules"))
              ; (string-append "-DPYTHON_EXECUTABLE=" (assoc-ref %build-inputs "python") "/bin/python")
            )
        #:test-target "test"
        #:tests? #t
        #:build-type "Release"
        #:phases
          #~(modify-phases %standard-phases
              (add-before 'check 'mpi-setup #$%openmpi-setup)
            )
      )
    )
    (home-page "https://github.com/wavefunction91/blacspp")
    (synopsis " Modern C++ (C++17) wrappers for BLACS ")
    (description " Modern C++ (C++17) wrappers for BLACS ")
    (license license:bsd-3)))

;; unused for the NWCHEM build, needded by blacspp
(define-public catch2
  (package
    (name "catch2")
    (version "2.13.5")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://github.com/catchorg/Catch2.git")
              (recursive? #f)
              (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256 (base32 "051nx3gakkx278di8v5azs9935m5dplnijybj93jxqjaimf4i0vb"))))
    (inputs
     (list python))
    (build-system cmake-build-system)
    (arguments (list 
      #:tests? #f ;; no tests available to the best of my knowledge
      #:build-type "Release"))
    (home-page "https://github.com/catchorg/Catch2")
    (synopsis " A modern, C++-native, test framework for unit-tests, TDD and BDD - using C++14, C++17 and later")
    (description "Catch2 is mainly a unit testing framework for C++, but it also provides basic micro-benchmarking features, and simple BDD macros. Catch2's main advantage is that using it is both simple and natural. Test names do not have to be valid identifiers, assertions look like normal C++ boolean expressions, and sections provide a nice and local way to share set-up and tear-down code in tests.")
    (license license:expat)))

;; unused for the NWCHEM build, needded by blacspp
(define-public linalg-cmake-modules
  (package
    (name "linalg-cmake-modules")
    (version "290e2e0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://github.com/wavefunction91/linalg-cmake-modules.git")
              (recursive? #f)
              (commit "290e2e0")))
        (file-name (git-file-name name version))
        (sha256 (base32 "1jr6y91kc4a5sz97a1yg12b6xr3zv3zda5cc4gndswbskm6wii3x"))))
    (build-system copy-build-system)
    ; (arguments (list 
    ;                  #:test-target "check"
    ;                  #:tests? #t
    ;                  #:build-type "Release"))
    (home-page "https://github.com/wavefunction91/linalg-cmake-modules")
    (synopsis " A set of CMake Modules for robust linear algebra discovery ")
    (description " A set of CMake Modules for robust linear algebra discovery ")
    (license license:bsd-3)))

;; unused for the NWCHEM build, needed by the "[xxxxx]pp" suite 
(define-public testsweeper
  (package
    (name "testsweeper")
    (version "2024.05.31")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://github.com/icl-utk-edu/testsweeper.git")
              (recursive? #f)
              (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256 (base32 "0blyjqncp0rllswq26i9canqykaziy553qfn7r85pgp2bpif5kdw"))))
    (inputs
     (list python))
    (build-system cmake-build-system)
    (arguments (list 
                    ;  #:configure-flags #~(list "--enable-silent-rules")
                     #:test-target "check"
                     #:tests? #t
                     #:build-type "Release"))
    (home-page "https://github.com/icl-utk-edu/testsweeper")
    (synopsis "TestSweeper is a C++ testing framework for parameter sweeps.")
    (description "TestSweeper is a C++ testing framework for parameter sweeps. It handles parsing command line options, iterating over the test space, and printing results. This simplifies test functions by allowing them to concentrate on setting up and solving one problem at a time.")
    (license license:bsd-3)))
