(define-module (custom n2p2)
      #:use-module ((guix licenses) #:prefix license:)
      #:use-module (guix packages)
      #:use-module (guix download)
      #:use-module (guix utils)
      #:use-module (guix build utils)
      #:use-module (guix build-system gnu)
      #:use-module (gnu packages)
      #:use-module (gnu packages maths)
      #:use-module (gnu packages algebra)
      #:use-module (gnu packages mpi)
      #:use-module (gnu packages base)
      #:use-module (gnu packages gcc)
      #:use-module (gnu packages elf))

(define-public n2p2
 (package
    (name "n2p2")
    (version "2.1.4")
    (source (origin
            (method url-fetch)
            (uri (string-append "https://github.com/CompPhysVienna/n2p2/archive/refs/tags/v" version ".tar.gz"))
            (sha256
             (base32
              "1vql7awa9yq795bxkgq4izlzx83m8rz9f1k675znmlafmw4jqrzi"))))
    (build-system gnu-build-system)
      (arguments
        `(#:phases
         (modify-phases %standard-phases
          (add-after 'unpack 'post-unpack
            (lambda* (#:key outputs inputs #:allow-other-keys)
              (chdir "./src")
              (substitute* "makefile.gnu"
                (("PROJECT_EIGEN=/usr/include/eigen3")
                 (string-append "PROJECT_EIGEN="
                                (assoc-ref inputs "eigen") "/include/eigen3")))
              (substitute* "makefile.gnu"
                (("-lblas")
                 (string-append "-L"
                                (assoc-ref inputs "openblas") "/lib -lopenblas")))))
           (delete 'configure)
           (delete 'check)
           (replace 'build
             (lambda _
               (invoke "make" "--no-print-directory" "MODE=shared")))
           (replace 'install
             (lambda* (#:key outputs inputs #:allow-other-keys)
                (let* ((out (assoc-ref outputs "out"))
                    (bindir (string-append out "/bin"))
                    (libdir (string-append out "/lib"))
                    (incdir (string-append out "/include")))
                    (for-each (lambda (f) (install-file f libdir))
                             (find-files "../lib/" "."))
                    (for-each (lambda (f) (install-file f incdir))
                             (find-files "../include/" "."))
                    (for-each (lambda (f) (install-file f bindir))
                             (find-files "../bin/" ".")))))
      ;; Add a phase to correct the runpath of the binaries
          (add-after 'install 'correct-runpath
              (lambda* (#:key native-inputs inputs outputs #:allow-other-keys)
                  (define out (assoc-ref outputs "out"))
                  (invoke "rm" (string-append out "/bin/.gitignore"))
                  (define n2p2-bin (string-append out "/bin"))
                  (define n2p2-lib (string-append out "/lib"))
                  (define glibc-lib (string-append (assoc-ref inputs "glibc") "/lib"))
                  (define gcc-lib (string-append (assoc-ref inputs "gcc") "/lib"))
                  (define gsl-lib (string-append (assoc-ref inputs "gsl") "/lib"))
                  (define ompi-lib (string-append (assoc-ref inputs "openmpi") "/lib"))
                  (define oblas-lib (string-append (assoc-ref inputs "openblas") "/lib"))
                  (define ld.so (string-append (assoc-ref inputs "glibc") "/bin/ld.so"))
                  (define runpath-n2p2 
                      (string-append
                        n2p2-lib ":"
                        glibc-lib ":"
                        gcc-lib ":"
                        gsl-lib ":"
                        ompi-lib ":"
                        oblas-lib))
                  ;; Change runpath without --force-runpath patchelf option! (@PAB//gricad-guix-packages/common/absuite.scm)
                  (define (patch-elf-interpreter ld rpath file) ; for binaries
                      (make-file-writable file)
                          (format #t "Setting interpreter on '~a'...~%" file)
                          (format #t "Interpreter is '~a'...~%" ld)
                          (invoke "patchelf" "--set-interpreter" ld file)
                          (format #t "Setting RPATH on '~a'...~%" file)
                          (format #t "runpath is '~a'...~%" rpath)
                          (invoke "patchelf" "--set-rpath" rpath  file))
                  ;; Use patchelf
                  (for-each (lambda (f)
                      (patch-elf-interpreter ld.so runpath-n2p2 f)#t)
                          (find-files n2p2-bin))))
          )))
    (propagated-inputs
    `(("openmpi" ,openmpi)
      ("gsl" ,gsl)
      ("openblas" ,openblas)
      ("eigen" ,eigen)
      ("glibc" ,glibc)
      ("gcc" ,gcc "lib")
      ("patchelf" ,patchelf)))
  (synopsis "Neural network potentials for chemistry and physics")
  (description "This package contains software that will allow you to use
existing neural network potential parameterizations to predict energies and
forces (with standalone tools but also in conjunction with the MD software
LAMMPS).  In addition it is possible to train new neural network potentials
with the provided training tools.")
  (home-page "https://compphysvienna.github.io/n2p2/")
  (properties '((tunable? . #t)))        ;to benefit from SIMD code in Eigen
  (license license:gpl3+)))
