(define-module (custom dftb+)
  #:use-module (guix)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system python)
  #:use-module (guix build-system pyproject)
  #:use-module (guix git-download)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages mpi)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages ninja)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-build)
  #:use-module (custom plumed)
)

(define-public dftb+
  (package
    (name "dftb+")
    (version "24.1")
    (source
      (origin
        (method url-fetch)
        (uri (string-append "https://github.com/dftbplus/dftbplus/archive/refs/tags/" version ".tar.gz"))
        (sha256 (base32 "1cg0ym3qpbpvg00kxqq3h2dl1rxqrmjicfqcjgr6pq36jrvq6vbp"))))
    (inputs
     (list
      gfortran
      openblas
      scalapack
      ; arpack-ng-openmpi
      python
      plumed
      chimes_calculator
      libmbd
      mctc-lib
      mstore
      toml-f
      simple-dftd3
      multicharge
      dftd4
      tblite
      libnegf
      openmpi
      mpifx
      scalapack
      scalapackfx
      ; elsi-interface
     )
    )
    (build-system cmake-build-system)
    (arguments
      (list
        #:configure-flags 
          #~(list
              (string-append "-DCMAKE_INSTALL_PREFIX=" (assoc-ref %outputs "out"))
              ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
              ;; ARPACK / PARPACK OPTIONS
              ;; Not compatible with MPI ? Despite what's in the official install instructions
              "-DWITH_ARPACK=no" 
              ; "-DARPACK_LIBRARY=parpack"
              ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
              "-DWITH_PLUMED=yes"
              "-DPLUMED_LIBRARY=plumed"
              "-DWITH_POISSON=yes"
              "-DWITH_SOCKETS=yes"
              "-DWITH_CHIMES=yes"    ;; needs chimescalc --> url = https://github.com/rk-lindsey/chimes_calculator
              "-DWITH_MBD=yes"       ;; needs mbd --> url = https://github.com/libmbd/libmbd.git
              ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
              ;; The following two need the following ecosystem
              ;;   mctc-lib --> url = https://github.com/grimme-lab/mctc-lib
              ;;   mstore --> url = https://github.com/grimme-lab/mstore
              ;;   toml-f --> url = https://github.com/toml-f/toml-f
              ;;   simple-dftd3 --> url = https://github.com/dftd3/simple-dftd3
              ;;   multicharge --> url = https://github.com/grimme-lab/multicharge
              ;;   dftd4 --> url = https://github.com/dftd4/dftd4
              ;;   tblite --> url = https://github.com/tblite/tblite.git
              "-DWITH_SDFTD3=yes"
              "-DWITH_TBLITE=yes"
              ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
              "-DWITH_TRANSPORT=yes" ;; needs negf --> url = https://github.com/libnegf/libnegf.git
              ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
              ;; MPI requires
              ;;    mpifx --> url = https://github.com/dftbplus/mpifx.git
              ;;    scalapackfx --> url = https://github.com/dftbplus/scalapackfx.git
              ;; and seems like it does not work with arpack
              "-DWITH_MPI=yes"
              (string-append "-DSCALAPACK_LIBRARY=" (assoc-ref %build-inputs "scalapack") "/lib/libscalapack.so")
              ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
              ; "-DWITH_ELSI=yes"      ;; needs elsi + mpi --> see https://wordpress.elsi-interchange.org/
              "-DWITH_OMP=yes"
              ; "-DWITH_GPU=yes"       ;; not supported in the project yet
            )
        #:test-target "test"
        #:tests? #f ;; Needs to download external files
        #:build-type "Release"
        #:phases 
          #~(modify-phases %standard-phases 
              (add-before 'check 'mpi-setup #$%openmpi-setup)
            )
      )
    )
    (home-page "https://dftbplus.org/")
    (synopsis "DFTB+: general package for performing fast atomistic calculations")
    (description "DFTB+ is a fast and efficient versatile quantum mechanical simulation software package. Using DFTB+ you can carry out quantum mechanical atomistic simulations similar to density functional theory but in an approximate way, typically gaining around two orders of magnitude in speed.")
    (license license:gpl3)))

(define-public mpifx
  (package
    (name "mpifx")
    (version "1.5")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://github.com/dftbplus/mpifx.git")
              (recursive? #f)
              (commit "1.5")))
        (file-name (git-file-name name version))
        (sha256 (base32 "1lrpa01c8ia0bcns82czri8riwq1y0bmwf9l3ryzk11nilc0aaq3"))))
    (inputs
     (list gfortran openmpi python-fypp))
    (build-system cmake-build-system)
    (arguments (list 
                     #:test-target "test"
                     #:tests? #t
                     #:build-type "Release"
                     #:phases 
                     #~(modify-phases %standard-phases 
                         (add-before 'check 'mpi-setup #$%openmpi-setup)
                       )
               )
    )
    (home-page "https://github.com/dftbplus/mpifx")
    (synopsis "MpiFx - Modern Fortran Interface for MPI")
    (description "The open source library MpiFx provides modern Fortran (Fortran 2008) wrappers around routines of the MPI library to make their use as simple as possible. Currently several data distribution routines are covered.")
    (license license:bsd-3)))

(define-public python-fypp
  (package
    (name "python-fypp")
    (version "3.2")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "fypp" version))
       (sha256
        (base32 "1kiva4mpv7c18nh2nisdk31bi5rzf8r80s6qwbzhcwlsvmqhzhh5"))))
    (build-system pyproject-build-system)
    (native-inputs (list python-setuptools python-wheel))
    (home-page "https://github.com/aradi/fypp")
    (synopsis "Python powered Fortran preprocessor")
    (description "Python powered Fortran preprocessor.")
    (license license:bsd-3)))


(define-public scalapackfx
  (package
    (name "scalapackfx")
    (version "1.2")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://github.com/dftbplus/scalapackfx.git")
              (recursive? #f)
              (commit "1.2")))
        (file-name (git-file-name name version))
        (sha256 (base32 "1wslds2g2j5vlk7idk0jn5ym803ahkdczdfwq8yjrnjjs5229n64"))))
    (inputs
     (list gfortran openmpi lapack scalapack python-fypp))
    (build-system cmake-build-system)
    (arguments (list 
                     #:configure-flags
                     #~(list
                         (string-append "-DSCALAPACK_LIBRARY=" (assoc-ref %build-inputs "scalapack") "/lib/libscalapack.so")
                       )
                     #:test-target "test"
                     #:tests? #t
                     #:build-type "Release"
                     #:phases #~(modify-phases %standard-phases (add-before 'check 'mpi-setup #$%openmpi-setup))))
    (home-page "https://github.com/dftbplus/scalapackfx")
    (synopsis "ScalapackFx - Modern Fortran Interface for ScaLAPACK")
    (description "The open source library ScalapackFx provides convenient modern Fortran (Fortran 2003) wrappers for the routines of the ScaLAPACK library. Currently mostly the routines related to diagonalization are covered.")
    (license license:bsd-3)))

;; TO FINISH ;; ELPA PROBLEM ;; MPI FUNCITONS DEPRECATED ? ;; 
(define-public elsi-interface
  (package
    (name "elsi-interface")
    (version "2.10.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://gitlab.com/elsi_project/elsi_interface.git")
              (recursive? #f)
              (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256 (base32 "1v03fl45315j39k6pcry1nhidmkdvwcs51s6r7z768d0b475bq7w"))))
    (inputs
     (list gfortran openmpi openblas lapack scalapack slepc elpa))
    (build-system cmake-build-system)
    (arguments
      (list
        #:configure-flags 
          #~(list
              "-DCMAKE_Fortran_COMPILER=mpifort"
              "-DCMAKE_C_COMPILER=mpicc"
            )
        #:test-target "test"
        #:tests? #t
        #:build-type "Release"
        #:phases 
        #~(modify-phases %standard-phases
            (add-before 'check 'mpi-setup #$%openmpi-setup)
          )
      )
    )
    (home-page "https://wordpress.elsi-interchange.org/")
    (synopsis "ELSI - ELectronic Structure Infrastructure")
    (description "ELSI is a unified software interface designed for electronic structure codes to connect with various high-performance eigensolvers and density matrix solvers.")
    (license license:bsd-3)))

(define-public chimes_calculator
  (package
    (name "chimes_calculator")
    (version "1.0.2")
    (source
      (origin
        (method url-fetch)
        (uri (string-append "https://github.com/rk-lindsey/chimes_calculator/archive/refs/tags/v." version ".tar.gz"))
        (sha256 (base32 "0s7lc4x8z7yhxnmidxjsl0h0d7a70l4l15la8d6f0hygp29y55cp"))))
    (inputs
     (list gfortran python))
    (build-system cmake-build-system)
    (arguments (list 
                     #:test-target "test"
                     #:tests? #t
                     #:build-type "Release"))
    (home-page "https://chimes-calculator.readthedocs.io")
    (synopsis "Chebyshev Interaction Model for Efficient Simulation")
    (description "The Chebyshev Interaction Model for Efficient Simulation (ChIMES) is a machine-learned interatomic potential targeting chemistry in condensed phase systems. ChIMES models are able to approach quantum-accuracy through a systematically improvable explicitly many-bodied basis comprised of linear combinations of Chebyshev polynomials. ")
    (license license:gpl3)))

(define-public mctc-lib
  (package
    (name "mctc-lib")
    (version "0.3.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://github.com/grimme-lab/mctc-lib.git")
              (recursive? #f)
              (commit "v0.3.1")))
        (file-name (git-file-name name version))
        (sha256 (base32 "1f7ykqnf6mhgxidk5ipcixkn4pbajmcijs7kypgx1d92kgyy0y01"))))
    (inputs
     (list ninja gfortran))
    (build-system cmake-build-system)
    (arguments (list 
                     #:test-target "test"
                     #:tests? #t
                     #:build-type "Release"))
    (home-page "https://grimme-lab.github.io/mctc-lib/")
    (synopsis "Modular computation tool chain library")
    (description "Common tool chain for working with molecular structure data in various applications. This library provides a unified way to perform operations on molecular structure data, like reading and writing to common geometry file formats.")
    (license license:gpl3)))

(define-public mstore
  (package
    (name "mstore")
    (version "0.3.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://github.com/grimme-lab/mstore.git")
              (recursive? #f)
              (commit "v0.3.0")))
        (file-name (git-file-name name version))
        (sha256 (base32 "0g3jxc96hd7r57kczi5p28g2xm345ad1hk9lm5v6wlkmnrvg3ynd"))))
    (inputs
     (list ninja gfortran mctc-lib))
    (build-system cmake-build-system)
    (arguments (list 
                     #:test-target "test"
                     #:tests? #f ;; no test suite
                     #:build-type "Release"))
    (home-page "https://github.com/grimme-lab/mstore")
    (synopsis "Molecular structure store for testing")
    (description "Molecular structure store for testing")
    (license license:gpl3)))

(define-public toml-f
  (package
    (name "toml-f")
    (version "0.3.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://github.com/toml-f/toml-f.git")
              (recursive? #f)
              (commit "v0.3.1")))
        (file-name (git-file-name name version))
        (sha256 (base32 "1mb29jmav6333glm546rrvc993hf2n0awhp2i8gzwl498x9ffmph"))))
    (inputs
     (list gfortran test-drive))
    (build-system cmake-build-system)
    (arguments (list
                     #:configure-flags 
                      #~(list
                          "-DFETCHCONTENT_FULLY_DISCONNECTED=ON"
                        )
                     #:test-target "test"
                     #:tests? #t
                     #:build-type "Release"))
    (home-page "https://github.com/toml-f/toml-f")
    (synopsis "TOML parser for Fortran projects")
    (description "A TOML parser implementation for data serialization and deserialization in Fortran.")
    (license license:gpl3)))

(define-public test-drive
  (package
    (name "test-drive")
    (version "0.4.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://github.com/fortran-lang/test-drive.git")
              (recursive? #f)
              (commit "v0.4.0")))
        (file-name (git-file-name name version))
        (sha256 (base32 "0lws9f1wg3ypgambgqwa58478lcxy9hv9qzzklj9s7pmacf2gc1r"))))
    (inputs
     (list gfortran))
    (build-system cmake-build-system)
    (arguments (list 
                     #:test-target "test"
                     #:tests? #t
                     #:build-type "Release"))
    (home-page "https://github.com/fortran-lang/test-drive")
    (synopsis "The simple testing framework")
    (description "This project offers a lightweight, procedural unit testing framework based on nothing but standard Fortran. Integration with meson, cmake and Fortran package manager (fpm) is available. Alternatively, the testdrive.F90 source file can be redistributed in the project's testsuite as well.")
    (license license:gpl3)))

(define-public simple-dftd3
  (package
    (name "simple-dftd3")
    (version "1.0.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://github.com/dftd3/simple-dftd3.git")
              (recursive? #f)
              (commit "v1.0.0")))
        (file-name (git-file-name name version))
        (sha256 (base32 "1ymihgchic9w7iys9rxnxh4gz7q6nx8ab2hl2dday9w6l0lf5xbm"))))
    (inputs
     (list gfortran ninja mctc-lib openblas mstore toml-f))
    (build-system cmake-build-system)
    (arguments (list 
                     #:test-target "test"
                     #:tests? #t
                     #:build-type "Release"))
    (home-page "https://github.com/dftd3/simple-dftd3")
    (synopsis "The D3 dispersion model")
    (description "A simple drop-in replacement for dftd3.")
    (license license:gpl3)))

(define-public multicharge
  (package
    (name "multicharge")
    (version "0.2.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://github.com/grimme-lab/multicharge.git")
              (recursive? #f)
              (commit "v0.2.0")))
        (file-name (git-file-name name version))
        (sha256 (base32 "0paydb5jkyy1pg2l37j5kmgpsh95wm4gnw5x0lcl2xy6kz3kjhm1"))))
    (inputs
     (list gfortran ninja mctc-lib mstore openblas lapack))
    (build-system cmake-build-system)
    (arguments (list 
                     #:test-target "test"
                     #:tests? #t
                     #:build-type "Release"))
    (home-page "https://github.com/grimme-lab/multicharge")
    (synopsis "Atomic Partial Charge Model")
    (description "Electronegativity equilibration model for atomic partial charges.")
    (license license:gpl3)))

(define-public dftd4
  (package
    (name "dftd4")
    (version "3.6.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://github.com/dftd4/dftd4.git")
              (recursive? #f)
              (commit "v3.6.0")))
        (file-name (git-file-name name version))
        (sha256 (base32 "1mf1llpv5g6kmbjn89jysqk7nn1zzv8lb04kp8cl7iz1kpvpv1al"))))
    (inputs
     (list gfortran ninja mctc-lib mstore multicharge lapack openblas))
    (build-system cmake-build-system)
    (arguments (list 
                     #:test-target "test"
                     #:tests? #t
                     #:build-type "Release"))
    (home-page "https://github.com/dftd4/dftd4")
    (synopsis "DFT-D4 project")
    (description "Generally Applicable Atomic-Charge Dependent London Dispersion Correction.")
    (license license:gpl3)))

(define-public tblite
  (package
    (name "tblite")
    (version "0.3.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://github.com/tblite/tblite.git")
              (recursive? #f)
              (commit "v0.3.0")))
        (file-name (git-file-name name version))
        (sha256 (base32 "1nslvqb71xgrc8zc5qd9z30bashdpvmca9gb44wrkrzidwa81c27"))))
    (inputs
     (list gfortran ninja lapack openblas mctc-lib mstore toml-f dftd4 multicharge simple-dftd3))
    (build-system cmake-build-system)
    (arguments (list 
                     #:test-target "test"
                     #:tests? #t
                     #:build-type "Release"))
    (home-page "https://github.com/tblite/tblite")
    (synopsis "Light-weight tight-binding framework")
    (description "Generally Applicable Atomic-Charge Dependent London Dispersion Correction.This project is an effort to create a library implementation of the extended tight binding (xTB) Hamiltonian which can be shared between xtb and dftb+.")
    (license license:gpl3)))

(define-public libnegf
  (package
    (name "libnegf")
    (version "1.1.3")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://github.com/libnegf/libnegf.git")
              (recursive? #f)
              (commit "v1.1.3")))
        (file-name (git-file-name name version))
        (sha256 (base32 "1plnlrz2naya4x4jdn5ik9vmwkmcfy7skvw09mc75b8z333zy494"))))
    (inputs
     (list gfortran openblas openmpi mpifx python python-2))
    (build-system cmake-build-system)
    (arguments (list #:configure-flags #~(list "-DBUILD_TESTING=TRUE" "-DWITH_MPI=TRUE")
                     #:test-target "test"
                     #:tests? #f ;; 1/9 tests fails (number 2)
                     #:build-type "Release"
                     #:phases #~(modify-phases %standard-phases (add-before 'check 'mpi-setup #$%openmpi-setup))))
    (home-page "https://github.com/libnegf/libnegf")
    (synopsis "Light-weight tight-binding framework")
    (description "libNEGF is a general library for Non Equilibrium Green's Functions.")
    (license license:lgpl3)))

(define-public libmbd
  (package
    (name "libmbd")
    (version "0.12.8")
    (source
      (origin
        (method url-fetch)
        (uri (string-append "https://github.com/libmbd/libmbd/releases/download/" version "/libmbd-" version ".tar.gz"))
        (sha256 (base32 "07kbyx9yhl30pl4ydkyv5fblg3l4mrmbzp1jdkvizsvsil3622n5"))))
    (inputs
     (list gfortran openblas lapack scalapack openmpi))
    (build-system cmake-build-system)
    (arguments (list #:configure-flags #~(list "-DENABLE_SCALAPACK_MPI=ON")
                     #:test-target "test"
                     #:tests? #t
                     #:build-type "Release"
                     #:phases #~(modify-phases %standard-phases (add-before 'check 'mpi-setup #$%openmpi-setup))))
    (home-page "https://chimes-calculator.readthedocs.io")
    (synopsis "Library for the many-body dispersion (MBD) method")
    (description "libMBD implements the many-body dispersion (MBD) method in several programming languages and frameworks.  Fortran implementation is the reference, most advanced implementation, with support for analytical gradients and distributed parallelism, and additional functionality beyond the MBD method itself. It provides a low-level and a high-level Fortran API, as well as a C API. Furthermore, Python bindings to the C API are provided. The Python/Numpy implementation is intended for prototyping, and as a high-level language reference. The Python/Tensorflow implementation is an experiment that should enable rapid prototyping of machine learning applications with MBD.")
    (license license:mpl2.0)))
