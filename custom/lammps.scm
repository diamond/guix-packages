(define-module (custom lammps)
      #:use-module ((guix licenses) #:prefix license:)
      #:use-module (guix packages)
      #:use-module (guix download)
      #:use-module (guix utils)
      #:use-module (guix gexp)
      #:use-module (guix build utils)
      #:use-module (guix git-download)
      #:use-module (guix build-system cmake)
      #:use-module (guix build-system copy)
      #:use-module (guix build-system gnu)
      #:use-module (gnu packages)
      #:use-module (gnu packages base)
      #:use-module (gnu packages algebra)
      #:use-module (gnu packages chemistry)
      #:use-module (gnu packages image-processing)
      #:use-module (gnu packages image)
      #:use-module (gnu packages graphics)
      #:use-module (gnu packages maths)
      #:use-module (gnu packages mpi)
      #:use-module (gnu packages cmake)
      #:use-module (gnu packages python)
      #:use-module (gnu packages python-xyz)
      #:use-module (gnu packages compression)
      #:use-module (gnu packages commencement)
      #:use-module (gnu packages gcc)
      #:use-module (gnu packages gl)
      #:use-module (gnu packages vim)
      #:use-module (gnu packages pkg-config)
      #:use-module (gnu packages video)
      #:use-module (gnu packages llvm)
      #:use-module (gnu packages multiprecision)
      #:use-module (gnu packages xiph)
      #:use-module (gnu packages geo)
      #:use-module (gnu packages serialization)
      #:use-module (gnu packages xml)
      #:use-module (gnu packages pdf)
      #:use-module (gnu packages fontutils)
      #:use-module (gnu packages networking)
      #:use-module (gnu packages perl)
      #:use-module (gnu packages linux)
      #:use-module (gnu packages fabric-management)
      #:use-module (gnu packages sqlite)
      #:use-module (gnu packages crypto)
      #:use-module (gnu packages autotools)
      #:use-module (gnu packages check)
      #:use-module (gnu packages m4)
      #:use-module (gnu packages elf)
      #:use-module (custom plumed)
      #:use-module (custom quip)
      #:use-module (custom n2p2)
      #:use-module (custom kim-api))

(define-public voro-lib
  (package
  (name "voro-lib")
  (version "0.4.6")
  (source (origin
            (method url-fetch)
	    (uri (string-append "https://math.lbl.gov/voro++/download/dir/voro++-" version ".tar.gz"))
            (sha256
             (base32
              "0zj3xbrqf8sm49yhypy23k3w9786r94kcwm8v803ikp23q3p0ygg"))))
  (build-system gnu-build-system)
  (arguments
   `(
     #:phases
      (modify-phases %standard-phases
        (delete 'configure)
        (add-before 'build 'edit-compile-flags
          (lambda _ 
            (substitute* "config.mk" (("CFLAGS=") "CFLAGS=-fPIC ")) ;; neded to build LAMMPS as a shared library
          ))
        (replace 'install
             (lambda* (#:key outputs inputs #:allow-other-keys)
                (let* ((out (assoc-ref outputs "out"))
                    (bindir (string-append out "/bin"))
                    (incdir (string-append out "/include"))
                    (libdir (string-append out "/lib")))
                    (install-file "./src/voro++" bindir)
                    (for-each (lambda (f) (install-file f incdir))
                             (find-files "./src/" "\\.hh"))
                    (for-each (lambda (f) (install-file f libdir))
                             (find-files "./src/" "\\.a"))))))
      #:tests? #f))
  (inputs
     `(("gcc" ,gcc)
       ("gzip" ,gzip)
       ("perl" ,perl)))
  (native-inputs
    `(("bc" ,bc)))
  (synopsis "Voro++, a 3D cell-based Voronoi library")
  (description "Voro++ is a software library for carrying out three-dimensional computations of the Voronoi tessellation. A distinguishing feature of the Voro++ library is that it carries out cell-based calculations, computing the Voronoi cell for each particle individually, rather than computing the Voronoi tessellation as a global network of vertices and edges. It is particularly well-suited for applications that rely on cell-based statistics, where features of Voronoi cells (eg. volume, centroid, number of faces) can be used to analyze a system of particles.")
  (home-page "http://math.lbl.gov/voro++/")
  (license license:gpl2+)))



(define-public lammps
  (package
  (name "lammps")
  ;(version "patch_2Jul2021")
  ;(version "stable_2Aug2023")
  ; (version "stable_2Aug2023_update2")
  (version "stable_29Aug2024_update1")
  (source (origin
            (method url-fetch)
            (uri (string-append "https://github.com/lammps/lammps/archive/refs/tags/" version ".tar.gz"))
            (sha256
             (base32
              ;"0mzagwvrk97pyqjlpqpb487887xffc5na5l70k00k26yvfi8rpag")))) ;; for version patch_2Jul2021
              ;"1pzpax15dd8d29z5bb07jzlgrqy3rdd2fli0xa09ws430l5qpp28")))) ;; for version stable_2Aug2023
              ; "0jjn7pyxkf4x7ksarsyqxbvhb1f554nvb2frwjjd025dkjycmkiv")))) ;; for version stable_2Aug2023_update2
              "1p42sjbhal3yb2jnmimp5sg9cvv88nvcl528zhh83yx2ka343sis")))) ;; for version stable_29Aug2024_update1
  (build-system cmake-build-system)
  (arguments
   `(#:configure-flags
      (list
      "-DPKG_COMPRESS=yes"       ;; in most, was already here
      "-DPKG_DIFFRACTION=yes"    ;; in most, was already here
      "-DPKG_DPD-BASIC=yes"      ;; in most, was already here
      "-DPKG_EFF=yes"            ;; in most, was already here
      "-DPKG_KSPACE=yes"         ;; in most, was already here
      "-DPKG_MANYBODY=yes"       ;; in most, was already here
      "-DPKG_MC=yes"             ;; in most, was already here
      "-DPKG_MEAM=yes"           ;; in most, was already here
      "-DPKG_MISC=yes"           ;; in most, was already here
      "-DPKG_MOLECULE=yes"       ;; in most, was already here
      "-DPKG_PHONON=yes"         ;; in most, was already here
      "-DPKG_REAXFF=yes"         ;; in most, was already here
      "-DPKG_RIGID=yes"          ;; in most, was already here
      "-DPKG_USER-MISC=yes"      ;; not included in most but was there
      "-DPKG_ML-RANN=yes"        ;; not included in most but was there
      "-DPKG_ML-HDNNP=yes"       ;; not included in most but was there
      "-DPKG_BPM=yes"            ;; manually added by BA
      "-DDOWNLOAD_N2P2=no"       ;; because we built it earlier
      "-DPKG_KIM=yes"
      "-DDOWNLOAD_KIM=no"
      "-C ../cmake/presets/all_on.cmake"
      "-DDOWNLOAD_VORONOI=no"
      "-DPKG_VORONOI=yes"
      (string-append "-DVORO_LIBRARY=" (assoc-ref %build-inputs "voro-lib") "/lib/libvoro++.a")
      (string-append "-DVORO_INCLUDE_DIR=" (assoc-ref %build-inputs "voro-lib") "/include")
      "-DPKG_PLUMED=yes"
      "-DDOWNLOAD_PLUMED=no"
      "-DPLUMED_MODE=shared"
      (string-append "-DLOCAL_ML-PACE=" (assoc-ref %build-inputs "ml-pace-src"))
      "-DDOWNLOAD_SCAFACOS=no"
      (string-append "-DSCAFACOS_LIBRARY=" (assoc-ref %build-inputs "scafacos") "/lib/libfcs.a")
      (string-append "-DSCAFACOS_INCLUDE_DIR=" (assoc-ref %build-inputs "scafacos") "/include")
      ;; MPI
      "-D LAMMPS_MACHINE=mpi"
      "-D BUILD_MPI=yes"
      ;; Build libraries to link to other codes 
      "-DBUILD_SHARED_LIBS=yes"
      ;; GPU
      "-DPKG_GPU=no"
      (string-append "-DN2P2_DIR=" (assoc-ref %build-inputs "n2p2"))
      (string-append "-DCMAKE_INSTALL_PREFIX=" (assoc-ref %outputs "out"))
      "-DDOWNLOAD_POTENTIALS=off"
      ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
      ;; TESTING (not working yet)
      ; "-DENABLE_TESTING=on"
      ;"-DYAML_LIBRARY=somthing"
      ;"-DYAML_INCLUDEDIR=somthing"
      )
      #:phases
      (modify-phases %standard-phases
        (add-after 'unpack 'post-unpack
          (lambda* (#:key outputs inputs #:allow-other-keys)
            (mkdir-p "./build")
            (chdir "./build")
          ))
        (replace 'configure
          (lambda* (#:key inputs outputs configure-flags #:allow-other-keys)
            (let ((out (assoc-ref outputs "out")))
              (apply invoke "cmake" "../cmake" configure-flags))))
        (add-after 'install 'link-lammps-so
          (lambda* (#:key inputs outputs configure-flags #:allow-other-keys)
            (define lmp_exec 
                    (string-append (assoc-ref outputs "out") "/bin/lmp_mpi"))
            (define lmp_lib
                    (string-append (assoc-ref outputs "out") "/lib64"))
            (make-file-writable lmp_exec)
            (invoke "patchelf" "--add-rpath" lmp_lib lmp_exec)
          ))
        ; (replace 'check
        ;   (lambda _ 
        ;     (invoke "ctest")
        ;   ))
        )
      #:tests? #f
      #:build-type "Release"))
  (inputs
    `(("python" ,python-sans-pip)
      ("gfortran" ,gfortran)
      ("gfortran-lib" ,gfortran "lib")
      ("gcc" ,gcc)
      ("pkg-config" ,pkg-config)
      ("zlib" ,zlib)
      ("libomp" ,libomp)
      ("zstd" ,zstd)
      ("fftw" ,fftw)
      ("kim-api" ,kim-api)
      ("voro-lib" ,voro-lib)
      ("plumed" ,plumed)
      ("vtk" ,vtk)
      ("glew" ,glew)
      ("libtheora" ,libtheora)
      ("proj" ,proj)
      ("jsoncpp" ,jsoncpp)
      ("libxml2" ,libxml2)
      ("libharu" ,libharu)
      ("gl2ps" ,gl2ps)
      ("expat" ,expat)
      ("double-conversion" ,double-conversion)
      ("freetype" ,freetype)
      ("lz4" ,lz4)
      ("zstd" ,zstd)
      ("gsl" ,gsl)
      ("adios" ,adios)
      ("quip" ,quip)
      ("mdi-lib" ,mdi-lib)
      ("ml-pace-src" ,ml-pace-src)
      ("scafacos" ,scafacos)
      ("eigen" ,eigen)
      ("n2p2" ,n2p2)
      ("netcdf" ,netcdf)
      ("openmpi" ,openmpi)
      ("openblas" ,openblas)
      ("lapack" ,lapack)
      ("ffmpeg" ,ffmpeg)
      ("libpng" ,libpng)
      ("libjpeg" ,libjpeg-turbo)
      ("hdf5" ,hdf5)
      ("gzip" ,gzip)))
  (native-inputs
    `(("bc" ,bc)
      ("patchelf" ,patchelf)
      ;; needed for testing, but LAMMPS prefers downloading
      ;; its own version and does not propose clean ways to
      ;; provide your own for both of them.
      ; ("python-pyyaml" ,python-pyyaml)
      ; ("googletest" ,googletest)
     ))
  (synopsis "Classical molecular dynamics simulator")
  (description "LAMMPS is a classical molecular dynamics simulator designed to run efficiently on parallel computers.  LAMMPS has potentials for solid-state materials (metals, semiconductors), soft matter (biomolecules, polymers), and coarse-grained or mesoscopic systems.  It can be used to model atoms or, more generically, as a parallel particle simulator at the atomic, meso, or continuum scale.")
  (home-page "http://lammps.sandia.gov/")
  (license license:gpl2+)))

(define-public adios
  (package
    (name "adios")
    (version "2.10.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://github.com/ornladios/ADIOS2.git")
              (recursive? #f)
              (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256 (base32 "157d68vsww8wcrwpm9dc9lc5rpy922b28vy2rn9dhcfghix0vnzx"))))
    (inputs
     (list
        openmpi 
        zeromq 
        hdf5 
        pkg-config 
        python 
        perl 
        gfortran 
        bzip2 
        libpng 
        libfabric 
        ucx 
        sqlite
        libsodium
      ))
    (build-system cmake-build-system)
    (arguments
      (list 
        #:configure-flags 
        #~(list 
            "-DBUILD_SHARED_LIBS=ON"
            "-DADIOS2_BUILD_EXAMPLES=OFF"
            "-DBUILD_TESTING=ON"
            ; "-DCMAKE_INSTALL_PREFIX=replaceme"
            ; "-DCMAKE_PREFIX_PATH=separatedwithsmeicolons"
          )
        #:test-target "test"
        ;; too long + timeouts + a few tests fail out of more than 1600 
        ;; --> let's see if it's good enough for now
        #:tests? #f
        #:build-type "Release"
        #:phases 
        #~(modify-phases %standard-phases 
            (add-before 'check 'mpi-setup #$%openmpi-setup)
          )
      )
    )
    (home-page "https://adios2.readthedocs.io")
    (synopsis " Adaptable Input Output System 2")
    (description "ADIOS2 is the latest implementation of the Adaptable Input Output System. This brand new architecture continues the performance legacy of ADIOS1, and extends its capabilities to address the extreme challenges of scientific data IO.")
    (license license:gpl3)))

(define-public mdi-lib
  (package
    (name "mdi-lib")
    (version "1.4.29")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://github.com/MolSSI-MDI/MDI_Library.git")
              (recursive? #f)
              (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256 (base32 "0p3dybgcbpqxhci2f55b6zxwb9pss2ig9gridal9y34asjqlmj1x"))))
    (inputs
     (list gfortran openmpi python))
    (build-system cmake-build-system)
    (arguments
      (list
        #:configure-flags
        #~(list 
            "-Dlibtype=SHARED"
            "-Dlanguage=Fortran"
          )
          #:test-target "test"
          #:tests? #f
          #:build-type "Release"
          #:phases
          #~(modify-phases %standard-phases 
              (add-before 'check 'mpi-setup #$%openmpi-setup)
              (replace 'install
                ;; by default, mdi installs in /lib/mdi and /include/mdi respectively,
                ;; which are not loaded in defaults paths by Guix (/lib and /include) 
                (lambda* (#:key inputs outputs configure-flags #:allow-other-keys)7
                  (define libdir (string-append (assoc-ref outputs "out") "/lib"))
                  (define incdir (string-append (assoc-ref outputs "out") "/include"))
                  (define shadir (string-append (assoc-ref outputs "out") "/share/cmake/mdi"))
                  (for-each 
                    (lambda (f) (install-file f libdir))
                    (list "MDI_Library/libmdi.so"
                          "MDI_Library/libmdi.so.1" ))
                  (for-each 
                    (lambda (f) (install-file f incdir))
                    (list "MDI_Library/mdi.h"
                          "MDI_Library/mdi.mod" ))
                  ;; patching CMake files
                  (substitute* "MDI_Library/CMakeFiles/Export/02414a8867f2a4dd91707df29925cd54/mdiTargets-release.cmake"
                    (("/lib/mdi") "/lib")
                    (("/include/mdi") "/include"))
                  (for-each 
                    (lambda (f) (install-file f shadir))
                    (list "MDI_Library/mdiConfig.cmake"
                          "MDI_Library/mdiConfigVersion.cmake"
                          "MDI_Library/CMakeFiles/Export/02414a8867f2a4dd91707df29925cd54/mdiTargets.cmake"
                          "MDI_Library/CMakeFiles/Export/02414a8867f2a4dd91707df29925cd54/mdiTargets-release.cmake"))
                  ; (invoke "scp")
                )
              )
            )
      )
    )
    (home-page "https://molssi-mdi.github.io/MDI_Library")
    (synopsis "MolSSI Driver Interface (MDI) Library")
    (description "The MolSSI Driver Interface (MDI) project provides a standardized API for fast, on-the-fly communication between computational chemistry codes. This greatly simplifies the process of implementing methods that require the cooperation of multiple software packages and enables developers to write a single implementation that works across many different codes. The API is sufficiently general to support a wide variety of techniques, including QM/MM, ab initio MD, machine learning, advanced sampling, and path integral MD, while also being straightforwardly extensible. Communication between codes is handled by the MDI Library, which enables tight coupling between codes using either the MPI or TCP/IP methods.")
    (license license:bsd-3)))

(define-public ml-pace-src
  (package
    (name "ml-pace-src")
    (version "2024.9.11")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://github.com/ICAMS/lammps-user-pace.git")
              (recursive? #f)
              (commit (string-append "v." version))))
        (file-name (git-file-name name version))
        (sha256 (base32 "1amgb4p7i81dkxbrvzb1i9rsw43b46j9097257scss5cw40z68zi"))))
    (build-system copy-build-system)
    ;; So it turns out LAMMPS can work with a local copy of the Git repository for this, but I am
    ;; unable to link it to a existing compiled version on ML-PACE despite being easily able to
    ;; build it.
    (home-page "https://github.com/ICAMS/lammps-user-pace")
    (synopsis "Performant implementation of the atomic cluster expansion (uncompiled source code for LAMMPS)")
    (description "The atomic cluster expansion is a general polynomial expansion of the atomic energy in multi-atom basis functions. Here we implement the atomic cluster expansion in the performant C++ code PACE that is suitable for use in large-scale atomistic simulations. We briefly review the atomic cluster expansion and give detailed expressions for energies and forces as well as efficient algorithms for their evaluation. We demonstrate that the atomic cluster expansion as implemented in PACE shifts a previously established Pareto front for machine learning interatomic potentials toward faster and more accurate calculations. Moreover, general purpose parameterizations are presented for copper and silicon and evaluated in detail. We show that the Cu and Si potentials significantly improve on the best available potentials for highly accurate large-scale atomistic simulations.")
    (license license:gpl3+)))

(define-public ml-pace
  (package
    (name "ml-pace")
    (version "2024.9.11")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://github.com/ICAMS/lammps-user-pace.git")
              (recursive? #f)
              (commit (string-append "v." version))))
        (file-name (git-file-name name version))
        (sha256 (base32 "1amgb4p7i81dkxbrvzb1i9rsw43b46j9097257scss5cw40z68zi"))))
    (build-system cmake-build-system)
    (arguments
      (list 
        #:test-target "check"
        #:tests? #f
        #:build-type "Release"
        #:phases
        #~(modify-phases %standard-phases
            (replace 'install
              (lambda* (#:key inputs outputs configure-flags #:allow-other-keys)
                (define libdir (string-append (assoc-ref outputs "out") "/lib"))
                (for-each
                  (lambda (f) 
                          (install-file f libdir))
                          (list "libcnpy-static.a"
                                "libpace.a")
                )
                (install-file "build-yaml-cpp/libyaml-cpp-pace.a" (string-append libdir "/build-yaml-cpp"))
              )
            )
          )
      ))
    (home-page "https://github.com/ICAMS/lammps-user-pace")
    (synopsis "Performant implementation of the atomic cluster expansion")
    (description "The atomic cluster expansion is a general polynomial expansion of the atomic energy in multi-atom basis functions. Here we implement the atomic cluster expansion in the performant C++ code PACE that is suitable for use in large-scale atomistic simulations. We briefly review the atomic cluster expansion and give detailed expressions for energies and forces as well as efficient algorithms for their evaluation. We demonstrate that the atomic cluster expansion as implemented in PACE shifts a previously established Pareto front for machine learning interatomic potentials toward faster and more accurate calculations. Moreover, general purpose parameterizations are presented for copper and silicon and evaluated in detail. We show that the Cu and Si potentials significantly improve on the best available potentials for highly accurate large-scale atomistic simulations.")
    (license license:gpl3+)))

(define-public scafacos
  (package
    (name "scafacos")
    (version "1.0.4")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://github.com/scafacos/scafacos.git")
              (recursive? #t)
              (commit (string-append "v" version ))))
        (file-name (git-file-name name version))
        (sha256 (base32 "0s6wyfqgqlz0llzn65yflidk9303b14cky3xzrz2vmilkmbjq1mj"))))
    (native-inputs
     (list autoconf libtool automake m4))
    (inputs
     (list openmpi fftw gsl gfortran pkg-config python zlib))
    (build-system gnu-build-system)
    (arguments
      (list
        #:configure-flags
        #~(list 
            (string-append "--prefix=" (string-append (assoc-ref %outputs "out")))
            "-C" ;; to speed up
            "--disable-doc" ;; not needed by LAMMPS
            "--enable-fcs-solvers=fmm,p2nfft,direct,ewald,p3m" ;; the ones specified in LAMMPS CMake file
            "CFLAGS=-fPIC -g -O2"   ;; -fPIC neded to build LAMMPS as a shared library
            "CXXFLAGS=-fPIC -g -O2" ;; -fPIC  neded to build LAMMPS as a shared library
            "FCFLAGS=-fPIC -g -O2"  ;; -fPIC  neded to build LAMMPS as a shared library
          )
        #:test-target "check"
        #:tests? #t
        #:phases
        #~(modify-phases %standard-phases 
            (add-before 'check 'mpi-setup #$%openmpi-setup)
            ; (add-after 'mpi-setup 'fix-lz
            ;   (lambda* (#:key inputs #:allow-other-keys)
            ;     (for-each (lambda (f)
            ;       (invoke "sed" "-i" (string-append "s@libopen-pal.la -lz@libopen-pal.la " (assoc-ref %build-inputs "zlib") "/lib/libz.so@g") f))
            ;         (find-files "."))))
          )))
    (home-page "http://www.scafacos.de")
    (synopsis "Scalable Fast Coulomb Solvers")
    (description "This is a network project of German research groups working on a unified parallel library for various methods to solve electrostatic (and gravitational) problems in large particle simulations. The main focus of the project is to provide efficiently implemented methods for electrostatic problems in order to scale up to thousands of processors.")
    (license license:gpl3+)))