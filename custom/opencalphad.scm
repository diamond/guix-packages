(define-module (custom opencalphad)
  #:use-module (guix)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (guix build-system gnu)
  #:use-module (gnu packages gawk)
  #:use-module (gnu packages gcc))

(define-public opencalphad
  (package
    (name "opencalphad")
    (version "6.0")
    (source
      (origin
        (method url-fetch)
        (uri (string-append "https://github.com/sundmanbo/opencalphad/archive/refs/tags/v." version ".tar.gz"))
        (sha256 (base32 "1a1glhf1riaw53llccrr1svaqdx88gcc1flgbqlm8db1wzwldh5b"))))
    (inputs
     (list 
      ; gawk 
      gfortran))
    (build-system gnu-build-system)
    (arguments 
      (list
        #:phases
        #~(modify-phases %standard-phases
            (replace 'configure
              (lambda* (#:key outputs inputs #:allow-other-keys)
                (substitute* "Makefile"
                  (("#.*-DLinux")
                    "$(C) -c $(FCOPT) -DLinux")
                )
              )
            )
            (replace 'build
              (lambda* (#:key outputs inputs #:allow-other-keys)
                (invoke "make" "all")
              )
            )
            (replace 'install
              (lambda* (#:key outputs inputs #:allow-other-keys)
                (let* ((out (assoc-ref outputs "out"))
                    (bindir (string-append out "/bin"))
                    (libdir (string-append out "/lib"))
                    (incdir (string-append out "/include")))
                    (install-file "oc6P" bindir)
                    (install-file "liboceq.a" libdir)
                    (for-each (lambda (f) (install-file f libdir))
                             (find-files "./" "\\.o"))
                    (for-each (lambda (f) (install-file f incdir))
                             (find-files "." "\\.mod"))
                )
              )
            )
        )
        #:tests? #f
      )
    )
    (home-page "http://www.opencalphad.com/")
    (synopsis "OpenCalphad is an informal international collaboration of scientists and researchers interested in the development of free high-quality software and databases for thermodynamic calculations for all kinds of applications.")
    (description "The intention is to allow interested scientists to develop thermodynamic models and assess model parameters for thermodynamic databases to describe experimental data as well as theoretical results from DFT calculations to calculate phase equilibria and phase diagrams. The software can also be used to provide scientists interested in materials simulations a flexible and reliable software using consistent thermodynamic databases.")
    (license license:gpl3)))
