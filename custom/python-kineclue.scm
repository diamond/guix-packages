(define-module (custom python-kineclue)
  #:use-module (guix)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system python)
  #:use-module (guix build utils)
  #:use-module (guix build-system copy)
  #:use-module (guix git-download)
  #:use-module (guix utils)
  #:use-module (gnu packages)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages python-science))

(define-public python-kineclue
  (let ((commit "96373ec7d558b26d9e5306b763bfde263c9913f9")
        (version "1.0"))
  (package
    (name "python-kineclue")
    (version version)
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://github.com/lukamessina/kineclue.git")
              (commit commit)
              (recursive? #f)))
        (file-name (git-file-name name version))
        (sha256 (base32 "0vdqw5nza6rak1k1d5k48sj24nd90m3fgdzgyh5ha72c8bhn6ani"))))
    (build-system copy-build-system)
    (propagated-inputs 
        (list python
              python-mpmath
              python-numpy
              python-psutil
              python-scipy
              python-sympy))
    (arguments
     '(#:install-plan
        ;; This code is just three simple python scripts
        ;; Thus, we need to make a custom building process
            ;; We install what we get from the github repo in the specific paths
       '(("./Examples/" "share/examples")
         ("./Examples/" "share/examples")
         ("." "share/doc" #:include-regexp ("pdf"))
         ("." "scripts" #:include-regexp ("py")))))
    (home-page "https://github.com/lukamessina/kineclue")
    (synopsis "KineCluE - Kinetic Cluster Expansion")
    (description "KineCluE aims at computing cluster transport coefficients for general homogeneous crystallographic
        systems from the atomistic description of evolution mechanisms. It is based on the self-consistent mean-field 
        theory and written in Python 3.7. Technical references can be found in T. Schuler, L. Messina and M. Nastar, 
        Computational Matrials Science 109191 (2019).")
    (license license:lgpl3))))
