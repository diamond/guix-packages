(define-module (custom libxc)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system gnu)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages pkg-config)
)


(define-public libxc
  (package
    (name "libxc")
    (version "5.1.5")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://gitlab.com/libxc/libxc.git")
              (commit version)
              (recursive? #t)))
        (file-name (git-file-name name version))
        (sha256 (base32 "0cy3x2zn1bldc5i0rzislfbc8h4nqgds445jkfqjv0d1shvdy0zn"))))
    (build-system gnu-build-system)
    (native-inputs
     `(("automake" ,automake)
       ("autoconf" ,autoconf)
       ("bash" ,bash)
       ("gfortran" ,gfortran)
       ("libtool" ,libtool)
       ("perl" ,perl)
       ("pkg-config" ,pkg-config)))
    (arguments
     `(#:configure-flags (list "--enable-shared")))
    (home-page "")
    (synopsis "")
    (description "")
    (license license:mpl2.0)))