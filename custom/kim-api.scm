(define-module (custom kim-api)
      #:use-module ((guix licenses) #:prefix license:)
      #:use-module (guix packages)
      #:use-module (guix download)
      #:use-module (guix build utils)
      #:use-module (guix git-download)
      #:use-module (guix build-system cmake)
      #:use-module (gnu packages)
      #:use-module (gnu packages base)
      #:use-module (gnu packages commencement)
      #:use-module (gnu packages cmake)
      #:use-module (gnu packages vim)
      #:use-module (gnu packages pkg-config))

(define-public kim-api
  (package
    (name "kim-api")
    (version "2.3.0")
    (source (origin
      (method url-fetch)
      (uri (string-append "https://s3.openkim.org/kim-api/kim-api-" version ".txz"))
      (sha256
        (base32
          "14gr2hn8d5ii6wzcdaqcs5k36ybj2rfr2rzfya8mfqn0zfw3nrwk"))))
    (build-system cmake-build-system)
    (arguments
      `(#:configure-flags
        (list
          "-DCMAKE_BUILD_TYPE=Release"
          (string-append "-DCMAKE_INSTALL_PREFIX=" (assoc-ref %outputs "out")))
        #:phases
        (modify-phases %standard-phases
          (delete 'validate-runpath)
          (add-after 'unpack 'post-unpack
            (lambda* (#:key outputs inputs version #:allow-other-keys)
              (invoke "tar" "xf" (string-append "kim-api-" ,version ".tar"))
              (mkdir-p "./build")
              (chdir "./build")
              (setenv "FC" (which "gfortran"))
            )
          )
          (replace 'configure
            (lambda* (#:key inputs outputs configure-flags #:allow-other-keys)
                     (let ((out (assoc-ref outputs "out")))
                       (apply invoke "cmake" (string-append "../kim-api-" ,version ) configure-flags)
                     )
            )
          )
        )
        #:tests? #f
       )
    )
    ;; TODO how to install make (and others?) as a propagated-input if required to install user models?
    (propagated-inputs
      `(("tar" ,tar)
        ("gfortran" ,gfortran-toolchain)
        ("cmake" ,cmake)
    ;;    ("make" ,make)
        ("xxd" ,xxd)
        ("pkg-config" ,pkg-config)
        ("gcc-toolchain" ,gcc-toolchain-11)))
    (synopsis "Open Knowledgebase of Interatomic Models")
    (description "OpenKIM is a curated repository of interatomic potentials and analytics for making classical molecular simulations of materials reliable, reproducible, and accessible.")
    (home-page "https://openkim.org/")
    (license license:lgpl2.1+)))