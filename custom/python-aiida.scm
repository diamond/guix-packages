(define-module (custom python-aiida)
  #:use-module (guix)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system pyproject)
  #:use-module (guix build-system python)
  #:use-module (guix build utils)
  #:use-module (guix git-download)
  #:use-module (guix utils)
  #:use-module (gnu packages)
  #:use-module (guix transformations)
  #:use-module (gnu packages check)
  #:use-module (gnu packages databases)
  #:use-module (gnu packages graphviz)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-build)
  #:use-module (gnu packages python-check)
  #:use-module (gnu packages python-crypto)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages python-web)
  #:use-module (gnu packages sphinx)
  #:use-module (gnu packages time)
  #:use-module (gnu packages version-control)
  #:use-module (custom python-ase)
  #:use-module (custom python-phonopy)
  #:use-module (custom python-pymatgen)
  #:use-module (custom python-spglib)
  )

(define-public python-aiida-export-migration-tests
  (package
    (name "python-aiida-export-migration-tests")
    (version "0.9.0")
    (source (origin
              (method url-fetch)
        (uri (pypi-uri "aiida-export-migration-tests" version))
              (sha256
               (base32
                "0qsydgwafap0dfxav1qhq0x199pvynj29yq63zf238h6xca4qmnj"))))
    (build-system pyproject-build-system)
    (native-inputs (list python-setuptools python-wheel))
    (home-page "https://github.com/aiidateam/aiida-export-migration-tests")
    (synopsis "Export archives for migration tests for AiiDA")
    (description "Export archives for migration tests for @code{AiiDA}.")
    (license #f)))

(define-public python-upf-to-json
  (package
    (name "python-upf-to-json")
    (version "0.9.5")
    (source (origin
              (method url-fetch)
        (uri (pypi-uri "upf_to_json" version))
              (sha256
               (base32
                "0jkfylsq4a1x7ljsbf058sc8fnzmnbflxkkr2qb4zw3phr64qqap"))))
    (build-system pyproject-build-system)
    (native-inputs (list python-setuptools python-wheel))
    (arguments
    `(#:phases
      (modify-phases %standard-phases
          ;; No tests embedded
          (delete 'check))))
    (home-page "https://github.com/simonpintarelli/upf_to_json")
    (synopsis "upf to json converter")
    (description "upf to json converter.")
    (license #f)))

(define-public python-aiida-core
  (package
    (name "python-aiida-core")
    (version "2.6.3")
    (source (origin
              (method url-fetch)
        (uri (pypi-uri "aiida_core" version))
              (sha256
               (base32
                "1advr2v43xsyfr2m3zx9b6ra81jkrxkg9n4a86lxwqixyh9qgdm6"))))
    (build-system pyproject-build-system)
    (propagated-inputs (list python-aiida-export-migration-tests
                             ; python-aio-pika
                             python-alembic
                             ; python-archive-path
                             python-ase
                             ; python-bpython
                             ; python-circus
                             python-click
                             ; python-click-spinner
                             python-coverage
                             ; python-disk-objectstore
                             python-docstring-parser
                             python-docutils
                             python-flask
                             python-flask-cors
                             python-flask-restful
                             python-flit-core
                             ; python-get-annotations
                             python-graphviz
                             python-gssapi
                             python-importlib-metadata
                             python-ipython
                             python-jedi
                             python-jinja2
                             ; python-jupyter
                             ; python-jupyter-client
                             ; python-kiwipy
                             python-matplotlib
                             python-mypy
                             ; python-myst-nb
                             python-notebook
                             python-numpy
                             python-packaging
                             python-paramiko
                             ; python-pg8000
                             ; python-pgsu
                             ; python-pgtest
                             ; python-plumpy
                             python-pre-commit
                             python-psutil
                             python-psycopg2-binary
                             python-pyasn1
                             ; python-pycifrw
                             python-pydantic
                             python-pydata-sphinx-theme
                             python-pymatgen
                             python-pympler
                             python-pymysql
                             python-pyparsing
                             python-memcached
                             python-pytz
                             python-pyyaml
                             python-requests
                             python-seekpath
                             python-spglib
                             python-sphinx
                             python-sphinx-copybutton
                             ; python-sphinx-design
                             python-sphinx-intl
                             ; python-sphinx-notfound-page
                             ; python-sphinx-sqlalchemy
                             ; python-sphinxext-rediraffe
                             python-sqlalchemy
                             python-tabulate
                             python-tomli
                             python-tqdm
                             ; python-trogon
                             ; python-types-pyyaml
                             python-upf-to-json
                             python-wrapt))
    (native-inputs (list python-setuptools))
    (arguments
    `(#:phases
      (modify-phases %standard-phases
          ;; No tests embedded
          (delete 'check)
          (delete 'sanity-check))))
    (home-page "https://github.com/aiidateam/aiida-core")
    (synopsis
     "AiiDA is a workflow manager for computational science with a strong focus on provenance, performance and extensibility.")
    (description
     "@code{AiiDA} is a workflow manager for computational science with a strong focus
on provenance, performance and extensibility.")
    (license license:expat)))

(define-public python-aiida
  (package
    (name "python-aiida")
    (version "1.0.1")
    (source (origin
              (method url-fetch)
        (uri (pypi-uri "aiida" version))
              (sha256
               (base32
                "1gfs89v6c37gyx7f6zgfkd2s47ng0c67kinbfklmrhr8si3wyqky"))))
    (build-system pyproject-build-system)
    (propagated-inputs (list 
        python-aiida-core
        ))
    (arguments
    `(#:phases
      (modify-phases %standard-phases
          ;; No tests embedded
          (delete 'check)
          (delete 'sanity-check)
          )))
    (home-page "http://aiida.net")
    (synopsis
     "AiiDA: an automated interactive infrastructure and database for computational science")
    (description
     "@code{AiiDA}: an automated interactive infrastructure and database for
computational science.")
    (license license:expat)))