;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2024 - Pierre-Antoine Bouttier <pierre-antoine.bouttier@univ-grenoble-alpes.fr>
;;; Copyright © 2024 - Dylan Bissuel <dylan.bissuel@univ-lyon1.fr>
;;; Copyright © 2024 - Benjamin Arrondeau <benjamin.arrondeau@univ-grenoble-alpes.fr>

(define-module (custom libxc-cp2k)
  #:use-module (guix)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages commencement)
  #:use-module (gnu packages python)
  #:use-module (guix git-download)
  #:use-module (guix build-system cmake))

;; package based on the one built by @PAB//gricad-guix-packages/common/siesta.scm
;; somehow, it has to redefined with a new label to force its use in cp2k build => to check again
(define-public libxc-cp2k
  (package
    (name "libxc-cp2k")
    (version "6.1.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://github.com/ElectronicStructureLibrary/libxc.git")
              (commit version)
              (recursive? #f)))
        (file-name (git-file-name name version))
        (sha256 (base32 "04nzkw7dlsj8iscdwkj2rnd2120i0mz7f4g7k4h98p7g3wd6nmqy"))))
    (build-system cmake-build-system)
    (arguments
        ;; Add the main config flags for cmake build
        `(#:configure-flags
              (list "-DBUILD_SHARED_LIBS=ON"
                    "-DENABLE_FORTRAN=ON"
                    "-DENABLE_PYTHON=ON")
          #:phases
              (modify-phases %standard-phases
                  ;; Remove the check phase
                  (delete 'check)
          )))
    (inputs
        (list perl 
              gfortran-toolchain
              python))
    (home-page "https://github.com/ElectronicStructureLibrary/libxc")
    (synopsis "Libxc is a library of exchange-correlation functionals for density-functional theory.")
    (description "Libxc is a library of exchange-correlation functionals for density-functional theory. The aim is to provide a portable, well tested and reliable set of exchange and correlation functionals that can be used by a variety of programs.")
    (license license:mpl2.0)
  )
)