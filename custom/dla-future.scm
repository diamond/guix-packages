(define-module (custom dla-future)
  #:use-module (guix)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system cmake)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (gnu packages python)
  #:use-module (gnu packages mpi)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages algebra)
  #:use-module (gnu packages check)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages boost)
  #:use-module (gnu packages pretty-print)
  #:use-module (gnu packages logging)
  #:use-module (gnu packages c)
  #:use-module (gnu packages cpp)
  #:use-module (gnu packages base)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages elf)
  #:use-module (gnu packages commencement)
  #:use-module (gnu packages pkg-config)
  #:use-module (custom umpire)
  #:use-module (custom openblas-openmp))

(define-public pika
  (package
    (name "pika")
    (version "0.26.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://github.com/pika-org/pika")
              (commit "838e3ddabe75540204a7951dc3d6e2ef14312d03")
              (recursive? #t)))
        (file-name (git-file-name name version))
        (sha256 (base32 "1dz41wjsk48riwd7zdh4jmsp9n4nf3f2a8rimk0frwp4nh6w9c4f"))))
    (build-system cmake-build-system)
    (propagated-inputs
        `(("openmpi" ,openmpi)
          ("python3" ,python-sans-pip)
          ("python2" ,python2-minimal)
          ("boost" ,boost)
          ("fmt" ,fmt)
          ("spdlog" ,spdlog)
          ("mimalloc" ,mimalloc)
          ("hwloc" ,hwloc "lib")))
    (arguments 
          ;; Add the main config flags for cmake build
        '(#:configure-flags 
              (list "-DPIKA_WITH_MPI=ON"
                    "-DPIKA_WITH_HIP=OFF"
                    "-DPIKA_WITH_CUDA=OFF")
          #:phases (modify-phases %standard-phases
          ;; Remove check phase
          (delete 'check)
          ;; Add a phase to correct the runpath
          (add-before 'validate-runpath 'fix_runpath
              (lambda _
                  (let* ((testdir (string-append (getenv "out") "/lib/cmake/pika/tests") ))
                  (invoke "ln" "-sf" (string-append testdir "/cxx23_static_call_operator.cpp") (string-append testdir "/cxx23_static_call_operator.cu"))
                  (invoke "ln" "-sf" (string-append testdir "/cxx23_static_call_operator.cpp") (string-append testdir "/cxx23_static_call_operator.hip")) 
              )))
          )))
    (synopsis "pika builds on C++ std::execution with fiber, CUDA, HIP, and MPI support. ")
    (description "pika is a C++ library for concurrency and parallelism. It implements senders/receivers 
      (as proposed in P2300) for CPU thread pools, MPI, and CUDA.")
    (home-page "pikacpp.org")
    (license license:boost1.0)))

(define-public testsweeper
  (package
    (name "testsweeper")
    (version "v2024.05.31")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://github.com/icl-utk-edu/testsweeper")
              (commit "c16ca65ffacc1fa796c2dfba11ea8120c760e8c5")
              (recursive? #t)))
        (file-name (git-file-name name version))
        (sha256 (base32 "0blyjqncp0rllswq26i9canqykaziy553qfn7r85pgp2bpif5kdw"))))
    (build-system cmake-build-system)
    (propagated-inputs
      `(("python3" ,python-sans-pip)))
    (synopsis "C++ testing framework for parameter sweeps")
    (description "TestSweeper handles parsing command line options, iterating over the test space, and printing results. 
      This simplifies test functions by allowing them to concentrate on setting up and solving one problem at a time.")
    (home-page "https://github.com/icl-utk-edu/testsweeper")
    (license license:bsd-3)))

(define-public blaspp
  (package
    (name "blaspp")
    (version "v2024.05.31")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://github.com/icl-utk-edu/blaspp")
              (commit "5d8514a40918cfb9c59ad26e69a502a8f62e02e0")
              (recursive? #t)))
        (file-name (git-file-name name version))
        (sha256 (base32 "02n611n18yvyh4bcqj1zkpd3x669vvmkxm4k7d2lbxqshjhdqz1b"))))
    (build-system cmake-build-system)
    (propagated-inputs
        (list python-sans-pip
              perl
              openblas
              testsweeper))
    (arguments 
          ;; Add the main config flags for cmake build
        '(#:configure-flags 
              (list "-Dblas=openblas")
                   ))
    (synopsis "BLAS++ is a C++ wrapper around CPU and GPU BLAS (basic linear algebra subroutines), developed as part of the SLATE project. ")
    (description "The objective of BLAS++ is to provide a convenient, performance oriented API for development in the C++ language, 
      that, for the most part, preserves established conventions, while, at the same time, takes advantages of modern C++ features, 
      such as: namespaces, templates, exceptions, etc.")
    (home-page "icl.utk.edu/slate/")
    (license license:bsd-3)))

(define-public lapackpp
  (package
    (name "lapackpp")
    (version "v2024.05.31")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://github.com/icl-utk-edu/lapackpp")
              (commit "a27e587a2d085253dea545d4fccb44f17386bf5a")
              (recursive? #t)))
        (file-name (git-file-name name version))
        (sha256 (base32 "1h876nrj5gp7xlzb1r1qxxaha5fhvdsh0b6p5pdhahjfdxxh8mff"))))
    (build-system cmake-build-system)
    (propagated-inputs
        (list python-sans-pip
              perl
              blaspp
              testsweeper))
    (synopsis "LAPACK++ is a C++ wrapper around CPU and GPU LAPACK and LAPACK-like linear algebra libraries, developed as part of the SLATE project. ")
    (description "The objective of LAPACK++ is to provide a convenient, performance oriented API for development in the C++ language, 
      that, for the most part, preserves established conventions, while, at the same time, takes advantages of modern C++ features, 
      such as: namespaces, templates, exceptions, etc.")
    (home-page "icl.utk.edu/slate/")
    (license license:bsd-3)))

(define-public dla-future
  (package
    (name "dla-future")
    (version "v0.6.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://github.com/eth-cscs/DLA-Future")
              (commit "e2a46a21e8b12ae433ada7c99b2539337496af32")
              (recursive? #t)))
        (file-name (git-file-name name version))
        (sha256 (base32 "0h8ky9m5fdj93qx8lcfz9kmjp4hrx07l13x0pvbzggccs718ssgl"))))
    (build-system cmake-build-system)
    (inputs
        `(("openmpi" ,openmpi)
          ("openblas" ,openblas-openmp)
          ("lapack" ,lapack)
          ("scalapack" ,scalapack)
          ("blaspp" ,blaspp)
          ("lapackpp" ,lapackpp)
          ("umpire" ,umpire)
          ("pika" ,pika)
          ("googletest" ,googletest)
          ("gcc" ,gcc "lib")
          ("patchelf" ,patchelf)
          ("mimalloc" ,mimalloc)
          ("hwloc" ,hwloc "lib")
          ("spdlog" ,spdlog)
          ("patchelf" ,patchelf)
          ("pkg-config" ,pkg-config)))
    (arguments 
          ;; Add the main config flags for cmake build
        '(#:configure-flags 
            (list "-DDLAF_WITH_SCALAPACK=ON"
                  (string-append "-DCMAKE_INSTALL_RPATH=" 
                      (string-append (getenv "out") "/lib:")
                      (string-append (assoc-ref %build-inputs "gcc") "/lib:")
                      (string-append (assoc-ref %build-inputs "pika") "/lib:")
                      (string-append (assoc-ref %build-inputs "openmpi") "/lib:")
                      (string-append (assoc-ref %build-inputs "mimalloc") "/lib:")
                      (string-append (assoc-ref %build-inputs "hwloc") "/lib:")
                      (string-append (assoc-ref %build-inputs "spdlog") "/lib:")
                      (string-append (assoc-ref %build-inputs "scalapack") "/lib:")
                      (string-append (assoc-ref %build-inputs "lapackpp") "/lib:")
                      (string-append (assoc-ref %build-inputs "blaspp") "/lib:")
                      (string-append (assoc-ref %build-inputs "openblas") "/lib"))
                  (string-append "-DCMAKE_BUILD_RPATH=" 
                      (string-append (getenv "out") "/lib:")
                      (string-append (assoc-ref %build-inputs "gcc") "/lib:")
                      (string-append (assoc-ref %build-inputs "pika") "/lib:")
                      (string-append (assoc-ref %build-inputs "openmpi") "/lib:")
                      (string-append (assoc-ref %build-inputs "mimalloc") "/lib:")
                      (string-append (assoc-ref %build-inputs "hwloc") "/lib:")
                      (string-append (assoc-ref %build-inputs "spdlog") "/lib:")
                      (string-append (assoc-ref %build-inputs "scalapack") "/lib:")
                      (string-append (assoc-ref %build-inputs "lapackpp") "/lib:")
                      (string-append (assoc-ref %build-inputs "blaspp") "/lib:")
                      (string-append (assoc-ref %build-inputs "openblas") "/lib")))
          ;; Disable parallel build as too much memory is used
          #:parallel-build? #f
          #:phases (modify-phases %standard-phases
              ;; Remove check phase
              (delete 'check)
              ;; Add a phase to remove the calls to check_function_exists as it is buggy
              ;; https://cmake.org/cmake/help/v3.8/module/CheckFunctionExists.html
              (add-before 'configure 'fix_check_fuction_exists
                  (lambda _
                      (invoke "sed" "-i" "s@include(CheckFunctionExists)@include(CheckSymbolExists)@g" "cmake/FindLAPACK.cmake")
                      ;; no symbol available for cblas_dgemm function ... so I changed it to one that exists
                      (invoke "sed" "-i" (string-append "s@check_function_exists(dgemm_@check_symbol_exists(CBLAS_INDEX " (assoc-ref %build-inputs "lapack") "/include/cblas.h@g") "cmake/FindLAPACK.cmake")
                      (invoke "sed" "-i" (string-append "s@check_function_exists(dpotrf_@check_symbol_exists(LAPACK_dpotrf " (assoc-ref %build-inputs "lapack") "/include/lapack.h@g") "cmake/FindLAPACK.cmake")))
              ;; Add a phase to disable download of googletest-populate external library and set it as input package
              (add-after 'fix_check_fuction_exists 'fix_check_gtest
                  (lambda _
                      (invoke "sed" "-i" "s@add_subdirectory(external)@find_package(GTest REQUIRED)@g" "CMakeLists.txt")))
              ;; Add a phase to install the entire lib directory
              (add-after 'fix_check_gtest 'fix_install_lib_dir
                  (lambda _
                      (invoke "sed" "-i" "333i install(FILES ${PROJECT_SOURCE_DIR}/../build/src/libdlaf.core.so" "src/CMakeLists.txt")
                      (invoke "sed" "-i" "334i               ${PROJECT_SOURCE_DIR}/../build/src/libdlaf.c_api.so" "src/CMakeLists.txt")
                      (invoke "sed" "-i" "335i               ${PROJECT_SOURCE_DIR}/../build/src/libdlaf.eigensolver.so" "src/CMakeLists.txt")
                      (invoke "sed" "-i" "336i               ${PROJECT_SOURCE_DIR}/../build/src/libdlaf.permutations.so" "src/CMakeLists.txt")
                      (invoke "sed" "-i" "337i               ${PROJECT_SOURCE_DIR}/../build/src/libdlaf.multiplication.so" "src/CMakeLists.txt")
                      (invoke "sed" "-i" "338i               ${PROJECT_SOURCE_DIR}/../build/src/libdlaf.tridiagonal_eigensolver.so" "src/CMakeLists.txt")
                      (invoke "sed" "-i" "339i               ${PROJECT_SOURCE_DIR}/../build/src/libdlaf.factorization.so" "src/CMakeLists.txt")
                      (invoke "sed" "-i" "340i               ${PROJECT_SOURCE_DIR}/../build/src/libdlaf.solver.so" "src/CMakeLists.txt")
                      (invoke "sed" "-i" "341i               ${PROJECT_SOURCE_DIR}/../build/src/libdlaf.auxiliary.so" "src/CMakeLists.txt")
                      (invoke "sed" "-i" "342i        DESTINATION ${CMAKE_INSTALL_LIBDIR})" "src/CMakeLists.txt")))
                   )))
    (synopsis "DLA-Future is a distributed linear algebra library")
    (description "DLA-Future is a distributed linear algebra library implemented using C++ std::execution 
      P2300 which provides an asynchronous C++ interface, a synchronous C interface, a synchronous ScaLAPACK-like 
      C interface (ScaLAPACK drop-in with a subset of ScaLAPACK arguments. E.g. workspace arguments are not present), 
      and a synchronous ScaLAPACK-like Fortran interface (See DLA-Future-Fortran).")
    (home-page "eth-cscs.github.io/DLA-Future/master/")
    (license license:bsd-3)))