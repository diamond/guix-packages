(define-module (custom quip)
  #:use-module (guix)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system gnu)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (gnu packages python)
  #:use-module (gnu packages mpi)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages shells)
  #:use-module (custom openblas-openmp))

(define-public quip
  (let ((commit "72aaf3fbc9403fe22361bf3fdb4295c516dd1094")
        (version "v0.9.14")
        (revision "1"))
    (package
      (name "quip")
      (version version)
      (source
        (origin
          (method git-fetch)
          (uri (git-reference
                (url "https://github.com/libAtoms/QUIP")
                (commit version)
                (recursive? #t)))
          (file-name (git-file-name name version))
          (sha256 (base32 "17nask73vj0xy797pjz8h8w6frq14czysmcxjni3zvh7gx26zjx3"))))
      (build-system gnu-build-system)
      (propagated-inputs
          (list gfortran
                openblas-openmp
                lapack
                openmpi
                python-sans-pip
                python2-minimal
                perl
                tcsh))
      (arguments 
          '(;; Disable parallel build
            #:parallel-build? #f
            #:phases 
                (modify-phases %standard-phases
                    ;; There is no check phase
                    (delete 'check)
                    ;; Add a phase to fix bash interpreter
                    (add-after 'unpack 'fix_interpreters
                        (lambda* (#:key inputs #:allow-other-keys)
                            (for-each (lambda (f)
                                (invoke "sed" "-i" (string-append "s@/bin/bash@" (which "bash") "@g") f))
                                    (find-files "."))))
                    ;; Add a phase to prepare the Makefile used during the build
                    (add-before 'configure 'prepare_makefile
                        (lambda _
                            (invoke "sed" "-i" "s@QUIPPY_FCOMPILER = gnu95@QUIPPY_FCOMPILER = gfortran@g" "arch/Makefile.linux_x86_64_gfortran")))
                    ;; Replace the configure phase
                    ;; as the user behavior (pressing enter) cannot mimick, the default options are forced
                    ;; to do so, the output file is hard written
                    ;; this file was found by reversing the source code ...
                    (replace 'configure
                        (lambda* (#:key inputs outputs #:allow-other-keys)
                            (setenv "QUIP_ARCH" "linux_x86_64_gfortran_openmp")
                            (setenv "QUIP_ROOT" (getcwd))
                            (setenv "Makefile_dir" (string-append "build/" (getenv "QUIP_ARCH")))
                            (setenv "Makefile_file" (string-append (getenv "Makefile_dir") "/Makefile.inc"))
                            (mkdir-p (getenv "Makefile_dir"))
                            (call-with-output-file (getenv "Makefile_file")
                                (lambda (port)
                                    (format port "# Place to override setting elsewhere, in particular things set in ~a
  # look in ~a for defaults set by arch
  # 
  # F77=gfortran
  # F90=gfortran
  # F95=gfortran
  # CC=gcc
  # CPLUSPLUS=g++
  # FPP=gfortran -E -x f95-cpp-input
  # LINKER=gfortran
  # LIBTOOL=
  # OPTIM=
  # COPTIM=
  # QUIPPY_INSTALL_OPTS=
  # DEBUG=-O0 -g -DDUMP_CORE_ON_ABORT -DDEBUG -fbounds-check
  # DEBUG=
  # CDEBUG=
  MATH_LINKOPTS=~a
  PYTHON=~a
  PIP=~a
  EXTRA_LINKOPTS=
  HAVE_CP2K=0
  HAVE_VASP=0
  HAVE_TB=0
  HAVE_PRECON=1
  HAVE_ONIOM=0
  HAVE_LOCAL_E_MIX=0
  HAVE_QC=0
  HAVE_GAP=0
  HAVE_DESCRIPTORS_NONCOMMERCIAL=0
  HAVE_QR=1
  HAVE_SCALAPACK=0
  HAVE_THIRDPARTY=0
  HAVE_FX=0
  HAVE_SCME=0
  HAVE_MTP=0
  HAVE_MBD=0
  HAVE_TTM_NF=0
  HAVE_CH4=0
  HAVE_NETCDF4=0
  HAVE_MDCORE=0
  HAVE_ASAP=0
  HAVE_KIM=0
  HAVE_CGAL=0
  HAVE_METIS=0
  HAVE_LMTO_TBE=0
  SIZEOF_FORTRAN_T=2
                                  "
                                  (getenv "QUIP_ARCH")
                                  (string-append (getcwd) "arch/Makefile." (getenv "QUIP_ARCH"))
                                  "-llapack -lblas"
                                  (string-append (assoc-ref %build-inputs "python2-minimal") "/bin/python")
                                  (string-append (assoc-ref %build-inputs "python2-minimal") "/bin/pip"))))
                          ;; there is still an option to be added to this file: the size_t_fortran
                          ;; to add it, the config command of the source code is run after commented all the lines of the Makefile.config
                          (invoke "sed" "-i" "1,31 s/^/#/" "Makefile.config")
                          (invoke "sed" "-i" "35,474 s/^/#/" "Makefile.config")
                          (invoke "make" "config")))
                     ;; Add a phase to create a new Makefile for the build
                     (add-before 'build 'patch_makefile
                        (lambda _
                          (copy-file (string-append "arch/Makefile." (getenv "QUIP_ARCH")) (string-append "arch/Makefile." (getenv "QUIP_ARCH") "_forbuild"))
                          (invoke "sed" "-i" "s@include arch/Makefile.linux_x86_64_gfortran@include ../../arch/Makefile.linux_x86_64_gfortran@g" (string-append "arch/Makefile." (getenv "QUIP_ARCH") "_forbuild"))
                        ))
                     ;; Add a phase to copy the missing files for the build
                     (add-after 'patch_makefile 'copy_missing_files
                        (lambda _
                          (copy-file "Makefile.rules" "src/fox/Makefile.rules")
                          (copy-file "Makefile.rules" (string-append "build/" (getenv "QUIP_ARCH") "/Makefile.rules"))
                          (copy-file (string-append "arch/Makefile." (getenv "QUIP_ARCH") "_forbuild") (string-append "build/" (getenv "QUIP_ARCH") "/Makefile." (getenv "QUIP_ARCH")))
                          (copy-file (string-append "arch/Makefile." (getenv "QUIP_ARCH") "_forbuild") (string-append "src/fox/Makefile." (getenv "QUIP_ARCH")))
                          ; (copy-file (string-append "build/" (getenv "QUIP_ARCH") "/Makefile.inc") "src/fox/Makefile.inc")
                        ))
                     ;; Add a phase to patch shell interperter of fox configure
                     (add-after 'copy_missing_files 'fix_fox_build
                        (lambda _
                          (invoke "sed" "-i" (string-append "s@/bin/sh@" (which "sh") "@g") "src/fox/configure")
                        ))
                     ;; Add a phase to patch the path of the source code in the Makefile
                     (add-after 'fix_fox_build 'fix_makefile
                        (lambda _
                          (invoke "sed" "-i" "s#${PWD}/src#${PWD}/source/src#g" "Makefile")
                        ))
                     ;; Replace the install phase
                     (replace 'install
                        (lambda _
                            (let* ((out (getenv "out"))
                                   (incdir (string-append out "/include"))
                                   (bindir (string-append out "/bin"))
                                   (libdir (string-append out "/lib"))
                                   (makdir (string-append out "/lib/Makefile")))
                                (mkdir-p incdir)
                                (mkdir-p libdir)
                                (mkdir-p bindir)
                                (mkdir-p makdir)
                                ;; install .mod file
                                (install-file (string-append "build/" (getenv "QUIP_ARCH") "/quip_unified_wrapper_module.mod")  incdir)
                                ;; install headers
                                (install-file "src/libAtoms/libatoms.h" incdir)
                                (install-file "src/libAtoms/md5.h" incdir)
                                ;; install Mzkefile
                                (install-file (string-append "build/" (getenv "QUIP_ARCH") "/Makefile.inc" ) makdir)
                                (install-file (string-append "build/" (getenv "QUIP_ARCH") "/Makefile.rules" ) makdir)
                                (install-file (string-append "build/" (getenv "QUIP_ARCH") "/Makefile." (getenv "QUIP_ARCH")) makdir)
                                ;; install .a file from quip ...
                                (for-each (lambda (f) (install-file f libdir))
                                        (find-files (string-append "build/" (getenv "QUIP_ARCH")) (lambda (file stat) (ar-file? file))))
                                ;; ... and from fox
                                (for-each (lambda (f) (install-file f libdir))
                                        (find-files (string-append "src/fox/objs." (getenv "QUIP_ARCH") "/lib") (lambda (file stat) (ar-file? file))))
                     )))
                     ;; Add a phase to build and install libquip.a
                     (add-after 'install 'build_install_libquipa
                        (lambda _
                            (setenv "QUIP_INSTALLDIR" (string-append (getenv "out") "/bin"))
                            (invoke "make" "install")
                            (invoke "make" "libquip")
                            (install-file (string-append "build/" (getenv "QUIP_ARCH") "/libquip.a") (string-append (getenv "out") "/lib"))))
            )))
      (synopsis "libAtoms/QUIP molecular dynamics framework")
      (description "The QUIP package is a collection of software tools to carry out molecular dynamics simulations. 
        It implements a variety of interatomic potentials and tight binding quantum mechanics, and is also able to 
        call external packages, and serve as plugins to other software such as LAMMPS, CP2K and also the python 
        framework ASE. Various hybrid combinations are also supported in the style of QM/MM, with a particular focus 
        on materials systems such as metals and semiconductors.")
      (home-page "https://libatoms.github.io")
      (license license:gpl3+))))