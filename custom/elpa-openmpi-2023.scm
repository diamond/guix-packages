(define-module (custom elpa-openmpi-2023)
  #:use-module (guix)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system gnu)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages mpi)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages gcc))

;; This is just an updated version of the elpa and elpa-openmpi packages
;; available on the main guix channel
(define-public elpa-2023
  (package
    (name "elpa")
    (version "2023.11.001")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://elpa.mpcdf.mpg.de/software/"
                                  "tarball-archive/Releases/"
                                  version "/elpa-" version "-patched.tar.gz"))
              (sha256
               (base32
                "0pj1hxv7zivqfs97zmyvayvnbny4inaqph2ryh3ralq6zjd11vk2"))))
    (build-system gnu-build-system)
    (native-inputs
     `(("fortran" ,gfortran)
       ("perl" ,perl)))                 ;for configure and deps
    (inputs
     `(("blas" ,openblas)))
    (arguments
     `(#:configure-flags
       `("--enable-openmp"
         "--with-mpi=no"
         ;; ELPA unfortunately does not support runtime dispatch, so we can
         ;; only enable the "generic" kernels.  See the "Cross compilation"
         ;; section of INSTALL.md.
         "--enable-generic"
         "--disable-sse" "--disable-sse-assembly" ;Require SSE3
         "--disable-avx" "--disable-avx2" "--disable-avx512"
         "--enable-allow-thread-limiting" "--without-threading-support-check-during-build" ; to keep openmp and no threading support for inputs
         ,(string-append "CFLAGS=-O3 "
                         "-funsafe-loop-optimizations -funsafe-math-optimizations "
                         "-ftree-vect-loop-version -ftree-vectorize "
                         ,(let ((system (or (%current-target-system)
                                            (%current-system))))
                            (cond
                             ((or (string-prefix? "x86_64" system)
                                  (string-prefix? "i686" system))
                              "-msse2")
                             (else "")))))
       #:parallel-tests? #f             ;tests are multi-threaded, via BLAS
       #:phases
       (modify-phases %standard-phases
         (add-before 'configure 'patch-header-generation
           (lambda _
             (substitute* "configure"
               (("^  *make.*top_srcdir=\"\\$srcdir\"" &)
                (string-append & " CPP=\"$CPP\"")))
             #t))
         (add-before 'check 'setup-tests
           (lambda _
             ;; Decrease test time and RAM use by computing fewer eigenvalues.
             ;; The flags are (MATRIX-SIZE, EIGENVALUES, BLOCK-SIZE), where
             ;; the default is (500, 250, 16) for C tests and (5000, 150, 16)
             ;; for Fortran.  This also causes several tests to pass that
             ;; otherwise would otherwise fail with matrix size 5000; possibly
             ;; due to floating point tolerances that are too tight.
             (setenv "TEST_FLAGS" "1500 50 16") ;from elpa.spec
             (setenv "OMP_NUM_THREADS" (number->string (parallel-job-count)))
             (substitute* "Makefile"
               ;; Test scripts are generated, patch the shebang
               (("#!/bin/bash") (string-append "#!" (which "sh"))))
             #t)))))
    (home-page "https://elpa.mpcdf.mpg.de")
    (synopsis "Eigenvalue solvers for symmetric matrices")
    (description
     "The ELPA library provides efficient and scalable direct eigensolvers for
symmetric matrices.")
    (license license:lgpl3)))

(define-public elpa-openmpi-2023
  (package (inherit elpa-2023)
    (name "elpa-openmpi-2023")
    (inputs
     `(("mpi" ,openmpi)
       ("scalapack" ,scalapack)
       ,@(package-inputs elpa-2023)))
    (arguments
     (substitute-keyword-arguments (package-arguments elpa-2023)
       ((#:configure-flags cf '())
        `(cons "--with-mpi=yes" (delete "--with-mpi=no" ,cf)))
       ((#:phases phases '%standard-phases)
        `(modify-phases ,phases
           (add-before 'check 'mpi-setup
             (lambda _
               ;; Tests use 2 mpi tasks by default, use our remaining build
               ;; cores as OpenMP threads.
               (setenv "OMP_NUM_THREADS" (number->string
                                          (max (quotient (parallel-job-count) 2)
                                               1)))
               (,%openmpi-setup)))))))
    (synopsis "Eigenvalue solvers for symmetric matrices (with MPI support)")))