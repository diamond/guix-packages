(define-module (custom costa)
  #:use-module (guix)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system cmake)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (gnu packages mpi)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages maths))

(define-public costa
  (package
    (name "costa")
    (version "2.2.2")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://github.com/eth-cscs/COSTA.git")
              (commit version)
              (recursive? #f)))
        (file-name (git-file-name name version))
        (sha256 (base32 "026svdxdihh1zrm4ydwpq417f4y69d8l1v72kj22phmvf1jk484f"))))
    (build-system cmake-build-system)
    (propagated-inputs
     (list openmpi
           gfortran
           scalapack))
    (arguments 
        '(#:phases 
            (modify-phases %standard-phases
                ;; Remove check phase
                (delete 'check)  
          )))
    (synopsis "Distributed Communication-Optimal Shuffle and Transpose Algorithm ")
    (description "COSTA is a communication-optimal, highly-optimised algorithm for data redistribution 
      accross multiple processors, using MPI and OpenMP and offering the possibility to transpose and 
      scale some or all data. Unlike previous redistribution algorithms, COSTA will also propose the 
      relabelling of MPI ranks that minimizes the data reshuffling cost, leaving to users to decide if 
      they want to use it. This way, if the initial and the target data distributions differ up to a 
      rank permutation, COSTA will perform no communication, whereas other algorithms will reshuffle 
      all the data. Thanks to its optimizations, significant speedups will be achieved even if the 
      proposed rank relabelling is not used.")
    (home-page "https://github.com/eth-cscs/COSTA")
    (license license:bsd-3)))