(define-module (custom dp)
  #:use-module (guix)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system gnu)
  #:use-module (guix build utils)
  #:use-module (gnu packages)
  #:use-module (gnu packages algebra)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages commencement)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages m4)
  #:use-module (gnu packages shells)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages bison)
  #:use-module (gnu packages texinfo)
  #:use-module (custom libxc))

(define-public dp
  (package
  (name "dp")
  (version "4.6.5")
  (source (origin
            (method url-fetch)
        (uri (string-append "http://etsf.grenoble.cnrs.fr/dp/license/download/dp-" version ".tgz"))
            (sha256 (base32 "0ijszq7v58mwj4j07lyixi136av81s1ck0dz2nwxh6i53aq58wzd")) ;; version 4.6.5
            ))
  (build-system gnu-build-system)
  (arguments
   `(#:parallel-build? #f
     #:phases
      (modify-phases %standard-phases
        ;; we fix the interpreters that are not patched by the usual phase
        (add-after `unpack `patch-interpreters
            (lambda* (#:key inputs #:allow-other-keys)
                (for-each (lambda (f)
                    (invoke "sed" "-i" (string-append "s@/bin/sh@" (which "sh") "@g") f)
                    (invoke "sed" "-i" (string-append "s@/lib/cpp@" (which "cpp") "@g") f))
                        (find-files "."))))
        ;; There is no configure phase
        (delete `configure)           
        ;; we add the "-cpp" flag to FCFLAGS to not have an error with C comments
        (add-after `patch-interpreters 'add-cpp-flag
            (lambda* (#:key inputs #:allow-other-keys)
                (for-each (lambda (f)
                    (invoke "sed" "-i" "s@-pipe -O3@-pipe -O3 -cpp@g" f))
                        (find-files "lib/libxc"))))
        ;; in the source code, all dependencies are built locally
        ;; here, we only build the one that are not available on the main channel
        ;; and libxc because ...
        (add-before `build `fix-lib-build
            (lambda _
                (invoke "sed" "-i" "s@O3@O3 -fallow-argument-mismatch@g" "make.inc")
                ;; we only build libxc, mfft and etsf_io 
                (invoke "sed" "-i" "s@libblas.a liblapack.a libmfft.a libxc.a libnetcdf.a libetsf_io.a@libxc.a libmfft.a libetsf_io.a@g" "lib/Makefile")
                ;; mandatory to add autoreconf as we use newer version of autotools
                (invoke "sed" "-i" "s@libxc ; ./configure@libxc ; autoreconf ; ./configure@g" "lib/Makefile")
                ;; As we do not build all dependecies locally, we have to link properly
                (invoke "sed" "-i" (string-append "s@$(PWD)/../include@" (assoc-ref %build-inputs "netcdf-fortran") "/include@g") "lib/Makefile")
                (invoke "sed" "-i" (string-append "s@-L$(PWD)@-L" (assoc-ref %build-inputs "netcdf") "/lib@g") "lib/Makefile")
                (setenv "new_ldflag" (string-append (assoc-ref %build-inputs "lapack") "/lib/ -L"
                                                    (assoc-ref %build-inputs "fftw") "/lib/ -L"
                                                    (assoc-ref %build-inputs "netcdf") "/lib/ -L"
                                                    (assoc-ref %build-inputs "netcdf-fortran") "/lib/ -L../lib"))
                (setenv "new_cppflag" (string-append (assoc-ref %build-inputs "lapack") "/include/ -I"
                                                     (assoc-ref %build-inputs "fftw") "/include/ -I"
                                                     (assoc-ref %build-inputs "netcdf") "/include/ -I"
                                                     (assoc-ref %build-inputs "netcdf-fortran") "/include/ -I../include"))
                (invoke "sed" "-i" (string-append "s@-L../lib/@-L" (getenv "new_ldflag") "@g") "lib/Makefile")
                (invoke "sed" "-i" (string-append "s@-I../include/@-I" (getenv "new_cppflag") "@g") "lib/Makefile")
                (invoke "sed" "-i" (string-append "s@-L../lib/@-L" (getenv "new_ldflag") "@g") "src/Makefile")
                (invoke "sed" "-i" (string-append "s@-I../include/@-I" (getenv "new_cppflag") "@g") "src/Makefile")
                (invoke "sed" "-i" (string-append "s@-L../lib/@-L" (getenv "new_ldflag") "@g") "make.inc")
                (invoke "sed" "-i" (string-append "s@-I../include/@-I" (getenv "new_cppflag") "@g") "make.inc")))
        ;; we need to fix the final compilation of libxc and util folders
        (add-after `fix-lib-build `fix-libxc-util-build
            (lambda _
                (invoke "sed" "-i" "s@$(FC) -c $(FCFLAGS) $(FREEFORM) $(CPPFLAGS) $<@$(FC) -c $(FCFLAGS) $(FREEFORM) $(CPPFLAGS) $< -lnetcdff -lnetcdf@g" "src/Makefile")
                (invoke "sed" "-i" "s@$(ILIBS) $(LDFLAGS) $(LIBS)@$(ILIBS) $(LDFLAGS) -lnetcdff $(LIBS)@g" "src/Makefile")
                (invoke "sed" "-i" "s@util ; make@util ; make CC=gcc@g" "Makefile")))
        ;; There is no proper install phase
           (replace 'install
              (lambda* (#:key outputs inputs #:allow-other-keys)
                (let* ((out (assoc-ref outputs "out"))
                       (bindir (string-append out "/bin"))
                       (incdir (string-append out "/include"))
                       (etcdir (string-append out "/etc"))
                       (libdir (string-append out "/lib")))
                       ; we install the dp binary ...
                       (for-each (lambda (f) (install-file f bindir))
                                 (find-files "bin/"))
                       ; ... the lib file ...
                       ; (for-each (lambda (f) (install-file f libdir))
                                 ; (find-files "lib/"))
                       ; ... the include file ...
                       (for-each (lambda (f) (install-file f incdir))
                                 (find-files "include/"))
                       ; ... and the etc
                       (for-each (lambda (f) (install-file f etcdir))
                                 (find-files "etc/")))))
           )))
  (inputs `(("gfortran", gfortran)
            ("lapack", lapack)
            ("gcc" ,gcc)
            ("fftw" ,fftw)
            ("m4" ,m4)
            ("perl" ,perl)
            ("tcsh" ,tcsh)
            ("bison" ,bison)
            ("netcdf-fortran" ,netcdf-fortran)
            ("netcdf", netcdf)
            ("texinfo", texinfo)
            ("autoconf", autoconf)
            ("automake", automake)))
  (synopsis "DP is a Linear Response TDDFT code, in Frequency-Reciprocal and Frequency-Real space, on a Plane Waves basis set")
  (description "
    Definition: Linear Response Time-Dependent Density-Functional Theory (LR-TDDFT) code in Frequency Reciprocal (k-ω) space on a Plane Waves (PW) basis set. Some quantities (the Kohn-Sham polarizability and the exchange correlation kernel) are calculated in Real (r) space
    Purpose: Dielectric and Optical Spectroscopy (Optical Absorption, Reflectivity, Refraction Indices, EELS, IXSS, CIXS, ...).
    Systems: Bulk Solids, Surfaces, Clusters, Molecules, Atoms (through supercells) made of Insulator, Semiconductor and Metal elements.
    Approximations: RPA, ALDA, GW-RPA, LRC, non-local kernels, ..., with and without LF (Local Fields).
")
  (home-page "http://etsf.grenoble.cnrs.fr/dp/")
  (license #f)))