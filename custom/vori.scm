;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2024 - Dylan Bissuel <dylan.bissuel@univ-lyon1.fr>
;;; Copyright © 2024 - Benjamin Arrondeau <benjamin.arrondeau@univ-grenoble-alpes.fr>

(define-module (custom vori)
  #:use-module (guix)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (guix build-system cmake))

(define-public vori
  (package
    (name "vori")
    (version "220621")
    (source
      (origin
        (method url-fetch)
        (uri "https://brehm-research.de/files/libvori-220621.tar.gz")
        (sha256 (base32 "052ck0bj7iwjndp7wc7nd237az8kh8az3ry0y6ndsjw1ck2riyhw"))))
    (build-system cmake-build-system)
    (arguments
        (list
            #:tests? #t
            #:build-type "Release"
            #:phases
                #~(modify-phases %standard-phases
                    ;; Replace check phase
                    (replace `check
                        (lambda* (#:key outputs #:allow-other-keys)
                            (invoke "./test01")))
                    ;; Replace install phase
                    (replace `install
                        (lambda* (#:key outputs #:allow-other-keys)
                            (let* ((out (assoc-ref outputs "out"))
                            (bindir (string-append out "/bin"))
                            (libdir (string-append out "/lib")))
                            (install-file "./test01" bindir) ; Not sure it's needed but it's the only executable so keeping it for now
                            (install-file "./libvori.a" libdir))))
                    ;; Add a phase to change flags used for compilation
                    (add-before 'build 'replace_flags
                        (lambda _
                            (for-each (lambda (f) (invoke "sed" "-i" "s@-O3@-O3 -fPIC@g" f))
                                (find-files "."))))
                    ;; Add a phase to install CMakeLists.txt
                    (add-after 'install 'install_cmakefile
                        (lambda _
                            (install-file "../libvori-220621/CMakeLists.txt" (string-append (getenv "out") "/include/"))))
          )

      ))
    (home-page "https://brehm-research.de/libvori.php")
    (synopsis "Library for Voronoi Integration")
    (description "In principle, libvori enables other programs to include these approaches. 
      The present version of libvori is a very early development version, which is hard-coded 
      to work with the CP2k program package, in which are implemented  methods for Voronoi integration 
      as well as compressed bqb file format . There is no well-defined interface or documentation yet. 
      If you want to use libvori in your code, please have some more patience.")
    (license license:lgpl3)))