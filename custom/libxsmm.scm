;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2024 - Dylan Bissuel <dylan.bissuel@univ-lyon1.fr>
;;; Copyright © 2024 - Benjamin Arrondeau <benjamin.arrondeau@univ-grenoble-alpes.fr>

(define-module (custom libxsmm)
  #:use-module (guix)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system gnu)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (gnu packages python)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages mpi)
  #:use-module (gnu packages base))

(define-public libxsmm
  (package
    (name "libxsmm")
    (version "1.17")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://github.com/libxsmm/libxsmm")
              (commit "617beb4a13baa559b689bdea77dce26a5e983ada")))
        (file-name (git-file-name name version))
        (sha256 (base32 "0garfjzhydaqxl3fbbxm0y0nhpim1il58w71zvjx7488cv0asw0x"))))
    (build-system gnu-build-system)
    (propagated-inputs
        `(("python3" ,python-sans-pip)
          ("python2" ,python2-minimal)
          ("pkg-config" ,pkg-config)
          ("gcc" ,gcc)
          ("gfortran" ,gfortran)
          ("coreutils" ,coreutils)))
    (arguments 
        '(#:phases 
              (modify-phases %standard-phases
                  ;; Remove configure and check phases
                  (delete 'configure)
                  (delete 'check)    
                  ;; Replace build phase to add custom parameters
                  (replace 'build
                      (lambda _
                          (invoke "make" "-j12" (string-append "PREFIX=" (getenv "out")) "INTRINSICS=1" "CXX=g++" "CC=gcc" "FC=gfortran")))
                  ; Add a phase before build to patch the interpreters in Makefile
                  (add-before 'build 'add_interpreters
                      (lambda _
                          (invoke "sed" "-i" (string-append "s@PYTHON3 := $(call which,python3)@PYTHON3 := " (assoc-ref %build-inputs "python3") "/bin/python3" "@g") "Makefile.inc")
                          (invoke "sed" "-i" (string-append "s@CP ?= $(call which,cp)@CP ?= " (assoc-ref %build-inputs "coreutils") "/bin/cp" "@g") "Makefile.inc")
                          (invoke "sed" "-i" (string-append "s@MV ?= $(call which,mv)@MV ?= " (assoc-ref %build-inputs "coreutils") "/bin/mv" "@g") "Makefile.inc")))
                  ;; Replace install phase to add custom parameters
                  (replace 'install
                      (lambda _
                          (invoke "make" (string-append "PREFIX=" (getenv "out")) "INTRINSICS=1" "CXX=g++" "CC=gcc" "FC=gfortran" "install")))  
          )))
    (synopsis "Library for specialized dense and sparse matrix operations, and deep learning primitives. ")
    (description "LIBXSMM is a library for specialized dense and sparse matrix operations as well as for deep learning primitives 
      such as small convolutions. The library is targeting Intel Architecture with Intel SSE, Intel AVX, Intel AVX2, Intel AVX‑512 
      (with VNNI and Bfloat16), and Intel AMX (Advanced Matrix Extensions) supported by future Intel processor code-named Sapphire Rapids. 
      Code generation is mainly based on Just‑In‑Time (JIT) code specialization for compiler-independent performance (matrix 
      multiplications, matrix transpose/copy, sparse functionality, and deep learning).")
    (home-page "libxsmm.readthedocs.io/")
    (license license:bsd-3)))