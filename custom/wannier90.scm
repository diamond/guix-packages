(define-module (custom wannier90)
  #:use-module (guix)
  #:use-module (guix build-system gnu)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages python)
  #:use-module (gnu packages perl)
)

(define-public wannier90
  (package
    (name "wannier90")
    (version "3.1.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://github.com/wannier-developers/wannier90.git")
              (commit (string-append "v" version))
              (recursive? #f)))
        (file-name (git-file-name name version))
        (sha256 (base32 "1wvvaybzdp58hk8ak3xk6z28198511nk1hqzb19q98ib34ipakg0"))))
    (inputs
     (list gcc gfortran lapack perl python-wrapper))
    (build-system gnu-build-system)
    (arguments
      (list 
        #:tests? #f
        #:phases #~(modify-phases %standard-phases
              (replace 'configure
                (lambda* (#:key outputs inputs #:allow-other-keys)
                  (setenv "DESTDIR" (assoc-ref outputs "out")) ; to define where executables
                  (setenv "PREFIX" "")                         ; and libraries will be installed
                  (invoke "cp" "config/make.inc.gfort" "make.inc")
                )
              )
              (replace 'build
                (lambda* (#:key outputs inputs #:allow-other-keys)
                  (invoke "make" "-j" "default" "lib")
                )
              )
              (add-before 'install 'install-includes
                (lambda* (#:key outputs inputs #:allow-other-keys)
                  (for-each
                    (lambda (f) (install-file f (string-append (assoc-ref outputs "out") "/include")))
                    (find-files "src/objp" "\\.mod$")
                  )
                  (for-each
                    (lambda (f) (install-file f (string-append (assoc-ref outputs "out") "/include")))
                    (find-files "src/obj" "\\.mod$")
                  )
                )
              )
        )
      ))
    (home-page "https://wannier.org/")
    (synopsis "Wannier90 is an open-source code for generating maximally-localized Wannier functions")
    (description "Wannier90 is an open-source code for generating maximally-localized Wannier functions and using them to compute advanced electronic properties of materials with high efficiency and accuracy.
    Many electronic structure codes have an interface to Wannier90, and there are several post-processing codes that use the output of Wannier90 for further analysis and calculation. A registry of this Wannier software ecosystem can be found on Github.
    The development of Wannier90 is managed on the Wannier developers GitHub site where you will find details of on-going developments. Continuous integration of the code is provided by GitHub Actions.")
    (license license:gpl2)))