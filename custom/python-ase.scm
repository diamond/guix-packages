(define-module (custom python-ase)
  #:use-module (guix)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system python)
  #:use-module (guix build-system pyproject)
  #:use-module (guix build utils)
  #:use-module (guix git-download)
  #:use-module (guix utils)
  #:use-module (gnu packages)
  #:use-module (gnu packages check)
  #:use-module (gnu packages chemistry)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages python-science)
  #:use-module (gnu packages python-build))

(define-public python-ase
  (package
    (name "python-ase")
    (version "3.23.0")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "ase" version))
       (sha256 (base32 "1bn8zrvashg0cn7g7jdz5a5q4cjg4s27wjpyzl70pncvv0qsm8li"))))
    (build-system pyproject-build-system)
    (propagated-inputs (list python-matplotlib 
                             python-numpy 
                             python-scipy))
    (native-inputs (list python-sans-pip 
                         python-pytest 
                         python-pytest-xdist 
                         python-pytest-mock 
                         spglib
                         python-setuptools
                         python-wheel))
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         ;; Pytest is used for testing ASE and relies on many external codes (quantum-espresso, plumed, ...)
         ; =========================== short test summary info ============================
         ; FAILED ase/test/fio/test_espresso.py::test_pw_input_write_nested_flat - Faile...
         ; FAILED ase/test/fio/test_espresso.py::TestConstraints::test_fix_scaled - Fail...
         ; = 2 failed, 2727 passed, 559 skipped, 5 xfailed, 44 warnings in 154.83s (0:02:34) =
         ;; The two failed tests concern raising exception (see this issue https://gitlab.com/ase/ase/-/issues/1461)
         (delete 'check))))
    (home-page "https://wiki.fysik.dtu.dk/ase/")
    (synopsis "Atomic Simulation Environment: A Python library for working with atoms")
    (description "The Atomic Simulation Environment (ASE) is a set of tools and Python modules 
        for setting up, manipulating, running, visualizing and analyzing atomistic simulations. 
        The code is freely available under the GNU LGPL license.")
    (license license:lgpl2.1)))