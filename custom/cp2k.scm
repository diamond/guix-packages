(define-module (custom cp2k)
  #:use-module (guix)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system cmake)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (gnu packages python)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages shells)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages mpi)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages algebra)
  #:use-module (gnu packages chemistry)
  #:use-module (gnu packages c)
  #:use-module (gnu packages logging)
  #:use-module (custom plumed)
  #:use-module (custom cosma)
  #:use-module (custom dbcsr)
  #:use-module (custom dla-future)
  #:use-module (custom elpa-openmpi-2023)
  #:use-module (custom libint)
  #:use-module (custom libxsmm)
  #:use-module (custom vori)
  #:use-module (custom libxc-cp2k)
  #:use-module (custom pexsi)
  #:use-module (custom pt-scotch-cp2k)
  #:use-module (custom quip)
  #:use-module (custom sirius)
  #:use-module (custom spla)
  #:use-module (custom umpire)
  #:use-module (custom openblas-openmp))

(define-public cp2k
  (package
    (name "cp2k")
    (version "v2024.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://github.com/cp2k/cp2k")
              (commit "077bf32593c66d7a8606997006b97a99204d7b92")))
        (file-name (git-file-name name version))
        (sha256 (base32 "047969pmqbq5a54vzjiihsff5syw53mly56wy4ls21zyvgpx517a"))))
    (build-system cmake-build-system)
    (propagated-inputs
        `(("python3" ,python-sans-pip)
          ("python2" ,python2-minimal)
          ("perl" ,perl)
          ("tcsh" ,tcsh)
          ("gfortran" ,gfortran)
          ("pkg-config" ,pkg-config)
          ("openmpi" ,openmpi)
          ("openblas" ,openblas-openmp)
          ("scalapack" ,scalapack)
          ("dbcsr" ,dbcsr)
          ("fftw" ,fftw)
          ("spglib" ,spglib)
          ("elpa-openmpi-2023" ,elpa-openmpi-2023)
          ("superlu" ,superlu)
          ("vori" ,vori)
          ("plumed" ,plumed)
          ("libint" ,libint)
          ("libxsmm" ,libxsmm)
          ("sirius" ,sirius)
          ("cosma" ,cosma)
          ("libxc-cp2k" ,libxc-cp2k)
          ("spla" ,spla)
          ("quip" ,quip)
          ("mimalloc" ,mimalloc)
          ("spdlog" ,spdlog)
          ("pexsi" ,pexsi)
          ("superlu-dist-cp2k" ,superlu-dist-cp2k)
          ("pt-scotch-cp2k" ,pt-scotch-cp2k)
          ("parmetis" ,pt-scotch-cp2k "metis")
          ("dla-future" ,dla-future)
          ("quip" ,quip)))
    (arguments 
        ;; Add the main config flags for cmake build
        '(#:configure-flags 
            (list "-DCP2K_USE_SPGLIB=ON"
                  "-DCP2K_USE_SUPERLU=ON"
                  "-DCP2K_USE_ELPA=ON"
                  "-DCP2K_USE_VORI=ON"
                  "-DCP2K_USE_PLUMED=ON"
                  "-DCP2K_USE_PLUMED2=ON"
                  "-DCP2K_USE_LIBINT2=ON"
                  "-DCP2K_USE_LIBXSMM=ON"
                  "-DCP2K_USE_SIRIUS=ON"
                  "-DCP2K_USE_COSMA=ON"
                  "-DCP2K_USE_LIBXC=ON"
                  "-DCP2K_USE_SPLA=ON"
                  "-DCP2K_USE_PEXSI=ON"
                  "-DCP2K_USE_QUIP=ON"
                  "-DCP2K_USE_DLAF=ON"
                  ;; and libraries
                  (string-append "-DCP2K_LIBQUIP_INCLUDE_DIRS=" (assoc-ref %build-inputs "quip") "/include")
                  (string-append "-DCP2K_LIBQUIP_LINK_LIBRARIES=" (assoc-ref %build-inputs "quip") "/lib")
                  (string-append "-DCP2K_QUIP_LINK_LIBRARIES=" (assoc-ref %build-inputs "quip"))
                  (string-append "-DCP2K_DLAF_INCLUDE_DIRS=" (assoc-ref %build-inputs "dla-future") "/include")
                  (string-append "-DCP2K_DLAF_LINK_LIBRARIES=" (assoc-ref %build-inputs "dla-future") "/lib")
                  (string-append "-DCP2K_LIBVORI_INCLUDE_DIRS=" (assoc-ref %build-inputs "vori") "/include")
                  (string-append "-DCP2K_LIBSPG_INCLUDE_DIRS=" (assoc-ref %build-inputs "spglib") "/include/spglib")
                  (string-append "-DCOSMA_INCLUDE_DIRS=" (assoc-ref %build-inputs "cosma") "/include")
                  (string-append "-DCOSMA_LIBRARIES=" (assoc-ref %build-inputs "cosma") "/lib")
                  (string-append "-DSIRIUS_INCLUDE_DIRS=" (assoc-ref %build-inputs "sirius") "/include")
                  (string-append "-DSIRIUS_LIBRARIES=" (assoc-ref %build-inputs "sirius") "/lib")
                  (string-append "-DSPLA_LIBRARIES=" (assoc-ref %build-inputs "spla") "/lib")
                  (string-append "-DMPI_INCLUDE_DIRS=" (assoc-ref %build-inputs "openmpi") "/include")
                  (string-append "-DCP2K_PEXSI_INCLUDE_DIRS=" (assoc-ref %build-inputs "pexsi") "/include")
                  (string-append "-DCP2K_PEXSI_LINK_LIBRARIES=" (assoc-ref %build-inputs "pexsi") "/lib")
                  (string-append "-DCP2K_PTSCOTCH_INCLUDE_DIRS=" (assoc-ref %build-inputs "pt-scotch-cp2k") "/include")
                  (string-append "-DCP2K_PTSCOTCH_LINK_LIBRARIES=" (assoc-ref %build-inputs "pt-scotch-cp2k") "/lib")
                  (string-append "-DCP2K_PTSCOTCHPARMETIS_LINK_LIBRARIES=" (assoc-ref %build-inputs "parmetis") "/lib") 
                  (string-append "-DCP2K_SCOTCHMETIS_LINK_LIBRARIES=" (assoc-ref %build-inputs "parmetis") "/lib")
                  (string-append "-DCP2K_METIS_INCLUDE_DIRS=" (assoc-ref %build-inputs "parmetis") "/include")
                  (string-append "-DCP2K_METIS_LINK_LIBRARIES=" (assoc-ref %build-inputs "parmetis") "/lib")
                  ;; enable regtesting
                  "-DCP2K_ENABLE_REGTESTS=ON")
          #:phases (modify-phases %standard-phases
              ;; Replace check phase
              (replace 'check
                  (lambda _
                     (chdir "../source")
                     (setenv "CP2K_DATA_DIR" (string-append (getenv "TMPDIR") "/source/data/"))
                     (setenv "OMPI_MCA_plm_rsh_agent" "")
                     ;; to be meticulous, we should use --mpiranks=4 and --ompthreads=4 flags to run one more test ...
                     (invoke "./tests/do_regtest.py"  "local" "psmp")
                     (chdir "../build")))
              ;; Add a phase to patch search of QUIP package
              (add-before 'configure 'patch_quip
                  (lambda _
                      (invoke "sed" "-i" "s@cp2k_set_default_paths(LIBQUIP)@cp2k_set_default_paths(LIBQUIP \"Quip\")@g" "cmake/modules/FindQuip.cmake")))
              ;; Add a phase to patch search of DLA-Future package
              (add-after 'patch_quip 'patch_dlaf
                  (lambda _  
                      (invoke "sed" "-i" "913,916 s/^/#/" "CMakeLists.txt")))
              ;; Add a phase to patch search of pexsi, metis and pt-scotch package
              (add-after 'patch_dlaf 'patch_pexsi
                  (lambda _ 
                      ;; for pexsi
                      (invoke "sed" "-i" "s@find_package(ptscotch)@find_package(Ptscotch)@g" "cmake/modules/FindPEXSI.cmake")
                      (invoke "sed" "-i" "s@cp2k_include_dirs(PEXSI \"pexsi.hpp\")@cp2k_include_dirs(PEXSI \"ppexsi.hpp\")@g" "cmake/modules/FindPEXSI.cmake")
                      (invoke "sed" "-i" "s@cp2k::PEXSI PROPERTIES@cp2k::PEXSI::pexsi PROPERTIES@g" "cmake/modules/FindPEXSI.cmake")
                      (invoke "sed" "-i" "15 s/^/#/" "cmake/modules/FindPEXSI.cmake")
                      (invoke "sed" "-i" "1554 s/^/#/" "src/CMakeLists.txt")
                      ;; for metis
                      (invoke "sed" "-i" "s@FLEXIBLAS \"metis\"@METIS \"scotchmetisv3\"@g" "cmake/modules/FindMetis.cmake")
                      (invoke "sed" "-i" "s@FFTW3 \"metis.h\"@METIS \"metis.h\"@g" "cmake/modules/FindMetis.cmake")
                      ;; for pt-scotch
                      (invoke "sed" "-i" "s@find_package(Parmetis REQUIRED)@find_package(Metis REQUIRED)@g" "cmake/modules/FindPtscotch.cmake")
                      (invoke "sed" "-i" "s@ptscotchparmetis@ptscotchparmetisv3@g" "cmake/modules/FindPtscotch.cmake")
                      (invoke "sed" "-i" "s@scotchmetis@scotchmetisv3@g" "cmake/modules/FindPtscotch.cmake")))
              ;; Add a phase to patch plumed flag
              (add-after 'patch_dlaf 'patch_plumed
                  (lambda _  
                      (invoke "sed" "-i" "s@PLUMMED@PLUMED@g" "CMakeLists.txt")))
              ;; Add a phase to patch the cmakecache file
              (add-before 'build 'patch_cmakecache
                  (lambda _
                      (invoke "sed" "-i" "s@-DNDEBUG@@g" "CMakeCache.txt")
                      (invoke "sed" "-i" "s@-I;-I/libint2@@g" "CMakeCache.txt")
                      (invoke "sed" "-i" "s@INTERNAL=/libint2@INTERNAL=@g" "CMakeCache.txt")))
              ;; Add a phase to patch links to libxsmm library
              (add-after 'patch_cmakecache 'patch_links_libxsmm
                  (lambda _
                      ;; First, the dependency to cmake_check_build_system of the all target is removed
                      (invoke "sed" "-i" "s@all: cmake_check_build_system@all:@g" "Makefile")
                      ;; Then, the cmake_check_build_system target is triggered manually
                      (invoke "make" "cmake_check_build_system")
                      ;; This enables to patch the files that cause trouble during the build 
                      (setenv "new_rpath" (string-append "libxsmmf.a " (assoc-ref %build-inputs "libxsmm") "/lib/libxsmm.a"))
                      (invoke "sed" "-i" (string-append "s@libxsmmf.a@" (getenv "new_rpath") "@g") "src/CMakeFiles/dbm_miniapp.dir/link.txt")
                      (invoke "sed" "-i" (string-append "s@libxsmmf.a@" (getenv "new_rpath") "@g") "src/CMakeFiles/grid_unittest.dir/link.txt")
                      (invoke "sed" "-i" (string-append "s@libxsmmf.a@" (getenv "new_rpath") "@g") "src/CMakeFiles/grid_miniapp.dir/link.txt")
                      (invoke "sed" "-i" (string-append "s@libxsmmf.a@" (getenv "new_rpath") "@g") "src/CMakeFiles/libcp2k_unittest.dir/link.txt")
                      (invoke "sed" "-i" (string-append "s@libxsmmf.a@" (getenv "new_rpath") "@g") "src/CMakeFiles/memory_utilities_unittest.dir/link.txt")
                      (invoke "sed" "-i" (string-append "s@libxsmmf.a@" (getenv "new_rpath") "@g") "src/CMakeFiles/nequip_unittest.dir/link.txt")
                      (invoke "sed" "-i" (string-append "s@libxsmmf.a@" (getenv "new_rpath") "@g") "src/CMakeFiles/parallel_rng_types_unittest.dir/link.txt")
                      (invoke "sed" "-i" (string-append "s@libxsmmf.a@" (getenv "new_rpath") "@g") "src/CMakeFiles/cp2k-bin.dir/link.txt")
                      (invoke "sed" "-i" (string-append "s@libxsmmf.a@" (getenv "new_rpath") "@g") "src/CMakeFiles/dbt_tas_unittest.dir/link.txt")
                      (invoke "sed" "-i" (string-append "s@libxsmmf.a@" (getenv "new_rpath") "@g") "src/CMakeFiles/xyz2dcd.dir/link.txt")
                      (invoke "sed" "-i" (string-append "s@libxsmmf.a@" (getenv "new_rpath") "@g") "src/CMakeFiles/graph.dir/link.txt")
                      (invoke "sed" "-i" (string-append "s@libxsmmf.a@" (getenv "new_rpath") "@g") "src/CMakeFiles/dumpdcd.dir/link.txt")
                      (invoke "sed" "-i" (string-append "s@libxsmmf.a@" (getenv "new_rpath") "@g") "src/CMakeFiles/dbt_unittest.dir/link.txt")                        
                      (invoke "sed" "-i" (string-append "s@libxsmmf.a@" (getenv "new_rpath") "@g") "src/CMakeFiles/cp2k.dir/link.txt")))
              ;; Add a phase to patch links to pexsi library
              (add-after 'patch_links_libxsmm 'patch_links_pexsi
                  (lambda _
                      ;; This enables to patch the files that cause trouble during the build 
                      (setenv "new_rpath" (string-append "libxsmmf.a " 
                                          (assoc-ref %build-inputs "parmetis") "/lib/libptscotchparmetisv3.so " 
                                          (assoc-ref %build-inputs "parmetis") "/lib/libscotchmetisv3.so "
                                          (assoc-ref %build-inputs "pt-scotch-cp2k") "/lib/libptscotch.so "
                                          (assoc-ref %build-inputs "pt-scotch-cp2k") "/lib/libscotch.so "
                                          (assoc-ref %build-inputs "pt-scotch-cp2k") "/lib/libptscotcherr.so "
                                          (assoc-ref %build-inputs "pt-scotch-cp2k") "/lib/libscotcherr.so "
                                          (assoc-ref %build-inputs "pt-scotch-cp2k") "/lib/libptesmumps.so "
                                          (assoc-ref %build-inputs "superlu-dist-cp2k") "/lib/libsuperlu_dist.a "))
                      (invoke "sed" "-i" (string-append "s@libxsmmf.a@" (getenv "new_rpath") "@g") "src/CMakeFiles/dbm_miniapp.dir/link.txt")
                      (invoke "sed" "-i" (string-append "s@libxsmmf.a@" (getenv "new_rpath") "@g") "src/CMakeFiles/grid_unittest.dir/link.txt")
                      (invoke "sed" "-i" (string-append "s@libxsmmf.a@" (getenv "new_rpath") "@g") "src/CMakeFiles/grid_miniapp.dir/link.txt")
                      (invoke "sed" "-i" (string-append "s@libxsmmf.a@" (getenv "new_rpath") "@g") "src/CMakeFiles/libcp2k_unittest.dir/link.txt")
                      (invoke "sed" "-i" (string-append "s@libxsmmf.a@" (getenv "new_rpath") "@g") "src/CMakeFiles/memory_utilities_unittest.dir/link.txt")
                      (invoke "sed" "-i" (string-append "s@libxsmmf.a@" (getenv "new_rpath") "@g") "src/CMakeFiles/nequip_unittest.dir/link.txt")
                      (invoke "sed" "-i" (string-append "s@libxsmmf.a@" (getenv "new_rpath") "@g") "src/CMakeFiles/parallel_rng_types_unittest.dir/link.txt")
                      (invoke "sed" "-i" (string-append "s@libxsmmf.a@" (getenv "new_rpath") "@g") "src/CMakeFiles/cp2k-bin.dir/link.txt")
                      (invoke "sed" "-i" (string-append "s@libxsmmf.a@" (getenv "new_rpath") "@g") "src/CMakeFiles/dbt_tas_unittest.dir/link.txt")
                      (invoke "sed" "-i" (string-append "s@libxsmmf.a@" (getenv "new_rpath") "@g") "src/CMakeFiles/xyz2dcd.dir/link.txt")
                      (invoke "sed" "-i" (string-append "s@libxsmmf.a@" (getenv "new_rpath") "@g") "src/CMakeFiles/graph.dir/link.txt")
                      (invoke "sed" "-i" (string-append "s@libxsmmf.a@" (getenv "new_rpath") "@g") "src/CMakeFiles/dumpdcd.dir/link.txt")
                      (invoke "sed" "-i" (string-append "s@libxsmmf.a@" (getenv "new_rpath") "@g") "src/CMakeFiles/dbt_unittest.dir/link.txt")                        
                      (invoke "sed" "-i" (string-append "s@libxsmmf.a@" (getenv "new_rpath") "@g") "src/CMakeFiles/cp2k.dir/link.txt")))
              ;; Add a phase to patch links to QUIP library
              (add-after 'patch_links_pexsi 'patch_links_quip
                  (lambda _
                      (setenv "new_rpath" (string-append "libxsmmext.a " (assoc-ref %build-inputs "quip") "/lib/libquip.a"))                       
                      (invoke "sed" "-i" (string-append "s@libxsmmext.a@" (getenv "new_rpath") "@g") "src/CMakeFiles/cp2k.dir/link.txt")))
              ;; Add a phase to patch links to PLUMED library
              (add-after 'patch_links_quip 'patch_links_plumed
                  (lambda _
                      (setenv "new_rpath" (string-append "libquip.a " 
                                          (assoc-ref %build-inputs "plumed") "/lib/libplumed.so "
                                          (assoc-ref %build-inputs "plumed") "/lib/libplumedKernel.so"))                       
                      (invoke "sed" "-i" (string-append "s@libquip.a@" (getenv "new_rpath") "@g") "src/CMakeFiles/cp2k.dir/link.txt")))
                   )))
    (synopsis "Quantum chemistry and solid state physics software package")
    (description "CP2K is a quantum chemistry and solid state physics software package that can perform 
      atomistic simulations of solid state, liquid, molecular, periodic, material, crystal, and biological 
      systems. CP2K provides a general framework for different modeling methods such as DFT using the mixed 
      Gaussian and plane waves approaches GPW and GAPW. Supported theory levels include DFT, MP2, RPA, GW, 
      tight-binding (xTB, DFTB), semi-empirical methods (AM1, PM3, PM6, RM1, MNDO, ...), and classical force 
      fields (AMBER, CHARMM, ...). CP2K can do simulations of molecular dynamics, metadynamics, Monte Carlo, 
      Ehrenfest dynamics, vibrational analysis, core level spectroscopy, energy minimization, and transition 
      state optimization using NEB or dimer method.")
    (home-page "www.cp2k.org")
    (license license:gpl2)))