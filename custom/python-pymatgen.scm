(define-module (custom python-pymatgen)
  #:use-module (guix)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system python)
  #:use-module (guix build-system pyproject)
  #:use-module (guix build utils)
  #:use-module (guix git-download)
  #:use-module (guix utils)
  #:use-module (guix transformations)
  #:use-module (gnu packages)
  #:use-module (gnu packages check)
  #:use-module (gnu packages graph)
  #:use-module (gnu packages serialization)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages python-science)
  #:use-module (gnu packages python-web)
  #:use-module (gnu packages python-build)
  #:use-module (custom python-phonopy)
  #:use-module (custom python-ase)
  #:use-module (custom python-spglib))

(define-public python-monty
  (package
    (name "python-monty")
    (version "2025.1.9")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "monty" version))
       (sha256
        (base32 "0v80y7hr5ns5his4agbzy3p968rbvnq38rk6rcjr5rd13sq81dpd"))))
    (build-system pyproject-build-system)
    (propagated-inputs (list python-coverage
                             python-invoke
                             python-ipython
                             ; python-monty
                             python-msgpack
                             python-numpy
                             python-orjson
                             python-pandas
                             python-pint
                             python-pydantic
                             python-pymongo
                             python-pytest
                             python-pytest-cov
                             python-requests
                             python-ruamel.yaml
                             ; python-sphinx
                             ; python-sphinx-rtd-theme
                             ; python-torch
                             python-tqdm
                             python-types-requests))
    (native-inputs (list python-setuptools python-wheel))
    (arguments
    `(#:phases
      (modify-phases %standard-phases
          ;; No tests embedded
          (delete 'check))))
    (home-page "https://github.com/materialsvirtuallab/monty")
    (synopsis "Monty is the missing complement to Python.")
    (description "Monty is the missing complement to Python.")
    (license #f)))

(define-public python-pymatgen
  (package
    (name "python-pymatgen")
    (version "2025.1.9")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "pymatgen" version))
       (sha256
        (base32 "0a69hccczzqbw007r47m8znv0h2wsyxdnlsdpwc58r1jxlxmv78w"))))
    (build-system pyproject-build-system)
    (propagated-inputs (list python-ase
                             python-beautifulsoup4
                             ; python-boltztrap2
                             ; python-chemview
                             ; python-chgnet
                             ; python-f90nml
                             ; python-fdint
                             ; python-galore
                             python-h5py
                             python-cython
                             ; python-hiphive
                             python-invoke
                             ; python-jarvis-tools
                             python-joblib
                             ; python-matgl
                             python-matplotlib
                             python-monty
                             python-netcdf4
                             python-networkx
                             python-numba
                             python-numpy
                             python-palettable
                             python-pandas
                             python-phonopy
                             python-plotly
                             python-pybtex
                             python-pytest
                             python-pytest-cov
                             ; python-pytest-split
                             python-requests
                             python-ruamel.yaml
                             python-scipy
                             python-seekpath
                             python-spglib
                             ; python-sphinx
                             ; python-sphinx-markdown-builder
                             ; python-sphinx-rtd-theme
                             python-sympy
                             python-tabulate
                             ; python-tblite
                             python-tqdm
                             python-uncertainties
                             ; python-vtk
                             ))
    (arguments
    `(#:phases
      (modify-phases %standard-phases
          ;; python-numpy>=1.25.0 required
          (delete 'sanity-check))))
    (home-page "https://github.com/materialsproject/pymatgen")
    (synopsis
     "Python Materials Genomics is a robust materials analysis code that defines core object representations for structures")
    (description
     "Python Materials Genomics is a robust materials analysis code that defines core
object representations for structures.")
    (license license:expat)))