;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2024 - Dylan Bissuel <dylan.bissuel@univ-lyon1.fr>
;;; Copyright © 2024 - Benjamin Arrondeau <benjamin.arrondeau@univ-grenoble-alpes.fr>

(define-module (custom libint)
  #:use-module (guix)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system cmake)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (gnu packages python)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages algebra)
  #:use-module (gnu packages boost))

(define-public libint
  (package
    (name "libint")
    (version "2.7.0-beta.5")
    (source
      (origin
        (method url-fetch)
        (uri (string-append "https://github.com/cp2k/libint-cp2k/releases/download/v"
                            version "/libint-v" version "-cp2k-lmax-7.tgz"))
        (sha256 (base32 "1qgq98k1s3z84prs3id0kj096byds9nn8z5apa7ffak537d96gjj"))))
    (inputs
     (list eigen 
           boost 
           gfortran
           python-sans-pip
           python2-minimal))
    (build-system cmake-build-system)
    (arguments 
      (list 
          ;; Add the main config flags for cmake build
          #:configure-flags 
              #~(list (string-append "-DCMAKE_INSTALL_PREFIX=" (assoc-ref %outputs "out"))
                      "-DENABLE_FORTRAN=ON"
                      "-DBUILD_SHARED_LIBS=ON")
          ;; Disable parallel build
          #:parallel-build? #f
          ;; Change the folder used for the test phase
          #:test-target "check"
          ;; Disable the test phase
          ;; One variable in tests (LIBINT2_MAX_AM_default1) is not declared (typo ?) -> see if it gets fixed
          #:tests? #f
          #:build-type "Release"))
    (home-page "https://github.com/cp2k/libint-cp2k")
    (synopsis "Libint - a library for the evaluation of molecular integrals of many-body operators over Gaussian functions")
    (description "Libint - a library for the evaluation of molecular integrals of many-body operators over Gaussian functions")
    (license license:gpl3)))