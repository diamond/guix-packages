(define-module (custom cosma)
  #:use-module (guix)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system cmake)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (gnu packages python)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages cpp)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages mpi)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages base)
  #:use-module (gnu packages elf)
  #:use-module (custom costa)
  #:use-module (custom openblas-openmp))

(define-public cosma
  (package
    (name "cosma")
    (version "v2.6.6")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://github.com/eth-cscs/COSMA")
              (commit "a8161530e780d8fa41c43444a96c056d9929998e")
              (recursive? #t)))
        (file-name (git-file-name name version))
        (sha256 (base32 "001cc3kyfwkcxzpcnnxi8azgpyy7jzkkqmmmj39n7i3sh57fmf4g"))))
    (build-system cmake-build-system)
    (propagated-inputs
        `(("openmpi" ,openmpi)
          ("gfortran" ,gfortran)
          ("openblas" ,openblas-openmp)
          ("scalapack" ,scalapack)
          ("cxxopts" ,cxxopts)
          ("costa" ,costa)
          ("python-sans-pip" ,python-sans-pip)
          ("glibc" ,glibc)
          ("gcc" ,gcc "lib")
          ("patchelf" ,patchelf)))
    (arguments 
          ;; Add the main config flags for cmake build
        '(#:configure-flags 
              (list "-DCMAKE_BUILD_TYPE='RelWithDebInfo'"
                    "-DBUILD_SHARED_LIBS=ON"
                    "-DCOSMA_BLAS=OPENBLAS"
                    "-DCOSMA_SCALAPACK=CUSTOM"
                    "-DCMAKE_SCALAPACK=CUSTOM")
          #:phases (modify-phases %standard-phases
          ;; Remove check phase
          (delete 'check)
          ;; Add a phase to install the CMakeLists.txt file
          (add-after 'install 'install_cmakefile
              (lambda _
                  (install-file "../source/src/cosma/CMakeLists.txt" (string-append (getenv "out") "/include/cosma/src/"))))
          ;; Add a phase to correct the runpath of the binaries
          (add-after 'install_cmakefile 'correct-runpath
              (lambda* (#:key native-inputs inputs outputs #:allow-other-keys)
                  (define out (assoc-ref outputs "out"))
                  (define cosma-bin (string-append out "/bin"))
                  (define cosma-lib (string-append out "/lib"))
                  (define glibc-lib (string-append (assoc-ref inputs "glibc") "/lib"))
                  (define gcc-lib (string-append (assoc-ref inputs "gcc") "/lib"))
                  (define ompi-lib (string-append (assoc-ref inputs "openmpi") "/lib"))
                  (define oblas-lib (string-append (assoc-ref inputs "openblas") "/lib"))
                  (define scal-lib (string-append (assoc-ref inputs "scalapack") "/lib"))
                  (define ld.so (string-append (assoc-ref inputs "glibc") "/bin/ld.so"))
                  (define runpath-COSMA 
                      (string-append
                        cosma-bin ":"
                        cosma-lib ":"
                        glibc-lib ":"
                        gcc-lib ":"
                        ompi-lib ":"
                        oblas-lib ":"
                        scal-lib))
                  ;; Change runpath without --force-runpath patchelf option! (@PAB//gricad-guix-packages/common/absuite.scm)
                  (define (patch-elf-interpreter ld rpath file) ; for binaries
                      (make-file-writable file)
                          (format #t "Setting interpreter on '~a'...~%" file)
                          (format #t "Interpreter is '~a'...~%" ld)
                          (invoke "patchelf" "--set-interpreter" ld file)
                          (format #t "Setting RPATH on '~a'...~%" file)
                          (format #t "runpath is '~a'...~%" rpath)
                          (invoke "patchelf" "--set-rpath" rpath  file))
                  ;; Use patchelf
                  (for-each (lambda (f)
                      (patch-elf-interpreter ld.so runpath-COSMA f)#t)
                          (find-files cosma-bin))))
                   )))
    (synopsis "Distributed Communication-Optimal Matrix-Matrix Multiplication Algorithm")
    (description "COSMA is a parallel, high-performance, GPU-accelerated, matrix-matrix multiplication algorithm 
      that is communication-optimal for all combinations of matrix dimensions, number of processors and memory sizes, 
      without the need for any parameter tuning. ")
    (home-page "https://github.com/eth-cscs/COSMA")
    (license license:bsd-3)))