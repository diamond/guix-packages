(define-module (custom python-phonopy)
  #:use-module (guix)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system python)
  #:use-module (guix build-system pyproject)
  #:use-module (guix build-system cmake)
  #:use-module (guix build utils)
  #:use-module (guix git-download)
  #:use-module (guix utils)
  #:use-module (gnu packages)
  #:use-module (gnu packages algebra)
  #:use-module (gnu packages check)
  #:use-module (gnu packages chemistry)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages python-science)
  #:use-module (gnu packages python-build)
  #:use-module (gnu packages version-control)
  #:use-module (gnu packages serialization)
  #:use-module (gnu packages graphviz)
  #:use-module (gnu packages mpi)
  #:use-module (custom python-spglib))

;; One required dependency of phonopy
(define-public python-symfc
  (package
    (name "python-symfc")
    (version "1.1.6")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "symfc" version))
       (sha256
        (base32 "0i5p1hqlwi4xks1x9bq0dqn4b5wadh7qml7lwwv3mb95jbw1h262"))))
    (build-system pyproject-build-system)
    (propagated-inputs (list python-numpy 
                             python-scipy 
                             python-spglib))
    (native-inputs (list python-setuptools python-wheel))
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         ;; No tests
         (delete 'check))))
    (home-page "https://github.com/symfc/symfc")
    (synopsis "Generate symmetrized force constants ")
    (description "Atomic vibrations in crystals are often conveniently described using
     the phonon model. In this model, the crystal potential is expanded into a Taylor 
     series with respect to atomic displacements from their equilibrium positions, and
      the expansion coefficients are referred to as force constants.")
    (license license:bsd-3)))

;; One recommended dependency of phonopy (to plot phonon band structure)
(define-public python-seekpath
  (package
    (name "python-seekpath")
    (version "2.1.0")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "seekpath" version))
       (sha256
        (base32 "1i2jhjc4ikd31v8wkxzfrvhwlv0dlzpkysf3lkafcql2c9wwbkii"))))
    (build-system pyproject-build-system)
    (propagated-inputs (list python-numpy python-spglib python-scipy))
    (native-inputs (list python-black python-pre-commit python-setuptools
                         python-pylint python-pytest python-wheel))
    (home-page "http://github.com/giovannipizzi/seekpath")
    (synopsis "A module to obtain and visualize k-vector coefficients and obtain 
      band paths in the Brillouin zone of crystal structures")
    (description "This package provides a module to obtain and visualize k-vector coefficients and
      obtain band paths in the Brillouin zone of crystal structures.")
    (license license:expat)))

;; One required dependency of cp2k-input-tools
(define-public python-transitions
  (package
    (name "python-transitions")
    (version "0.9.2")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "transitions" version))
       (sha256
        (base32 "01225mlp5922x02lkxcyhfswq1vdn0m065hmxxn3c6flvgdr111g"))))
    (build-system pyproject-build-system)
    (propagated-inputs (list python-pygraphviz python-pytest python-six python-pycodestyle))
    (native-inputs (list python-setuptools python-wheel))
    (home-page "http://github.com/pytransitions/transitions")
    (synopsis "A lightweight, object-oriented Python state machine implementation with many extensions.")
    (description "This package provides a lightweight, object-oriented Python state machine
      implementation with many extensions.")
    (license license:expat)))

;; One recommended dependency of phonopy (to plot phonon band structure)
(define-public python-cp2k-input-tools
  (package
    (name "python-cp2k-input-tools")
    (version "0.9.1")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "cp2k_input_tools" version))
       (sha256
        (base32 "0z5jxzj8vpigpmsfckdx3jymd7b83hyfprrjmb5b2hgspjdj4zdz"))))
    (build-system pyproject-build-system)
    (propagated-inputs (list python-click
                             python-jinja2
                             python-pint
                             python-poetry-dynamic-versioning
                             ;;python-pygls
                             python-pydantic
                             python-ruamel.yaml
                             python-transitions))
    (native-inputs (list python-setuptools))
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         ;; No tests
         (delete 'check)
         (delete 'sanity-check))))
    (home-page "https://github.com/cp2k/cp2k-input-tools")
    (synopsis "Fully validating pure-python CP2K input file tools including preprocessing capabilities")
    (description "Fully validating pure-python CP2K input file parsers including preprocessing capabilities.")
    (license license:expat)))

;; One required dependency of phonopy
(define-public nanobind
  (package
    (name "nanobind")
    (version "v2.4.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://github.com/wjakob/nanobind")
              (commit version)
              (recursive? #t)))
        (file-name (git-file-name name version))
        (sha256 (base32 "1if86ciiqbz9w937jczwcxl1ggwpqz8l7f6qdsnr2y2466r47spl"))))
    (build-system cmake-build-system)
    (propagated-inputs (list cmake 
                             python-scikit-build-core
                             eigen
                             python-numpy
                             python-pytest
                             python-sans-pip))
    (native-inputs (list python-setuptools))
    (arguments 
        ;; Add the main config flags for cmake build
        '(#:configure-flags 
            (list "-DNB_TEST_STABLE_ABI=ON"
                  "-DNB_TEST_SHARED_BUILD=\"$(python3 -c 'import sys; print(int(sys.version_info.minor>=11))')\"")
          #:phases
           (modify-phases %standard-phases
             ;; No tests
             (replace 'check
                (lambda _
                    (invoke "python3" "-m" "pytest"))))
          ))
    (home-page "https://github.com/wjakob/nanobind")
    (synopsis "nanobind: tiny and efficient C++/Python bindings")
    (description "nanobind is a small binding library that exposes C++ types in Python and vice versa. It is reminiscent 
      of Boost.Python and pybind11 and uses near-identical syntax. In contrast to these existing tools, nanobind is more 
      efficient: bindings compile in a shorter amount of time, produce smaller binaries, and have better runtime performance.")
    (license license:bsd-3)))

;; One recommended dependency of phonopy (for polynomial ml potentials)
(define-public python-pypolymlp
  (package
    (name "python-pypolymlp")
    (version "0.5.0")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "pypolymlp" version))
       (sha256
        (base32 "0a9jim6hndbldhgi21g71hrdx6bpb5x9afc1fj5bgysyzb5ry6zc"))))
    (build-system pyproject-build-system)
    (inputs (list python-numpy
                  python-scipy
                  python-pyyaml
                  python-setuptools-scm
                  eigen
                  pybind11
                  openmpi
                  python-scikit-build-core
                  cmake))
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         ;; No tests
         (delete 'check)
         ; (delete 'sanity-check)
         )))
    (home-page "https://github.com/sekocha/pypolymlp")
    (synopsis "Generator of polynomial machine learning potentials")
    (description "A generator of polynomial machine learning potentials.")
    (license license:bsd-3)))

(define-public python-phonopy
  (package
    (name "python-phonopy")
    (version "2.32.0")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "phonopy" version))
       (sha256
        (base32 "18c99scqb3mlyl4mgsz6f0ggl8qbzbx6sxfn42hpvqz453lcg39c"))))
    (propagated-inputs (list python-matplotlib 
                             python-numpy
                             python-h5py 
                             python-scipy
                             python-symfc 
                             python-spglib
                             nanobind
                             python-pyyaml
                             python-h5py
                             python-symfc
                             python-seekpath
                             python-cp2k-input-tools
                             python-pypolymlp))
    (native-inputs (list python-setuptools))
    (build-system pyproject-build-system)
    (home-page "https://phonopy.github.io/phonopy/index.html")
    (synopsis "Phonon code")
    (description "Phonopy is an open source package for phonon calculations at harmonic and quasi-harmonic levels.")
    (license license:bsd-3)))