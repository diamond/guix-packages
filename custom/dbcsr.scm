(define-module (custom dbcsr)
  #:use-module (guix)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system cmake)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (gnu packages python)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages mpi)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages maths))

(define-public dbcsr
  (package
    (name "dbcsr")
    (version "v2.6.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://github.com/cp2k/dbcsr")
              (commit "b4ffddaa13f915c8b47b47fc070cfd0d13483c97")
              (recursive? #t)))
        (file-name (git-file-name name version))
        (sha256 (base32 "02nkncdsn9k9m2q063n524krkq6ll00dh9hsh06ax6x6vid6q9im"))))
    (build-system cmake-build-system)
    (propagated-inputs
     (list python-sans-pip
           python2-minimal
           gfortran
           pkg-config
           openmpi
           lapack))
    (arguments 
        '(#:phases 
              (modify-phases %standard-phases
                  ;; Remove check phase
                  (delete 'check)
          )))
    (synopsis "DBCSR: Distributed Block Compressed Sparse Row matrix library ")
    (description "DBCSR is a library designed to efficiently perform sparse matrix-matrix multiplication, 
      among other operations. It is MPI and OpenMP parallel and can exploit Nvidia and AMD GPUs via CUDA and HIP.")
    (home-page "cp2k.github.io/dbcsr/")
    (license license:gpl2)))