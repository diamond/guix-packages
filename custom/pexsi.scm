(define-module (custom pexsi)
  #:use-module (guix)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system gnu)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (gnu packages python)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages mpi)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages base)
  #:use-module (gnu packages commencement)
  #:use-module (gnu packages shells)
  #:use-module (custom pt-scotch-cp2k)
  #:use-module (custom openblas-openmp))

;; custom version of superlu-dist for pexsi and cp2k
;; this package is highly inspired from the one on the main guix channel
(define-public superlu-dist-cp2k
  (package
    (name "superlu-dist-cp2k")
    (version "6.1.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://github.com/xiaoyeli/superlu_dist.git")
              (commit (string-append "v" version))
              (recursive? #f)))
        (file-name (git-file-name name version))
        (sha256 (base32 "05w6vgzn6mlj45sqlh6qkdgbip7cabqsi69rs7pglnp4zg9r11l5"))))
    (build-system cmake-build-system)
    (native-inputs
        (list tcsh))
    (inputs
        `(("gfortran" ,gfortran)
          ("blas" ,openblas)
          ("combblas" ,combinatorial-blas)))
    (propagated-inputs
        `(("mpi" ,openmpi)                  ;headers include MPI heades
          ("parmetis" ,pt-scotch32 "metis")
          ("pt-scotch" ,pt-scotch32)))
    (arguments
        `(#:parallel-tests? #f              ;tests use MPI and OpenMP
          ;; Add the main config flags for cmake build
          #:configure-flags 
              (list "-DTPL_ENABLE_PARMETISLIB=FALSE"
                    (string-append "-DCMAKE_INSTALL_PREFIX=" (getenv "out"))
                    (string-append "-DCMAKE_INSTALL_LIBDIR=" (getenv "out") "/lib"))
          #:phases (modify-phases %standard-phases
              ;; Add a phase to install another header
              (add-after 'install 'install_more_headers
                  (lambda _
                      (install-file "SRC/superlu_dist_config.h" (string-append (getenv "out") "/include"))))
              ;; Remove check phase
              (delete 'check))))
    (home-page (package-home-page superlu))
    (synopsis "Parallel supernodal direct solver")
    (description
     "SuperLU_DIST is a parallel extension to the serial SuperLU library.
It is targeted for distributed memory parallel machines.  SuperLU_DIST is
implemented in ANSI C, and MPI for communications.")
    (license license:bsd-3)))
           
(define-public pexsi
  (package
    (name "pexsi")
    (version "1.2.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://bitbucket.org/berkeleylab/pexsi/downloads/pexsi_v1.2.0.tar.gz")
        (sha256 (base32 "1p90z2ma7acb7hvriplywyarg01rgb0v2zy83jga5ik6d3nddylb"))))
    (inputs
        `(("parmetis" ,pt-scotch-cp2k "metis")
          ("pt-scotch" ,pt-scotch-cp2k)
          ("gfortran" ,gfortran-toolchain)
          ("superlu" ,superlu-dist-cp2k)
          ("gfortran" ,gfortran)
          ("openblas" ,openblas-openmp)
          ("lapack" ,lapack)
          ("openmpi" ,openmpi)
          ("coreutils" ,coreutils)))
    (build-system cmake-build-system)
    (arguments 
        (list #:phases #~(modify-phases %standard-phases
            ;; Add a phase to fix old MPI routines
            (add-before 'configure 'fix_mpi_routines
                (lambda _
                    (invoke "sed" "-i" "s@MPI_Address@MPI_Get_address@g" "include/pexsi/utility_impl.hpp")
                    (invoke "sed" "-i" "s@MPI_Type_struct@MPI_Type_create_struct@g" "include/pexsi/utility_impl.hpp")
                    (invoke "sed" "-i" "s@MPI_Type_hindexed@MPI_Type_create_hindexed@g" "include/pexsi/utility_impl.hpp")
                    (invoke "sed" "-i" "s@MPI_Errhandler_set@MPI_Comm_set_errhandler@g" "include/pexsi/utility_impl.hpp")
                    (invoke "sed" "-i" "s@MPI_Errhandler_set@MPI_Comm_set_errhandler@g" "include/pexsi/TreeBcast_v2_impl.hpp")))
            ;; Add a phase to patch include <limits> explicitly as required by gcc >= 11
            (add-after 'fix_mpi_routines 'fix_include_limits
                (lambda _
                    (invoke "sed" "-i" "s@#include <numeric>@#include <limits>@g" "include/pexsi/utility_impl.hpp")))
            ;; Add a phase to create the make.inc file
            (add-after 'fix_include_limits 'create_makeinc
                (lambda _ 
                    (copy-file "config/make.inc.linux.gnu" "make.inc")
                    ;; Disable sympack
                    (invoke "sed" "-i" "s@USE_SYMPACK      = 1@USE_SYMPACK      = 0@g" "make.inc")
                    ;; Disable coredumper
                    (invoke "sed" "-i" "s@-I${COREDUMPER_DIR}/include@@g" "make.inc")
                    (invoke "sed" "-i" "s@-DCOREDUMPER@@g" "make.inc")
                    ;; Change compilers
                    (invoke "sed" "-i" "s@CXX          = mpic++@CXX          = mpicxx@g" "make.inc")
                    (invoke "sed" "-i" "s@LOADER       = mpic++@LOADER       = mpicxx@g" "make.inc")
                    ;; Change interpreters
                    (invoke "sed" "-i" (string-append "s@RANLIB       = ranlib@RANLIB       = " (assoc-ref %build-inputs "coreutils") "/bin/touch@g") "make.inc")
                    (invoke "sed" "-i" (string-append "s@CP           = cp@CP           = " (assoc-ref %build-inputs "coreutils") "/bin/cp@g") "make.inc")
                    (invoke "sed" "-i" (string-append "s@RM           = rm@RM           = " (assoc-ref %build-inputs "coreutils") "/bin/rm@g") "make.inc")
                    ;; Fix all paths
                    (invoke "sed" "-i" (string-append "s@PEXSI_DIR       = $(HOME)/Projects/pexsi@PEXSI_DIR       = " (getcwd) "@g") "make.inc")
                    (invoke "sed" "-i" (string-append "s@PEXSI_BUILD_DIR = $(PEXSI_DIR)/build@PEXSI_BUILD_DIR = "  (getenv "out") "@g") "make.inc")
                    (invoke "sed" "-i" (string-append "s@DSUPERLU_DIR  = $(HOME)/Software/SuperLU_DIST_5.2.1@DSUPERLU_DIR  = " (assoc-ref %build-inputs "superlu") "@g") "make.inc")
                    (invoke "sed" "-i" (string-append "s@METIS_DIR     = $(HOME)/Software/metis-5.1.0/build_release@METIS_DIR  = " (assoc-ref %build-inputs "parmetis") "@g") "make.inc")
                    (invoke "sed" "-i" (string-append "s@PARMETIS_DIR  = $(HOME)/Software/parmetis-4.0.3/build_release@PARMETIS_DIR  = " (assoc-ref %build-inputs "parmetis") "@g") "make.inc")
                    (invoke "sed" "-i" (string-append "s@LAPACK_DIR    = $(HOME)/Software/lapack-3.5.0@LAPACK_DIR  = " (assoc-ref %build-inputs "lapack") "@g") "make.inc")
                    (invoke "sed" "-i" (string-append "s@BLAS_DIR      = $(HOME)/Software/OpenBLAS/build_release@BLAS_DIR  = " (assoc-ref %build-inputs "openblas") "@g") "make.inc")
                    (invoke "sed" "-i" (string-append "s@PTSCOTCH_DIR  = $(HOME)/Software/scotch_6.0.0@PTSCOTCH_DIR  = " (assoc-ref %build-inputs "pt-scotch") "@g") "make.inc")
                    (invoke "sed" "-i" "s@-I${DSUPERLU_DIR}/SRC@-I${DSUPERLU_DIR}/include@g" "make.inc")
                    ;; Fix all libraries linking
                    (invoke "sed" "-i" (string-append "s@/usr/lib/gcc/x86_64-linux-gnu/4.8/libgfortran.a@" (assoc-ref %build-inputs "gfortran") "/lib/libgfortran.a@g") "make.inc")
                    (invoke "sed" "-i" "s@${LAPACK_DIR}/liblapack.a@-llapack@g" "make.inc")
                    (invoke "sed" "-i" "s@${BLAS_DIR}/lib/libopenblas.a@-lblas@g" "make.inc")
                    (invoke "sed" "-i" "s@SRC/libsuperlu_dist_5.2.1.a@lib/libsuperlu_dist.a@g" "make.inc")
                    (invoke "sed" "-i" "s@${COREDUMPER_DIR}/lib/libcoredumper.a@@g" "make.inc")
                    (invoke "sed" "-i" "s@-lmetis@-lscotchmetis@g" "make.inc")
                    (invoke "sed" "-i" "s@-lparmetis@-lptscotchparmetis@g" "make.inc")
                    (invoke "sed" "-i" "s@-lscotchmetis@-lscotchmetisv3@g" "make.inc")
                    (invoke "sed" "-i" "s@-lptscotchparmetis@-lptscotchparmetisv3@g" "make.inc")
                    (invoke "sed" "-i" "s@-lmpi_cxx@@g" "make.inc")
                    ;; Change name of output library
                    (invoke "sed" "-i" "s@_${SUFFIX}.a@.a@g" "make.inc")
                    (invoke "sed" "-i" "s@_${SUFFIX}.a@.a@g" "Makefile")
                    ;; Change compilation flags
                    (invoke "sed" "-i" "s@-w -g@-w -g -fPIC@g" "make.inc")))
            ;; Add a phase to copy missing file in the fortran folder
            (add-after 'create_makeinc 'copy_missing_files
                (lambda _
                    (copy-file "src/f_interface.f90" "fortran/f_interface.f90")))
            ;; Add a phase to fix findmetis cmake routine
            (add-after 'copy_missing_files 'fix_findmetis
                (lambda _
                    (invoke "sed" "-i" "160,174 s/^/#/" "cmake/modules/FindMETIS.cmake")
                    (invoke "sed" "-i" "176,182 s/^/#/" "cmake/modules/FindMETIS.cmake")))
            ;; Replace the configure phase
            ;; This phase is useless, but it generates the CMakeLists.txt that is usefull for cp2k
            (replace 'configure
                (lambda _
                    (invoke "cmake" "-Wno-dev"
                        (string-append "-H" (getcwd)) 
                        (string-append "-B" (getcwd) "/../build")
                        (string-append "-DCMAKE_INSTALL_PREFIX=" (getenv "out"))
                        (string-append "-DMETIS_LIBRARIES=" (assoc-ref %build-inputs "parmetis") "/lib/libscotchmetisv3.so")
                        (string-append "-DPARMETIS_LIBRARIES=" (assoc-ref %build-inputs "parmetis") "/lib/libptscotchparmetisv3.so"))))
            ;; Replace build phase to only compile the fortran library
            (replace 'build
                (lambda _
                    (invoke "make" "finstall")))
            ;; Add a phase to install the CMakeLists.txt file
            (add-after 'install 'install_cmakefile
                (lambda _
                    (install-file "../build/src/PEXSIConfig.cmake" (string-append (getenv "out") "/include/pexsi/"))))
              ;; Add a phase to install all libs
              (add-after 'install_cmakefile 'install_libs
                  (lambda _
                      (let* ((out (getenv "out"))
                             (incdir (string-append out "/include"))
                             (libdir (string-append out "/lib"))
                             (srcdir (string-append out "/src")))
                      (for-each (lambda (f) (install-file f incdir))
                          (find-files "include" "\\.mod$"))
                      (for-each (lambda (f) (install-file f libdir))
                          (find-files "lib" "\\.a$"))
                      (for-each (lambda (f) (install-file f srcdir))
                          (find-files "src" "\\.o$")))))
            ;; Remove check phase
            (delete 'check)
        )))
    (home-page "http://www.pexsi.org/")
    (synopsis "A fast method for electronic structure calculation based on Kohn-Sham density functional theory.")
    (description "It efficiently evaluates certain selected elements of matrix functions, e.g., the Fermi-Dirac 
      function of the KS Hamiltonian, which yields a density matrix. It can be used as an alternative to 
      diagonalization methods for obtaining the density, energy and forces in electronic structure calculations. 
      The PEXSI library is written in C++, and uses message passing interface (MPI) to parallelize the computation 
      on distributed memory computing systems and achieve scalability on more than 10,000 processors.")
    (license license:bsd-3)))