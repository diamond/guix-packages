(define-module (custom gmsh)
  #:use-module (guix)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (guix build-system cmake)
  #:use-module (gnu packages fltk)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages mpi)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages gl)
)

;; This package is remotely inspired by the existing GMSH package hosted on the official
;; Guix channel -- see
;;     https://git.savannah.gnu.org/cgit/guix.git/tree/gnu/packages/maths.scm#n3338
;; but it implements a more recent version while maybe lacking features.

(define-public gmsh
  (package
    (name "gmsh")
    (version "4.13.1")
    (source
      (origin
        (method url-fetch)
        (uri "https://gitlab.onelab.info/gmsh/gmsh/-/archive/gmsh_4_13_1/gmsh-gmsh_4_13_1.tar.gz")
        (sha256 (base32 "1klmx867a82m6vggki1w530lqx6gvmrwypvprl00l2f7bd02v5bv"))))
    (inputs
     (list
      fltk
      openblas 
      opencascade-occt
      openmpi
      hdf5
      perl
      libx11
      glu
      mesa
      metis
      libxext
     )
    )
    (build-system cmake-build-system)
    (arguments 
      (list
        #:configure-flags 
          #~(list
              (string-append "-DCMAKE_INSTALL_PREFIX=" (assoc-ref %outputs "out"))
              "-DENABLE_BUILD_DYNAMIC=1"
            )
        #:test-target "unknown"
        #:tests? #f
        #:build-type "Release"
      )
    )
    (home-page "https://gmsh.info/")
    (synopsis "A three-dimensional finite element mesh generator with built-in pre- and post-processing facilities")
    (description " Gmsh is an open source 3D finite element mesh generator with a built-in CAD engine and post-processor. Its design goal is to provide a fast, light and user-friendly meshing tool with parametric input and flexible visualization capabilities. Gmsh is built around four modules (geometry, mesh, solver and post-processing), which can be controlled with the graphical user interface, from the command line, using text files written in Gmsh's own scripting language (.geo files), or through the C++, C, Python, Julia and Fortran application programming interface.")
    (license license:gpl2+)))