(define-module (custom granoo)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system cmake)
  #:use-module (guix download)
  #:use-module (guix svn-download)
  #:use-module (guix packages)
  #:use-module (gnu packages boost)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages image-processing)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages python)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages xml))

(define-public granoo
  (package
  (name "granoo")
  (version "3.0")
    (source
      (origin
        (method svn-fetch)
        (uri (svn-reference
              (url (string-append "https://subversion.renater.fr/anonscm/svn/granoo/tags/" version))
              (revision 6156)
              (password "anonsvn")))
        (sha256 (base32 "12fanbykas18y0ap41wyiqxnvn97zdn7af85jf0xfg4qql2ri97y"))))
  (build-system cmake-build-system)
  (inputs
     `(("boost", boost)
       ("zlib", zlib)
       ("vtk", vtk)
       ("python2", python2-minimal)
       ("python3", python-sans-pip)
       ("libglvnd", libglvnd)
       ("glu", glu)
       ("openblas", openblas)
       ("expat", expat)
       ("double-conversion", double-conversion)
       ("lz4", lz4)
       ("qtbase-5", qtbase-5)
       ("suitesparse", suitesparse)
       ("metis" ,metis)
       ))
  (arguments
   `(#:phases
      (modify-phases %standard-phases
          ;; There is no proper configure phase
          ;; Thus, we create a temporary build folder to compile everything in it
          ;; Then, we have to specify few parameters to the cmake command
          (replace 'configure
            (lambda _
                (mkdir-p "build/")
                (chdir "build")
                (invoke "cmake" "../CMake" (string-append "-DCMAKE_INSTALL_PREFIX=" (getenv "out")))))
        ;; There is no check phase
        (delete 'check)
      )))
  (synopsis "Alloy Theoretic Automated Toolkit (ATAT)")
  (description "The Alloy-Theoretic Automated Toolkit (ATAT)1 is a generic name that refers to a collection of alloy theory
tools developped by Axel van de Walle2 , in collaboration with various research groups.")
  (home-page "https://www.brown.edu/Departments/Engineering/Labs/avdw/atat/")
  (license license:cc-by4.0)))
