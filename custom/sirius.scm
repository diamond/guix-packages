(define-module (custom sirius)
  #:use-module (guix)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system gnu)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages mpi)
  #:use-module (gnu packages python)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages algebra)
  #:use-module (gnu packages chemistry)
  #:use-module (gnu packages autotools)
  #:use-module (custom costa)
  #:use-module (custom umpire)
  #:use-module (custom libxc-cp2k)
  #:use-module (custom spla)
  #:use-module (custom openblas-openmp))

(define-public spfft
  (package
    (name "spfft")
    (version "1.1.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://github.com/eth-cscs/SpFFT.git")
              (commit version)
              (recursive? #f)))
        (file-name (git-file-name name version))
        (sha256 (base32 "0mqjg0l35b59mg1rfxjy7kkm9vhxh8pnan0xppiwlbr30zyl35w5"))))
    (build-system cmake-build-system)
    (propagated-inputs
        (list fftw
              openmpi
              gfortran))
    (arguments 
        '(#:phases 
              (modify-phases %standard-phases
                  ;; Remove check phase
                  (delete 'check))))
    (synopsis "sirius synopsis")
    (description "sirius description")
    (home-page "sirius homepage")
    (license license:gpl3+)))

(define-public sirius
  (package
    (name "sirius")
    (version "7.5.2")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://github.com/electronic-structure/SIRIUS.git")
              (commit version)
              (recursive? #f)))
        (file-name (git-file-name name version))
        (sha256 (base32 "1fdql2npj9009ybiafw7nalar3q6fzkj2rc8xshdlr70wzm9x20d"))))
    (build-system cmake-build-system)
    (propagated-inputs
        (list python-sans-pip
              python2-minimal
              gcc
              openmpi
              openblas-openmp
              scalapack
              hdf5
              fftw
              spglib
              gsl
              pkg-config
              libxc-cp2k
              spfft
              spla
              costa
              umpire))
    (arguments 
        '(
              #:phases (modify-phases %standard-phases
              ;; Remove check phase
              (delete 'check)
              ;; Add a phase to fix issues occuring during the build
              (add-before 'build 'fix_build_sirius
                  (lambda _
                      (invoke "sed" "-i" "s@-std=gnu++17@-std=gnu++17 -nostdinc++@g" "apps/mini_app/CMakeFiles/sirius.scf.dir/flags.make")
                      (invoke "sed" "-i" (string-append "s@CXX_INCLUDES =@CXX_INCLUDES = -I" (assoc-ref %build-inputs "gcc") "/include/c++" " -I" (assoc-ref %build-inputs "gcc") "/include/c++/x86_64-unknown-linux-gnu" "@g") "apps/mini_app/CMakeFiles/sirius.scf.dir/flags.make")
                      (setenv "CPLUS_INCLUDE_PATH" "")
                      (invoke "sed" "-i" "s@std::feclearexcept@feclearexcept@g" "../source/apps/mini_app/sirius.scf.cpp")
                      (invoke "sed" "-i" "s@std::fetestexcept@fetestexcept@g" "../source/apps/mini_app/sirius.scf.cpp")))
              ;; Add a phase to install CMakeLists.txt
              (add-after 'install 'install_cmakefile
                  (lambda _
                      (install-file "../source/src/CMakeLists.txt" (string-append (getenv "out") "/include/sirius/src/"))))
          )))
    (synopsis "Domain specific library for electronic structure calculations ")
    (description "SIRIUS is a domain specific library for electronic structure calculations. 
      It implements pseudopotential plane wave (PP-PW) and full potential linearized augmented 
      plane wave (FP-LAPW) methods and is designed for GPU acceleration of popular community 
      codes such as Exciting, Elk and Quantum ESPRESSO. SIRIUS is written in C++17 with MPI, 
      OpenMP and CUDA/ROCm programming models. SIRIUS is organised as a collection of classes 
      that abstract away the different building blocks of DFT self-consistency cycle.")
    (home-page "https://github.com/electronic-structure/SIRIUS")
    (license license:bsd-3)))