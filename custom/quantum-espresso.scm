(define-module (custom quantum-espresso)
  #:use-module (guix)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system cmake)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (gnu packages algebra)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages mpi)
  #:use-module (gnu packages python)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages wget)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages version-control)
  ; #:use-module (gnu packages ssh)
  #:use-module (custom wannier90)
  #:use-module (custom libxc)
)

(define-public quantum-espresso
  (package
    (name "quantum-espresso")
    ; (version "7.2")
    ; (version "7.3")
    (version "7.3.1")
    (source
      (origin
        (method url-fetch)
        (uri 
          (string-append
            "https://gitlab.com/QEF/q-e/-/archive/qe-" 
            version "/q-e-qe-" version ".tar.gz"))
        ; (sha256 (base32 "1h08101ssxglvvlczha3sfc5qxa54rx33jnr8lssfrlb6jks8j5k")))) ;; 7.2
        ; (sha256 (base32 "0b4p11zz5mfapb9gc3j3r5xii0pnp5mchbpqyinrcsaw67rs1hpd")))) ;; 7.3
        (sha256 (base32 "0i6qh6q365bcs9pyl3871d127rmmgm2bm7mnm3jps5z4vzxbhn1c")))) ;; 7.3.1
    (inputs
     `(
        ("device-xlib" ,device-xlib)
        ("fftw" ,fftw)
        ("gfortran" ,gfortran)
        ("git" ,git)
        ("hdf5-para" ,hdf5-parallel-openmpi)
        ("hdf5-para-f90" ,hdf5-parallel-openmpi "fortran")
        ; ("scalapack" ,scalapack) ; uncomment ?
        ("lapack" ,lapack) ; comment ?
        ("libmbd" ,libmbd)
        ("libxc" ,libxc)
        ("openblas" ,openblas)
        ("openmpi" ,openmpi)
        ; ("openssh" ,openssh)
        ("perl" ,perl)
        ("pkg-config" ,pkg-config)
        ("python" ,python)
        ("wannier90" ,wannier90)
        ("wget" ,wget)
      )
    )
    (build-system cmake-build-system)
    (arguments 
      (list
        #:configure-flags
          #~(list 
              (string-append "-DWANNIER90_ROOT=" (assoc-ref %build-inputs "wannier90"))
              (string-append "-DMBD_ROOT=" (assoc-ref %build-inputs "libmbd"))
              (string-append "-DDEVICEXLIB_ROOT=" (assoc-ref %build-inputs "device-xlib"))
              (string-append "-DQE_DEVICEXLIB_INTERNAL=" "OFF")
              "-DBLA_VENDOR=OpenBLAS"
              (string-append "-DQE_FFTW_VENDOR=" "FFTW3")
              ;; New additions 
              ; "-DCMAKE_C_FLAGS=-fPIE -fPIC"
              ; "-DCMAKE_Fortran_FLAGS=-fPIE -fPIC"
              (string-append "-DCMAKE_INSTALL_PREFIX=" (assoc-ref %outputs "out"))
              "-DQE_ENABLE_MPI=ON"
              "-DQE_ENABLE_OPENMP=ON"
              "-DQE_ENABLE_CUDA=OFF"
              ; "-DQE_LAPACK_INTERNAL=OFF" ; uncomment ?
              ; "-DQE_FFTW_VENDOR=FFTW3"
              ; "-DQE_ENABLE_SCALAPACK=ON" ; uncomment ?
              "-DQE_ENABLE_LIBXC=ON"
              ; (string-append "-DLIBXC_ROOT=" (assoc-ref %build-inputs "libxc"))
              "-DQE_ENABLE_HDF5=ON"
            )
        #:tests? #f ; NEEDS TO DOWNLOAD EXAMPLES ONLINE
        #:build-type "Release"
        #:phases
        #~(modify-phases %standard-phases
          (add-before 'check 'mpi-setup #$%openmpi-setup)
          (add-after 'unpack 'enable-mpicxx
            (lambda* (#:key outputs inputs #:allow-other-keys)
              (substitute* "CMakeLists.txt"
                (("__MPI OMPI_SKIP_MPICXX")
                  "__MPI") ; disabling option ; comment ?
                ; Try uncommenting to see if it compiles and/or impacts performance
                ; ((".*QE_NO_CONFIG_H)")
                ;   "") ;; crashes if uncommented
                (("if.*HAVE_MALLINFO.*)")
                  "if()") ;; disabling option ; comment ?
              )
            )
          )
        )
      )
    )
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; I CAN'T FIND HOW TO USE ALREADY PREBUILT EXTERNAL LIBRARIES WITH GNU BUILD SYSTEM
    ; (build-system gnu-build-system)
    ; (arguments 
    ;   (list
    ;     #:tests? #f ; NEEDS TO DOWNLOAD EXAMPLES ONLINE
    ;     #:configure-flags
    ;       #~(list
    ;           (string-append "--prefix=" (assoc-ref %outputs "out"))
    ;           ; Parallel
    ;           "--enable-parallel"
    ;           "--enable-openmp"
    ;           "--enable-shared"
    ;           ; HDF5
    ;           (string-append "--with-hdf5=" (assoc-ref %build-inputs "hdf5-para-f90"))
    ;           (string-append "HDF5_LIBS=-L" (assoc-ref %build-inputs "hdf5-para") "/lib -lhdf5")
    ;           ; (string-append "--with-hdf5=" (assoc-ref %build-inputs "hdf5"))
    ;           ; (string-append "HDF5_LIBS=-L" (assoc-ref %build-inputs "hdf5") "/lib -lhdf5")
    ;           ; BLAS / LAPACK
    ;           (string-append "BLAS_LIBS=-L" (assoc-ref %build-inputs "openblas") "/lib -lopenblas")
    ;           (string-append "LAPACK_LIBS=-L" (assoc-ref %build-inputs "lapack") "/lib -lopenblas -llapack")
    ;           ; LIBXC
    ;           "--with-libxc=yes"
    ;           (string-append "--with-libxc-prefix=" (assoc-ref %build-inputs "libxc") "/include")
    ;           (string-append "--with-libxc-include=" (assoc-ref %build-inputs "libxc") "/include")
    ;           ; Wannier90
    ;           "--"
    ;         )
    ;     #:phases
    ;       #~(modify-phases %standard-phases
    ;           (add-before 'check 'mpi-setup #$%openmpi-setup)
    ;           (add-after 'unpack 'init-repo ;;; https://gitlab.com/QEF/q-e/-/wikis/Support/Build-without-internet
    ;             (lambda* (#:key outputs inputs #:allow-other-keys)
    ;               (chdir "external")
    ;               (invoke (which "sh") "./initialize_external_repos.sh")

    ;               (chdir "..")
    ;             )
    ;           )
    ;           (replace 'build
    ;             (lambda* (#:key outputs inputs #:allow-other-keys)
    ;               (invoke "make" "-j" "all")
    ;             )
    ;           )
    ;         )
    ;   )
    ; )
    (home-page "https://www.quantum-espresso.org/")
    (synopsis " Electronic-structure calculations and materials modeling at the nanoscale. ")
    (description " Quantum ESPRESSO  Is an integrated suite of Open-Source computer codes for electronic-structure calculations and materials modeling at the nanoscale. It is based on density-functional theory, plane waves, and pseudopotentials.")
    (license license:gpl2)))

(define-public device-xlib
  (package
    (name "device-xlib")
    (version "master") ; Sadly, the specific one needed for Quantum Espresso
    (source
      (origin
        (method url-fetch)
        (uri 
          (string-append
            "https://gitlab.com/max-centre/components/devicexlib/-/archive/master/devicexlib-"
            version ".tar.gz"))
        (sha256 (base32 "18cwkgzrf8mqiz3lzx662ds6l7ykxmhji4ci602sv08p3z4d82p0"))))
    (inputs
     (list openblas lapack gfortran python-wrapper))
    (build-system gnu-build-system)
    (arguments 
      (list
            #:tests? #f
            #:phases #~(modify-phases %standard-phases 
              (replace 'build
                (lambda* (#:key outputs inputs #:allow-other-keys)
                  (invoke "make" "-j" "all")
                )
              )
              (replace 'install
                (lambda* (#:key outputs inputs #:allow-other-keys)
                  (install-file "src/libdevXlib.a" (string-append (assoc-ref outputs "out") "/lib"))
                  (for-each
                    (lambda (f) (install-file f (string-append (assoc-ref outputs "out") "/include")))
                    (find-files "include" "\\.h$")
                  )
                  (for-each
                    (lambda (f) (install-file f (string-append (assoc-ref outputs "out") "/include")))
                    (find-files "src" "\\.mod$")
                  )
                )
              ) 
            )))
    (home-page "https://gitlab.com/max-centre/components/devicexlib")
    (synopsis " deviceXlib, fortran library wrapping device-oriented routines and utilities")
    (description "deviceXlib is a library that wraps device-oriented routines and utilities, such as device data allocation, host-device data transfers.
deviceXlib supports CUDA language, together with OpenACC and OpenMP programming paradigms.
deviceXlib wraps a subset of functions from Nvidia cuBLAS, Intel oneMKL BLAS and AMD rocBLAS libraries.")
    (license license:gpl3+)))

  (define-public libmbd
  (package
    (name "libmbd")
    (version "0.12.8")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://github.com/libmbd/libmbd.git")
              (commit version)))
        (file-name (git-file-name name version))
        (sha256 (base32 "1j8hzfi0j2hn0zc5wrxxy1pcdwmwkmcbndxmsqaplr6snc21mmbj"))))
    (inputs
     (list gfortran openblas lapack openmpi python-wrapper))
    (build-system cmake-build-system)
    (arguments
      (list
        #:configure-flags 
          #~(list
            "-B build"
            (string-append "-DCMAKE_INSTALL_PREFIX=" (assoc-ref %outputs "out"))
          )
        #:tests? #t
        #:build-type "Release"
        #:phases #~(modify-phases %standard-phases
          (add-before 'check 'mpi-setup #$%openmpi-setup)
          (add-after 'unpack 'patch-version
            (lambda* (#:key outputs inputs #:allow-other-keys)
              (invoke "sh" "-c" (string-append "echo \"set(VERSION_TAG " "0.12.8" ")\" > cmake/libMBDVersionTag.cmake")) ; using ",version" fails => setting it by hand 
            )
          )
          (replace 'configure
            (lambda* (#:key inputs outputs configure-flags #:allow-other-keys)
              (apply invoke "cmake" configure-flags)
            )
          )
          (replace 'build
            (lambda* (#:key inputs outputs #:allow-other-keys)
              (invoke "make" "-C" "build" "install")
            )
          )
          (replace 'check
            (lambda* (#:key inputs outputs #:allow-other-keys)
              (invoke "ctest" "--test-dir" "./build")
            )
          )
          (delete 'install)
        )
      ))
    (home-page "https://github.com/libmbd/libmbd")
    (synopsis "libMBD implements the many-body dispersion (MBD) method in several programming languages and frameworks.")
    (description "The Fortran implementation is the reference, most advanced implementation, with support for analytical gradients and distributed parallelism, and additional functionality beyond the MBD method itself. It provides a low-level and a high-level Fortran API, as well as a C API. Furthermore, Python bindings to the C API are provided.
The Python/Numpy implementation is intended for prototyping, and as a high-level language reference.
The Python/Tensorflow implementation is an experiment that should enable rapid prototyping of machine learning applications with MBD.
The Python-based implementations as well as Python bindings to the libMBD C API are accessible from the Python package called pyMBD.
libMBD is included in FHI-aims, Quantum Espresso, DFTB+, and ESL Bundle.")
    (license license:mpl2.0)))