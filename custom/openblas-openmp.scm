(define-module (custom openblas-openmp)
  #:use-module (guix)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system cmake)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages mpi))

(define-public openblas-openmp
  (package/inherit openblas
    (name "openblas-openmp")
    (inputs `(("mpi" ,openmpi)
       ,@(package-inputs openblas)))
    (arguments
     (substitute-keyword-arguments (package-arguments openblas)
       ((#:make-flags flags #~'())
        #~(append (list "USE_OPENMP=1")
                 #$flags))))
    (synopsis "Management suite for data with parallel IO support")))