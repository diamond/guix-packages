(define-module (custom xmgrace)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system python)
  #:use-module (guix build-system pyproject)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (gnu packages algebra)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bison)
  #:use-module (gnu packages c)
  #:use-module (gnu packages image)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages sphinx)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages flex)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages lesstif)
  #:use-module (gnu packages groff)
  #:use-module (gnu packages tex)
  #:use-module (gnu packages texlive)
  #:use-module (gnu packages texinfo)
  #:use-module (gnu packages python-build)
  #:use-module (gnu packages python-xyz))

(define-public linuxdoc-tools
  (package
    (name "linuxdoc-tools")
    (version "0.9.85")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://gitlab.com/agmartin/linuxdoc-tools")
              (commit version)))
        (file-name (git-file-name name version))
        (sha256 (base32 "11mix5xvgx6xiz5dgcjb5266g8izmnj9qi4ajskfpnhzybb4dpxc"))))
    (build-system gnu-build-system)
    (propagated-inputs
     (list opensp
           gcc
           flex
           which
           perl
           groff
           texlive
           texinfo))
    (arguments 
        '(#:configure-flags (list (string-append "CC=gcc"))
          #:phases 
            (modify-phases %standard-phases
              ;; fix the interpreters and the library path
              (add-after 'unpack 'fix_interpreters
                  (lambda* (#:key inputs #:allow-other-keys)
                      (for-each (lambda (f)
                          (invoke "sed" "-i" (string-append "s@/bin/sh@" (which "sh") "@g") f))
                              (find-files "."))))
              ;; fix the interpreters and the library path
              (add-after 'fix_interpreters 'fix-compilation-flags
                  (lambda* (#:key inputs #:allow-other-keys)
                      (for-each (lambda (f)
                          (invoke "sed" "-i" (string-append "s@CFLAGS=\"$(CFLAGS)\" LDFLAGS=\"$(LDFLAGS)\"@CC=gcc CFLAGS=\"$(CFLAGS)\" LDFLAGS=\"$(LDFLAGS)\" LEX=flex@g") f))
                              (find-files "."))))
              ;; There is no tests
              (delete 'check)
          )))
    (synopsis "Convert LinuxDoc SGML source into other formats")
    (description "Convert LinuxDoc SGML source into other formats.
      LinuxDoc sgml is a highly configurable text format for writing documentation, something like 
      html only it's simpler and can be converted to various other formats, including html for websites.")
    (home-page "https://gitlab.com/agmartin/linuxdoc-tools")
    (license license:x11)))

(define-public xmgrace
  (package
  (name "xmgrace")
  (version "5.1.25")
  (source (origin
            (method url-fetch)
        (uri (string-append "https://plasma-gate.weizmann.ac.il/pub/grace/src/grace-latest.tar.gz"))
            (sha256 (base32 "1b523pzy7wss1ymppdvkblc7w0s682ksp4y1fch27xnhgs8vj6km"))))
  (build-system gnu-build-system)
  (inputs (list gcc
                gfortran
                netcdf
                fftw
                libjpeg-turbo
                libpng
                bison
                byacc
                linuxdoc-tools
                motif))
  (arguments
   `(#:phases
      (modify-phases %standard-phases
          ;; Add a phase to fix bash interpreter
          (add-after 'unpack 'fix_interpreters
              (lambda* (#:key inputs #:allow-other-keys)
                  (for-each (lambda (f)
                      (invoke "sed" "-i" (string-append "s@/bin/sh@" (which "sh") "@g") f))
                          (find-files "."))))
          ;; fix the interpreters and the library path
          (add-after 'fix_interpreters 'fix-compilation-flags
              (lambda _
                  (invoke "sed" "-i" "s@cc tmc.c@gcc tmc.c@g" "examples/dotest")))
          ;; Add a phase to fix accent on a
          (add-after 'fix_interpreters 'fix-accent
              (lambda _
                  (invoke "sed" "-i" "177i <!--" "doc/Tutorial.sgml")
                  (invoke "sed" "-i" "180i -->" "doc/Tutorial.sgml")
                  (invoke "sed" "-i" "223i <!--" "doc/Tutorial.html")
                  (invoke "sed" "-i" "226i -->" "doc/Tutorial.html")))
          )))
  (synopsis "GRACE - GRaphing, Advanced Computation and Exploration of data")
  (description "The Alloy-Theoretic Automated Toolkit (ATAT)1 is a generic name that refers to a collection of alloy theory
tools developped by Axel van de Walle2 , in collaboration with various research groups.")
  (home-page "https://plasma-gate.weizmann.ac.il/Grace/")
  (license license:gpl2)))