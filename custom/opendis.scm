(define-module (custom opendis)
  #:use-module (guix)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (guix build-system cmake)
  #:use-module (guix git-download)
  #:use-module (gnu packages algebra)
  #:use-module (gnu packages mpi)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-xyz))

;; It builds without errors -- and it has been tested in guix build
;; and using guix shell.
;; --> https://opendis.github.io/OpenDiS/tutorials/index.html

(define-public opendis
  (package
    (name "opendis")
    (version "0.1.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://github.com/OpenDiS/OpenDiS.git")
              (recursive? #t)
              (commit "v0.1.0")))
        (file-name (git-file-name name version))
        (sha256 (base32 "0ham727crh74axv8qlblmzay5wf9c70yzrvlxf0bayxafhycz14m"))))
    (inputs
     (list 
      fftw
      openmpi 
      python-sans-pip 
      python-numpy 
      python-matplotlib 
      python-networkx
     ))
    (build-system cmake-build-system)
    (arguments
      (list
        #:tests? #t
        #:phases 
        #~(modify-phases %standard-phases 
            (add-before 'check 'mpi-setup #$%openmpi-setup)
            (add-before 'configure 'pre-setup
              (lambda* (#:key inputs outputs #:allow-other-keys)
                (chdir "../source")
              ))
            (replace 'configure
              (lambda _
                (invoke "sh" "configure.sh" "-DSYS=ubuntu")
              ))
            (replace 'build
              (lambda _
                (invoke "cmake" "--build" "build" "-j")
                (invoke "cmake" "--build" "build" "--target" "install")
              ))
            (replace 'install
              (lambda* (#:key inputs outputs #:allow-other-keys)
                ;; In order to be importable by python, packages
                ;; have to be located in GUIX_PYTHONPATH (which
                ;; is in fact all "lib/python3.10/site-packages")
                ;; directory).
                ;; TODO -- change hard-coded 3.10 to automatically
                ;;         adapt to whatever python version.
                (define sitedir (string-append
                                 (assoc-ref outputs "out")
                                 "/lib/python3.10/site-packages"))
                (copy-recursively "lib"
                  (string-append sitedir "/"))
                (copy-recursively "core/pydis/python/pydis"
                  (string-append sitedir "/pydis"))
                (copy-recursively "core/exadis/python"
                  (string-append sitedir "/"))
                (copy-recursively "python/framework/"
                  (string-append sitedir "/framework"))  
              ))
            (replace 'check
              (lambda* (#:key inputs #:allow-other-keys)
                (chdir "examples/02_frank_read_src")
                (invoke "python3" "-i" "test_frank_read_src_pydis.py")
                (invoke "python3" "-i" "test_frank_read_src_exadis.py")
                (invoke "python3" "-i" "test_data_convert.py")
                (invoke "python3" "-i" "test_frank_read_src_pydis_exadis.py")
                (chdir "../03_binary_junction")
                (invoke "python3" "-i" "test_binary_junction_pydis.py")
                (invoke "python3" "-i" "test_binary_junction_exadis.py")
                ;; The following test is extremely long and should perhaps
                ;; not be performed?
                (chdir "../10_strain_hardening")
                ;; Rather than aiming for max_strain=0.01 (>> 10 hours),
                ;; setting it to 0.00005 reduces it to ~5-6 minutes.
                (substitute* "test_strain_hardening_exadis.py"
                  (("max_strain=0.01")
                    "max_strain=0.00005"
                  ))
                (invoke "python3" "-i" "test_strain_hardening_exadis.py")
                (chdir "../..")
              ))
          )
      )
    )
    (home-page "https://opendis.github.io/OpenDiS")
    (synopsis "Open Dislocation Simulator")
    (description "The OpenDiS Project is a community-driven initiative aimed at developing a robust open-source code framework and a code development platform for dislocation dynamics (DD) simulations. Our mission is to provide a high-performance, accessible, configurable, and extensible tool enabling researchers to explore the intricate world of dislocation lines and their impact on materials behavior.")
    (license license:bsd-3))) 