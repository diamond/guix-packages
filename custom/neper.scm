(define-module (custom neper)
  #:use-module (guix)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (guix build utils)
  #:use-module (guix build-system cmake)
  #:use-module (gnu packages gawk)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages graphics)
  #:use-module (gnu packages plotutils)
  #:use-module (gnu packages texlive))

(define-public neper
  (package
    (name "neper")
    (version "4.9.0")
    (source
      (origin
        (method url-fetch)
        (uri (string-append "https://github.com/neperfepx/neper/archive/refs/tags/v" version ".tar.gz"))
        (sha256 (base32 "0nz2iw4j1pavp0j9m73y15q38nid58cg6mnbw4firgvdvv06z2mi"))))
    (inputs
     (list 
      gawk 
      gsl 
      gmsh      ; explicit when pack
      povray    ; explicit when pack
      asymptote ; explicit when pack
      texlive
      nlopt 
      scotch 
     )
    )
    (build-system cmake-build-system)
    (arguments 
      (list 
        #:configure-flags 
          #~(list 
              (string-append "-DCMAKE_INSTALL_PREFIX=" (assoc-ref %outputs "out"))
              (string-append "-DCMAKE_INSTALL_COMPLETION_PREFIX=" (assoc-ref %outputs "out") "/share")
              "-DHAVE_GSL=yes"
              "-DHAVE_OPENMP=yes"
            )
        #:phases
        #~(modify-phases %standard-phases  
            (add-after 'unpack 'post-unpack
              (lambda* (#:key outputs inputs #:allow-other-keys)
                (chdir "src")
              )
            )
          )
        #:test-target "test"
        ;; https://neper.info/doc/tutorials/simple_model.html
        ; #:tests? #t
        #:tests? #f
        ;; TESTS SUMMARY
          ; 99% tests passed, 5 tests failed out of 476

          ; Total Test time (real) = 126.31 sec

          ; The following tests FAILED:
	        ; 176 - M/part (Failed)
	        ; 177 - M/part2 (Failed)
	        ; 178 - M/part_dim2 (Failed)
	        ; 224 - V/asymptote (Failed)
	        ; 340 - V/povray (Failed)
        ;; END OF TESTS SUMMARY
        #:build-type "Release"
      )
    )
    (home-page "https://neper.info/")
    (synopsis "Neper: Polycrystal Generation and Meshing")
    (description "Neper is a free / open source software package for polycrystal generation and meshing. It can be used to generate polycrystals with a wide variety of morphological properties, from very simple morphologies (simple tessellations, grain-growth microstructures, …) to complex, multiphase or multiscale microstructures that involve grain subdivisions. The resulting tessellations can be meshed into high-quality meshes suitable for finite-element simulations.")
    (license license:gpl3)))
