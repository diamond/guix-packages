(define-module (custom pt-scotch-cp2k)
  #:use-module (guix)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system cmake)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (gnu packages mpi)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages flex)
  #:use-module (gnu packages bison)
  #:use-module (gnu packages gcc))

;; copy-paste of the package from the main channel
;; and a phase is added to install the ptscotchConfig.cmake
(define-public pt-scotch-cp2k
  (package
    (name "pt-scotch-cp2k")
    (version "7.0.4")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://gitlab.inria.fr/scotch/scotch.git")
              (commit (string-append "v" version))
              (recursive? #f)))
        (file-name (git-file-name name version))
        (sha256 (base32 "0rbc51albpd2923dkirpkj8rfkic6rsvwqqnv1mmsk391zhk3amr"))
       (modules '((guix build utils)))
       (snippet
        #~(substitute* "src/libscotchmetis/library_parmetis.h"
            (("typedef DUMMYINT SCOTCH_Num" all)
             ;; 'DUMMYINT' is typically replaced by 'int32_t'.  Include
             ;; <stdint.h> to get that type definition.
             (string-append "#include <stdint.h>\n" all "\n"))))))
    (build-system cmake-build-system)
    (inputs
     (list zlib))
    (native-inputs
     (list flex bison gfortran))
    (propagated-inputs
     (list openmpi))
    (outputs '("out" "metis"))
    (arguments
     `(#:configure-flags '("-DBUILD_SHARED_LIBS=YES" "-DINTSIZE=32"
                           "-DBUILD_PTSCOTCH=ON")
       #:phases
       (modify-phases %standard-phases
         (add-before 'check 'mpi-setup
             ,%openmpi-setup)
         (add-after 'install 'install-metis
           (lambda* (#:key outputs #:allow-other-keys)
             (let* ((out    (assoc-ref outputs "out"))
                    (metis  (assoc-ref outputs "metis"))
                    (prefix (string-length out)))
               (for-each (lambda (file)
                           (let ((target (string-append
                                          metis
                                          (string-drop file prefix))))
                             (mkdir-p (dirname target))
                             (rename-file file target)))
                         (find-files out "metis")))))
         (add-after 'install-metis 'install-ptscotchConfig
             (lambda _
                (copy-file "../build/src/SCOTCHConfig.cmake" "../build/src/ptscotchConfig.cmake")
                (install-file "../build/src/ptscotchConfig.cmake" (string-append (getenv "out") "/include/pt-scotch/")))))))
    (home-page "https://www.labri.fr/perso/pelegrin/scotch/")
    (properties
     `((release-monitoring-url
        . "https://gitlab.inria.fr/scotch/scotch/-/releases")))
    (synopsis "Programs and libraries for graph algorithms")
    (description "SCOTCH is a set of programs and libraries which implement
the static mapping and sparse matrix reordering algorithms developed within
the SCOTCH project.  Its purpose is to apply graph theory, with a divide and
conquer approach, to scientific computing problems such as graph and mesh
partitioning, static mapping, and sparse matrix ordering, in application
domains ranging from structural mechanics to operating systems or
bio-chemistry.")
    ;; See LICENSE_en.txt
    (license license:cecill-c)))