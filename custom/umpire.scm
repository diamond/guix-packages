(define-module (custom umpire)
  #:use-module (guix)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system cmake)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (gnu packages python)
  #:use-module (gnu packages shells))

(define-public umpire
  (package
    (name "umpire")
    (version "v2024.02.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://github.com/LLNL/Umpire.git")
              (commit version)
              (recursive? #t)))
        (file-name (git-file-name name version))
        (sha256 (base32 "0vlagm0b2j5nz7l35b0gfya533pp513qy2rdf1qxfvax2fahd1bh"))))
    (build-system cmake-build-system)
    (propagated-inputs
        (list python-sans-pip
              python2-minimal
              zsh))
    (synopsis "An application-focused API for memory management on NUMA & GPU architectures ")
    (description "Umpire is a resource management library that allows the discovery, provision, 
      and management of memory on machines with multiple memory devices like NUMA and GPUs.")
    (home-page "https://github.com/LLNL/Umpire")
    ;; should be MIT licence
    (license license:ogl-psi1.0)))