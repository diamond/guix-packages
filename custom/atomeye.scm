(define-module (custom atomeye)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system gnu)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages shells)
  #:use-module (gnu packages python)
  #:use-module (gnu packages readline)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages image)
  #:use-module (gnu packages maths)
  #:use-module (custom quip))

(define-public atomeye
    (let ((commit "c418eb2553f6793460d4a956236fc698c39fbe74")
        (version "2017-03-16")
        (revision "1"))
      (package
      (name "atomeye")
      (version version)
      (source (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://github.com/jameskermode/AtomEye")
              (commit commit)))
        (file-name (git-file-name name version))
        (sha256 (base32 "1hc5s8wcy944s2mi3p34q8cw9la1iy7a86c1x1198lii29jh9v1y"))))
      (build-system gnu-build-system)
      (inputs (list gfortran
                    netcdf
                    lapack
                    openblas
                    python2-minimal
                    readline
                    gsl
                    libx11
                    libxext
                    libxpm
                    libpng
                    ijg-libjpeg
                    curl
                    quip))
      (arguments
       `(#:phases
          (modify-phases %standard-phases
            ; We need to manually activate QUIP dependency ...
            (add-before 'build 'setup-quip
                (lambda _
                    ; first, setup env variables
                    (setenv "QUIP_ROOT" (assoc-ref %build-inputs "quip"))
                    (setenv "QUIP_ARCH" "linux_x86_64_gfortran_openmp")
                    ; then, patch the Makefile ...
                    ; we force the use of QUIP with our env variables and folder architecture
                    (invoke "sed" "-i" "1,2 s/^/#/" "Makefile")
                    (invoke "sed" "-i" "4 s/^/#/" "Makefile")
                    (invoke "sed" "-i" "6 s/^/#/" "Makefile")
                    (invoke "sed" "-i" "10,12 s/^/#/" "Makefile")
                    (invoke "sed" "-i" "s@/arch@/bin@g" "Makefile")
                    (invoke "sed" "-i" "s@/build/${QUIP_ARCH}@/lib@g" "Makefile")
                    (invoke "sed" "-i" "s@${QUIP_ROOT}/build/${QUIP_ARCH}@${QUIP_ROOT}/lib@g" "Makefile.atomeye")
                    ; We also need to copy one QUIP makefile
                    ; and to patch it as we do not copy the entire source code of QUIP
                    (copy-file (string-append (assoc-ref %build-inputs "quip") "/lib/Makefile/Makefile." (getenv "QUIP_ARCH")) (string-append "Makefile." (getenv "QUIP_ARCH")))
                    (invoke "sed" "-i" "33 s/^/#/" (string-append "Makefile." (getenv "QUIP_ARCH")))
                    (invoke "sed" "-i" "6,7 s/^/#/" "Makefile.atomeye")
                    ; Finally, we need to set the compiler and linker to the ones used for QUIP
                    (invoke "sed" "-i" "s@FC = ${F77}@FC = gfortran@g" "Makefile.atomeye")
                    (invoke "sed" "-i" "s@LD = ${LINKER}@LD = gfortran@g" "Makefile.atomeye")))
            ; We still need to tweak the source code as we need SIZEOF_FORTRAN_T for the QUIP dependency
            ; Thus, we patch two files, one for the binary and one for the static library 
            (add-after 'setup-quip 'patch-source-code
                (lambda _
                    (invoke "sed" "-i" "2401i #define SIZEOF_FORTRAN_T 2" "Atoms/Config.c")
                    (invoke "sed" "-i" "8i #define SIZEOF_FORTRAN_T 2" "A3/atomeyelib.h")))
            ; There is no clear install instructions
            ; Thus, we create one ourselves
            (replace 'install
              (lambda* (#:key outputs inputs #:allow-other-keys)
                (let* ((out (assoc-ref outputs "out"))
                       (bindir (string-append out "/bin")))
                       ;; install bash scripts
                       (install-file "bin/A" bindir))))
            ; The above commands have compiled and installed the binary
            ; Now, we do the same for the static libraries
            (add-after 'install 'make_install_atomeyelib
              (lambda* (#:key outputs inputs #:allow-other-keys)
                (let* ((out (assoc-ref outputs "out"))
                       (libdir (string-append out "/lib")))
                       ;; first, clean and make the lib
                       (invoke "make" "clean")
                       (invoke "make" "atomeyelib")
                       ;; install static libraries
                       (for-each (lambda (f) (install-file f libdir))
                                 (find-files "lib/" "\\.a")))))
            ; Finally, we install what is left (doc, headers, ...)
            (add-after 'make_install_atomeyelib 'finish_install
              (lambda* (#:key outputs inputs #:allow-other-keys)
                (let* ((out (assoc-ref outputs "out"))
                       (incdir (string-append out "/include"))
                       (docdir (string-append out "/share/doc")))
                       ; we install the header files ...
                       (for-each (lambda (f) (install-file f incdir))
                                 (find-files "include/" "\\.h"))
                       ; ... and the doc
                       (for-each (lambda (f) (install-file f docdir))
                                 (find-files "doc/")))))
            ; There is no configure nor check phases
            (delete 'configure)
            (delete 'check))))
      (synopsis "http://li.mit.edu/Archive/Graphics/A/")
      (description "AtomEye is free atomistic visualization software for all major UNIX platforms.
The functionalities of AtomEye include: parallel and perspective projections with
full three-dimensional navigation; customizable bond and coordination number
calculation; colour-encoding of arbitrary user-defined quantities; local atomic
strain invariant; coloured atom tiling and tracing; up to 16 cutting planes;
periodic boundary condition translations; high-quality JPEG, PNG and EPS
screenshots; and animation scripting. 

!! Caution !! This package is based on a fork of Atomeye (https://github.com/jameskermode/AtomEye)")
      (home-page "https://atomsk.univ-lille.fr//")
      (license #f))))