(define-module (custom spla)
  #:use-module (guix)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system cmake)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages mpi)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages maths)
  #:use-module (custom openblas-openmp))

(define-public spla
  (package
    (name "spla")
    (version "v1.6.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://github.com/eth-cscs/spla.git")
              (commit version)
              (recursive? #t)))
        (file-name (git-file-name name version))
        (sha256 (base32 "0sgzg8yl84d43323d7d88zrm2ayywxvzb1xi3fy1gmcmw8hgblbw"))))
    (build-system cmake-build-system)
    (propagated-inputs
        `(("openmpi" ,openmpi)
          ("gfortran" ,gfortran)
          ("openblas" ,openblas-openmp)))
    (arguments 
        ;; Add the main config flags for cmake build
        '(#:configure-flags 
            (list "-DSPLA_OMP=ON"
                  "-DSPLA_GPU_BACKEND=OFF"
                  "-DSPLA_FORTRAN=ON")
          #:phases (modify-phases %standard-phases
              ;; Remove check phase
              (delete 'check)
          )))
    (synopsis "Specialized Parallel Linear Algebra, providing distributed GEMM functionality for specific matrix distributions with optional GPU acceleration. ")
    (description "SPLA provides specialized functions for linear algebra computations with a C++ and C interface, 
      which are inspired by requirements in computational material science codes.")
    (home-page "https://github.com/eth-cscs/spla")
    (license license:bsd-3)))

;; This allows you to run guix shell -f spla.scm.
;; Remove this line if you just want to define a package.
spla