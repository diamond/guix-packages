(define-module (custom xcrysden)
  #:use-module (guix)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (guix build-system copy)
  #:use-module (guix build-system gnu)
  #:use-module (guix git-download)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bootstrap)
  #:use-module (gnu packages commencement)
  #:use-module (gnu packages elf)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages tcl)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages algebra)
  #:use-module (gnu packages xorg))

(define-public xcrysden
  (package
    (name "xcrysden")
    (version "1.6.2")
    (source
      (origin
        (method url-fetch)
        (uri (string-append "http://www.xcrysden.org/download/xcrysden-" version ".tar.gz"))
        (sha256 (base32 "0gsh9w5fjv5ka0xa95mfmmydsc1s0r70xlbz89dimv4bb7p3c5w1"))))
    (inputs
     (list
      bwidget-toolkit
      gfortran 
      tcl 
      tk-with-sources 
      mesa 
      glu 
      fftw 
      togl2-source
      libxmu
     ))
    (native-inputs
     (list util-linux)) ;; To be able to use "more" during configure
    (build-system gnu-build-system)
    (arguments 
      (list
        #:phases
        #~(modify-phases %standard-phases
            (replace 'configure
              (lambda* (#:key outputs inputs #:allow-other-keys)
                (invoke "cp" "system/Make.sys-shared" "Make.sys")
                (substitute* "Make.sys"
                  (("TCL_LIB.*= -ltcl")
                   (string-append "TCL_LIB = -L" (assoc-ref inputs "tcl") "/lib -ltcl"))
                  (("TK_LIB.*= -ltk")
                   (string-append "TK_LIB = -L" (assoc-ref inputs "tk-with-sources") "/lib -ltk"))
                  (("TOGL_LIB.*= -lTogl")
                   (string-append "TOGL_LIB = -L" (assoc-ref inputs "togl2-source") "/lib/Togl2.0 -lTogl2.0"))
                  (("GLU_LIB.*= -lGLU")
                   (string-append "GLU_LIB = -L" (assoc-ref inputs "glu") "/lib -lGLU"))
                  (("GL_LIB.*= -lGL")
                   (string-append "GL_LIB = -L" (assoc-ref inputs "mesa") "/lib -lGL"))
                  (("FFTW3_LIB.*= -lfftw3")
                   (string-append "FFTW3_LIB = -L" (assoc-ref inputs "fftw") "/lib -lfftw3"))
                  (("-I/usr/include/tcl.*)")
                   (string-append "-I" (assoc-ref inputs "tcl") "/include"))
                  (("CPPFLAGS.*")
                    "CPPFLAGS ?= -fcommon") 
                  (("CFLAGS +=")
                    "CFLAGS += -fcommon") 
                )
                (substitute* "Tcl/xcInit.tcl"
                  (("set bwidget .*/external/lib/bwidget-")
                   (string-append "set bwidget [glob -nocomplain " 
                                  (assoc-ref inputs "bwidget-toolkit") 
                                  "/lib/external/lib/bwidget-"))
                )
                (delete-file-recursively "external")
              )
            )
            (replace 'install
              (lambda* (#:key outputs inputs #:allow-other-keys)
              (setenv "prefix" (assoc-ref outputs "out"))
                (invoke "make" "install")
              )
            )
          )
        #:test-target "check"
        #:tests? #f))
    (home-page "http://www.xcrysden.org")
    (synopsis "X-window CRYstalline Structures and DENsities")
    (description " XCrySDen is a crystalline and molecular structure visualisation program aiming at display of isosurfaces and contours, which can be superimposed on crystalline structures and interactively rotated and manipulated. It runs on GNU/Linux. ")
    (license license:gpl2)))

(define-public togl2-source
  (package
    (name "togl2-source")
    (version "8.4")
    (source
      (origin
        (method url-fetch)
        (uri "https://downloads.sourceforge.net/project/togl/Togl/2.0/Togl2.0-src.tar.gz")
        (sha256 (base32 "043n7yx5gxb04gd1s8v3ky14rc4gzpkrkvh5slcadb6kp85skm5p"))))
    (build-system gnu-build-system)
    (arguments
      (list
        #:configure-flags 
        #~(list 
            (string-append "--with-tcl=" (assoc-ref %build-inputs "tcl") "/lib")
            (string-append "--with-tk=" (assoc-ref %build-inputs "tk") "/lib")
            (string-append "TCL_SRC_DIR=" (assoc-ref %build-inputs "tcl"))
            (string-append "TK_SRC_DIR=" (assoc-ref %build-inputs "tk"))
            (string-append "TCL_TOP_DIR_NATIVE=" (assoc-ref %build-inputs "tcl"))
            (string-append "CPPFLAGS=-I" (assoc-ref %build-inputs "tcl") "/include"
                                   " -I" (assoc-ref %build-inputs "tk") "/include"
                                   " -I" (assoc-ref %build-inputs "tk") "/include/generic"
                                   " -I" (assoc-ref %build-inputs "tk") "/include/unix"
            )
            (string-append "--prefix=" (assoc-ref %outputs "out"))
            (string-append "--exec-prefix=" (assoc-ref %outputs "out"))
          )
        #:phases
        #~(modify-phases %standard-phases
            (add-before 'configure 'patch-paths 
              (lambda* (#:key outputs inputs #:allow-other-keys)
                (substitute* "configure"
                  (("TCL_SRC_DIR.*=.*")
                   (string-append "TCL_SRC_DIR=" (assoc-ref inputs "tcl") "/include"))
                  (("generic/tclInt.h")
                    "tclInt.h")
                  (("TK_SRC_DIR.*=.*")
                   (string-append "TK_SRC_DIR=" (assoc-ref inputs "tk") "/include"))
                )
              )
            )
          )
        #:tests? #f)
    )
    (inputs
     `(
        ("mesa" ,mesa) 
        ("libxmu" ,libxmu)
        ("tk" ,tk-with-sources)
        ("tcl" ,tcl)
      ))
    (native-inputs
     (list automake autoconf-2.68))
    (home-page "")
    (synopsis "A Tcl/Tk widget for OpenGL rendering.")
    (description "Togl provides a platform independent Tk widget for using OpenGL rendering contexts.")
    (license #f)))

(define-public tk-with-sources
  (package
    (inherit tk)
    (name "tk-with-sources")
    (arguments
     `(#:phases
        (modify-phases %standard-phases
          (add-before 'configure 'pre-configure
              (lambda _ (chdir "unix")))
            (add-after 'install 'create-wish-symlink
              (lambda* (#:key outputs #:allow-other-keys)
                (let ((out (assoc-ref outputs "out")))
                     (symlink (string-append out "/bin/wish"
                                ,(version-major+minor
                                  (package-version tk)))
                              (string-append out "/bin/wish")))))
            (add-after 'install 'add-fontconfig-flag
              (lambda* (#:key inputs outputs #:allow-other-keys)
                ;; Add the missing -L flag for Fontconfig in 'tk.pc' and
                ;; 'tkConfig.sh'.
                (let ((out        (assoc-ref outputs "out"))
                      (fontconfig (assoc-ref inputs "fontconfig")))
                  (substitute* (find-files out
                                                 "^(tkConfig\\.sh|tk\\.pc)$")
                    (("-lfontconfig")
                     (string-append "-L" fontconfig
                                    "/lib -lfontconfig"))))))
          ;; This is the only modification brought to the package
          ;; It was needed as softwares using tk (such as xcrysden)
          ;; requires ".h" headers  not natively installed in the
          ;; package - namely the ones included in :
          ;;     * generic,
          ;;     * unix.
          ;; For clarity, adding both those subdirectories in the
          ;; /include output directory
          (add-after 'install 'relocate-sources
            (lambda* (#:key outputs #:allow-other-keys)
              (for-each
                (lambda (f) (install-file f 
                                          (string-append (assoc-ref outputs "out") "/include/generic/")))
                (find-files "../generic" "\\.h$")
              )
              (for-each
                (lambda (f) (install-file f 
                                          (string-append (assoc-ref outputs "out") "/include/unix/")))
                (find-files "../unix" "\\.h$")
              )
            )
          )
          ;; End of the modification ;;                         
        )
       #:configure-flags
       (list (string-append "--with-tcl="
                            (assoc-ref %build-inputs "tcl")
                            "/lib")
             ;; This is needed when cross-compiling, see:
             ;; https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=719247
             ,@(if (%current-target-system)
                   '("tcl_cv_strtod_buggy=1"
                     "ac_cv_func_strtod=yes")
                   '()))

       ;; The tests require a running X server, so we just skip them.
       #:tests? #f))))

(define-public bwidget-toolkit
  (package
    (name "bwidget-toolkit")
    (version "1.9.16")
    (source
      (origin
        (method url-fetch)
        (uri (string-append "https://downloads.sourceforge.net/project/tcllib/BWidget/"
                             version "/bwidget-" version ".tar.gz"))
        (sha256 (base32 "1q0sx1f7wcqz7jcfvpvs1hyq2wb5m3fzd9r06v996hmqfiih7q5z"))))
    (inputs
     (list tcl))
    (build-system copy-build-system)
    (arguments
     `(#:install-plan
       '(
          ("./"    "lib/external/lib/bwidget-1.9.16" #:include-regexp ("\\.tcl"))
          ("lang"  "lib/external/lib/bwidget-1.9.16/")
          ("BWman" "doc/")
        )
     )
    )
    (home-page "https://core.tcl-lang.org/bwidget/home")
    (synopsis "High-level Widget Set for Tcl/Tk built using native Tcl/Tk 8.x namespaces.")
    (description
      "The BWidget library was originally developed by UNIFIX Online, and
       released under both the GNU Public License and the Tcl license.
       No platform dependencies, and no compiling required.  The code is 100% Pure Tcl/Tk.
       BWidget is now maintained as a community project, hosted by
       Sourceforge.  Scores of fixes and enhancements have been added by
       community developers.  See the ChangeLog file for details.")
    (license license:bsd-3)))


(define-public xsfconvert
  (package
    (name "xsfconvert")
    (version "1.03")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://github.com/jenskunstmann/xsfconvert.git")
              (recursive? #f)
              (commit "abc57e183f8d8b7245fa71a42b47b84592524e52")))
        (file-name (git-file-name name version))
        (sha256 (base32 "0msbv5y89j4ngwqfwfd9xr8s48ws8sy7diwlgxhrkks683nzik3a"))))
    (build-system gnu-build-system)    
    (arguments 
      (list
        #:phases
        #~(modify-phases %standard-phases
            (replace 'configure
              (lambda* (#:key outputs inputs #:allow-other-keys)
                (mkdir-p "bin")
                (chdir "source")
              )
            )
            (replace 'build
              (lambda* (#:key inputs #:allow-other-keys)
                (invoke "make" "all")
              )
            )
            (replace 'install
              (lambda* (#:key outputs inputs #:allow-other-keys)
                (let* ((out (assoc-ref outputs "out"))
                       (bindir (string-append out "/bin"))
                       (shadir (string-append out "/share"))
                      )
                      (for-each (lambda (f) (install-file f bindir))
                        (find-files "../bin/" "."))
                      (install-file "../COPYING" shadir)
                )

              )
            )
          )
        #:tests? #f
      )
    )
    (home-page "https://github.com/jenskunstmann/xsfconvert")
    (synopsis "xsfconvert is a modular code that allows to convert the output of different ab initio codes into the xsf-format")
    (description ".xsf is the file format of the program XCrysDen, a powerful and easy to use visualization tool. 
This allows for an easy-to-handle and fast visualization of crystal structures and densities.")
    (license license:gpl2)))

(define-public siestoxsf
  (package
    (name "siestoxsf")
    (version "0.4")
    (source
      (origin
        (method url-fetch)
        (uri "https://www.home.uni-osnabrueck.de/apostnik/Software/Sies2xsf.tar.gz")
        (sha256 (base32 "11s6x826yfmwfpii83pnrr0jrggn2drys7ldyn865hqjn8wn09fp"))))
    (inputs
     (list
      gfortran))
    (build-system gnu-build-system)
    (arguments 
      (list
        #:phases
        #~(modify-phases %standard-phases
            (delete 'configure)
            ; (replace 'build
            ;   (lambda* (#:key inputs #:allow-other-keys)
            ;     (invoke "make" "all")
            ;   )
            ; )
            (replace 'install
              (lambda* (#:key outputs inputs #:allow-other-keys)
                (let* ((out (assoc-ref outputs "out"))
                       (bindir (string-append out "/bin"))
                      )
                      (for-each (lambda (f) (install-file f bindir))
                        (find-files "." (lambda (file stat) (executable-file? file))))
                )

              )
            )
          ) 
        #:tests? #f
      )
    )
    (home-page "https://www.home.uni-osnabrueck.de/apostnik/download.html")
    (synopsis "Sies2xsf selection of routines")
    (description "The set is for transforming SIESTA properties files into input files for the XCrySDen package.")
    (license license:gpl1)))

;;;                      MAYBE ADD c2x LATER ??                               ;;;
;;; >>>>>>>>>> https://www.c2x.org.uk/downloads/quick_install.html <<<<<<<<<< ;;;
