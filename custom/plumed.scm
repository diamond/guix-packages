(define-module (custom plumed)
  #:use-module (guix)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (guix build-system gnu)
  #:use-module (guix git-download)
  #:use-module (gnu packages algebra)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bootstrap)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages elf)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages libevent)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages mpi)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages python)
  #:use-module (gnu packages xorg)
  #:use-module (custom vmd))

(define-public plumed
  (package
    (name "plumed")
    (version "2.8.3")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
            (url "https://github.com/plumed/plumed2.git")
            (recursive? #f)
            (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256 (base32 "0b149gm0hdsqp1vl09myaf7nll9iwafm8f7fp3s5sycwb3l7w1zi")))) ; v2.8.3
    (native-inputs
     (list patch patchelf autoconf))
    (inputs
     `(
        ("fftw" ,fftw)
        ("gcc:lib" ,gcc "lib")
        ("gfortran" ,gfortran)
        ("glibc" ,glibc)
        ("hwloc:lib" ,hwloc "lib")
        ("lapack" ,lapack)
        ("libevent" ,libevent)
        ("libpciaccess" ,libpciaccess)
        ("vmd" ,vmd) ; comes with tcl
        ("openmpi" ,openmpi)
        ("openblas" ,openblas)
        ("perl" ,perl)
        ("python" ,python-wrapper)
        ("zlib" ,zlib)
      ))
    (build-system gnu-build-system)
    (arguments 
       `(#:configure-flags   (list
                                  (string-append "--prefix=" (assoc-ref %outputs "out"))
                                  (string-append "CPPFLAGS=-I" (assoc-ref %build-inputs "vmd") "/lib/plugins/include"
                                                         " -I" (assoc-ref %build-inputs "vmd") "/lib/plugins/LINUXAMD64/molfile")
                                  (string-append "LDFLAGS=-L" (assoc-ref %build-inputs "vmd") "/lib/plugins/LINUXAMD64/molfile"
                                                        " -L" (assoc-ref %build-inputs "tcl") "/lib -ltcl8.6")
                                  "CXXFLAGS=-O3"    
                                )
         #:test-target "check" ; needs a patchelf plan ; so should be done after installation
         #:tests? #f           ; hence disabling tests
         #:phases 
            (modify-phases %standard-phases 
                (add-before 'check 'mpi-setup ,%openmpi-setup)
                (delete 'validate-runpath)
                (delete 'strip)
                (add-after 'install 'correct-runpath
                    (lambda* (#:key native-inputs inputs outputs #:allow-other-keys)
                        (define out (assoc-ref outputs "out"))
                        (define plumed-exec (string-append out "/bin/plumed"))
                        (define plumed-lib1 (string-append out "/lib/libplumed.so"))
                        (define libself (string-append out "/lib"))
                        (define libglibc (string-append (assoc-ref inputs "glibc") "/lib"))
                        (define libompi (string-append (assoc-ref inputs "openmpi") "/lib"))
                        (define libgcc (string-append (assoc-ref inputs "gcc:lib") "/lib"))
                        (define libhwloc (string-append (assoc-ref inputs "hwloc:lib") "/lib"))
                        (define liblibpciaccess (string-append (assoc-ref inputs "libpciaccess") "/lib"))
                        (define libzlib (string-append (assoc-ref inputs "zlib") "/lib"))
                        (define liblibevent (string-append (assoc-ref inputs "libevent") "/lib"))
                        (define libvmd (string-append (assoc-ref inputs "vmd") "/lib")) ; not sure we need it
                        (define ld.so (string-append (assoc-ref inputs "glibc") ,(glibc-dynamic-linker)))
                        (define runpath-plumed 
                          (string-append libself ":"
                                         libglibc ":"
                                         libompi ":"
                                         libgcc ":"
                                         libhwloc ":"
                                         liblibpciaccess ":"
                                         libzlib ":"
                                         liblibevent ":"
                                         libvmd))
                        ;; Change runpath without --force-runpath patchelf option! (@PAB//gricad-guix-packages/common/absuite.scm)
      		              (define (patch-elf-interpreter ld rpath file) ; for binaries
                                  (make-file-writable file)
                                  (format #t "Setting interpreter on '~a'...~%" file)
                                  (format #t "Interpreter is '~a'...~%" ld)
                                  (invoke "patchelf" "--set-interpreter" ld file)
                                  (format #t "Setting RPATH on '~a'...~%" file)
                                  (format #t "runpath is '~a'...~%" rpath)
                                  (invoke "patchelf" "--set-rpath" rpath  file))
                        (define (patch-elf-no-interpreter ld rpath file) ; for shared libraries
                                  (make-file-writable file)
                                  (format #t "Setting RPATH on '~a'...~%" file)
                                  (format #t "runpath is '~a'...~%" rpath)
                                  (invoke "patchelf" "--set-rpath" rpath  file))
                        (patch-elf-interpreter ld.so runpath-plumed plumed-exec)#t
                        (patch-elf-no-interpreter ld.so runpath-plumed plumed-lib1)#f))
                )))
    (home-page "https://www.plumed.org/")
    (synopsis "The community-developed PLUgin for MolEcular Dynamics")
    (description "PLUMED is an open-source, community-developed library that provides a wide range of different methods, which include: enhanced-sampling algorithms, free-energy methods, tools to analyze the vast amounts of data produced by molecular dynamics (MD) simulations.
These techniques can be used in combination with a large toolbox of collective variables that describe complex processes in physics, chemistry, material science, and biology.")
    (license license:gpl3)))