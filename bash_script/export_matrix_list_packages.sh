shopt -s extglob
## for manifests which do not require time-machine
export matrix_code=""
folder="custom"
for file in "$folder"/*; do
      file="${file%.scm}"; file="${file#custom/}"
      matrix_code="${matrix_code}${file}, "
   done
matrix_code="${matrix_code::-2}"

## for manifests which do require time-machine
export matrix_code_tm=""
folder="channel_time_machine"
for file in "$folder"/*; do
   file="${file%.channels}"; file="${file#channel_time_machine/}"
   matrix_code_tm="${matrix_code_tm}${file}, "
   ## remove package requiring time-machine from global array
   matrix_code=`echo "${matrix_code}" | sed "s/, ${file},/,/g"`
done
matrix_code_tm="${matrix_code_tm::-2}"
